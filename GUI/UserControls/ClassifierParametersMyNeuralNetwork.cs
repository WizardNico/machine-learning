﻿using System;
using System.Windows.Forms;
using BioLab.Classification.Supervised;
using PRLab.EsercitazioniML;

namespace PRLab
{
  internal partial class ClassifierParametersMyNeuralNetwork : ParametersControl
  {
    private bool disableChangeParamEvent = false;

    public ClassifierParametersMyNeuralNetwork()
    {
      InitializeComponent();
    }

    public ClassifierParametersMyNeuralNetwork(MyNeuralNetworkClassifierBuilder c)
      : base(c)
    {
      InitializeComponent();
      disableChangeParamEvent = true;
      numericUpDown4.Value = c.HiddenUnitCount;
      numericUpDown5.Value = (decimal)c.LearningRate;
      numericUpDown6.Value = c.MaxEpochs;
      numericUpDown7.Value = (decimal)c.MaxError;
      checkBox1.Checked = c.Biased;
      disableChangeParamEvent = false;

      UpdateControls();
    }

    private void UpdateControls()
    {
        var c = (MyNeuralNetworkClassifierBuilder)argument;
    }

    private void paramChanged(object sender, EventArgs e)
    {
      if (!disableChangeParamEvent)
      {
          var c = (MyNeuralNetworkClassifierBuilder)argument;
        c.HiddenUnitCount = (int)numericUpDown4.Value;
        c.Biased = checkBox1.Checked;
        c.LearningRate = (double)numericUpDown5.Value;
        c.MaxEpochs = (int)numericUpDown6.Value;
        c.MaxError = (double)numericUpDown7.Value;
        UpdateControls();
        RaiseParametersChangedEvent();
      }
    }
  }
}
