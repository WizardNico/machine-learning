namespace PRLab
{
    partial class ClassifierParametersSVM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBoxShrinking = new System.Windows.Forms.CheckBox();
            this.checkBoxProbEstimates = new System.Windows.Forms.CheckBox();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.comboBoxKernel = new System.Windows.Forms.ComboBox();
            this.numericUpDownDegree = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownGamma = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCoef0 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDownEpsilon = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTolerance = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownCost = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDegree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGamma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoef0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEpsilon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCost)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Kernel";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Degree";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Gamma";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Coef0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Epsilon";
            // 
            // checkBoxShrinking
            // 
            this.checkBoxShrinking.AutoSize = true;
            this.checkBoxShrinking.Checked = true;
            this.checkBoxShrinking.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShrinking.Enabled = false;
            this.checkBoxShrinking.Location = new System.Drawing.Point(4, 193);
            this.checkBoxShrinking.Name = "checkBoxShrinking";
            this.checkBoxShrinking.Size = new System.Drawing.Size(70, 17);
            this.checkBoxShrinking.TabIndex = 16;
            this.checkBoxShrinking.Text = "Shrinking";
            this.checkBoxShrinking.UseVisualStyleBackColor = true;
            this.checkBoxShrinking.CheckedChanged += new System.EventHandler(this.paramChanged);
            // 
            // checkBoxProbEstimates
            // 
            this.checkBoxProbEstimates.AutoSize = true;
            this.checkBoxProbEstimates.Checked = true;
            this.checkBoxProbEstimates.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxProbEstimates.Enabled = false;
            this.checkBoxProbEstimates.Location = new System.Drawing.Point(77, 193);
            this.checkBoxProbEstimates.Name = "checkBoxProbEstimates";
            this.checkBoxProbEstimates.Size = new System.Drawing.Size(98, 17);
            this.checkBoxProbEstimates.TabIndex = 17;
            this.checkBoxProbEstimates.Text = "Prob. estimates";
            this.checkBoxProbEstimates.UseVisualStyleBackColor = true;
            this.checkBoxProbEstimates.CheckedChanged += new System.EventHandler(this.paramChanged);
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.Enabled = false;
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "C-SVC",
            "nu-SVC",
            "one-class SVM",
            "epsilon-SVR",
            "nu-SVR"});
            this.comboBoxType.Location = new System.Drawing.Point(59, 4);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(120, 21);
            this.comboBoxType.TabIndex = 1;
            this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.paramChanged);
            // 
            // comboBoxKernel
            // 
            this.comboBoxKernel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxKernel.FormattingEnabled = true;
            this.comboBoxKernel.Items.AddRange(new object[] {
            "linear",
            "polynomial",
            "radial basis function"});
            this.comboBoxKernel.Location = new System.Drawing.Point(59, 27);
            this.comboBoxKernel.Name = "comboBoxKernel";
            this.comboBoxKernel.Size = new System.Drawing.Size(120, 21);
            this.comboBoxKernel.TabIndex = 3;
            this.comboBoxKernel.SelectedIndexChanged += new System.EventHandler(this.paramChanged);
            // 
            // numericUpDownDegree
            // 
            this.numericUpDownDegree.Location = new System.Drawing.Point(59, 51);
            this.numericUpDownDegree.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownDegree.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDegree.Name = "numericUpDownDegree";
            this.numericUpDownDegree.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownDegree.TabIndex = 5;
            this.numericUpDownDegree.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownDegree.ValueChanged += new System.EventHandler(this.paramChanged);
            // 
            // numericUpDownGamma
            // 
            this.numericUpDownGamma.DecimalPlaces = 5;
            this.numericUpDownGamma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownGamma.Location = new System.Drawing.Point(59, 74);
            this.numericUpDownGamma.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownGamma.Name = "numericUpDownGamma";
            this.numericUpDownGamma.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownGamma.TabIndex = 7;
            this.numericUpDownGamma.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownGamma.ValueChanged += new System.EventHandler(this.paramChanged);
            // 
            // numericUpDownCoef0
            // 
            this.numericUpDownCoef0.DecimalPlaces = 1;
            this.numericUpDownCoef0.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownCoef0.Location = new System.Drawing.Point(59, 97);
            this.numericUpDownCoef0.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownCoef0.Name = "numericUpDownCoef0";
            this.numericUpDownCoef0.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownCoef0.TabIndex = 9;
            this.numericUpDownCoef0.ValueChanged += new System.EventHandler(this.paramChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Tolerance";
            // 
            // numericUpDownEpsilon
            // 
            this.numericUpDownEpsilon.DecimalPlaces = 2;
            this.numericUpDownEpsilon.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numericUpDownEpsilon.Location = new System.Drawing.Point(59, 143);
            this.numericUpDownEpsilon.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownEpsilon.Name = "numericUpDownEpsilon";
            this.numericUpDownEpsilon.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownEpsilon.TabIndex = 13;
            this.numericUpDownEpsilon.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownEpsilon.ValueChanged += new System.EventHandler(this.paramChanged);
            // 
            // numericUpDownTolerance
            // 
            this.numericUpDownTolerance.DecimalPlaces = 4;
            this.numericUpDownTolerance.Enabled = false;
            this.numericUpDownTolerance.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDownTolerance.Location = new System.Drawing.Point(59, 165);
            this.numericUpDownTolerance.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTolerance.Name = "numericUpDownTolerance";
            this.numericUpDownTolerance.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownTolerance.TabIndex = 15;
            this.numericUpDownTolerance.Value = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDownTolerance.ValueChanged += new System.EventHandler(this.paramChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Cost";
            // 
            // numericUpDownCost
            // 
            this.numericUpDownCost.DecimalPlaces = 1;
            this.numericUpDownCost.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownCost.Location = new System.Drawing.Point(59, 120);
            this.numericUpDownCost.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownCost.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCost.Name = "numericUpDownCost";
            this.numericUpDownCost.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownCost.TabIndex = 11;
            this.numericUpDownCost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCost.ValueChanged += new System.EventHandler(this.paramChanged);
            // 
            // ClassifierParametersSVM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.numericUpDownTolerance);
            this.Controls.Add(this.numericUpDownEpsilon);
            this.Controls.Add(this.numericUpDownCost);
            this.Controls.Add(this.numericUpDownCoef0);
            this.Controls.Add(this.numericUpDownGamma);
            this.Controls.Add(this.numericUpDownDegree);
            this.Controls.Add(this.comboBoxKernel);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.checkBoxProbEstimates);
            this.Controls.Add(this.checkBoxShrinking);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ClassifierParametersSVM";
            this.Size = new System.Drawing.Size(185, 226);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDegree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGamma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoef0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEpsilon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCost)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBoxShrinking;
        private System.Windows.Forms.CheckBox checkBoxProbEstimates;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.ComboBox comboBoxKernel;
        private System.Windows.Forms.NumericUpDown numericUpDownDegree;
        private System.Windows.Forms.NumericUpDown numericUpDownGamma;
        private System.Windows.Forms.NumericUpDown numericUpDownCoef0;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDownEpsilon;
        private System.Windows.Forms.NumericUpDown numericUpDownTolerance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownCost;
    }
}
