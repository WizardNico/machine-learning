using System;
using BioLab.Classification.Supervised;
using PRLab.EsercitazioniVR;

namespace PRLab
{
  internal partial class ClassifierParametersVR_MCSConfidence : ParametersControl
  {
    private bool RaiseEvent = false;

    public ClassifierParametersVR_MCSConfidence()
    {
      InitializeComponent();
    }

    public ClassifierParametersVR_MCSConfidence(VR_McsConfidenceBuilder c)
      : base(c)
    {
      InitializeComponent();
      comboBoxFusionMethod.SelectedIndex = (int)c.FusionRule;
      RaiseEvent = true;
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (RaiseEvent)
      {
        var c = (VR_McsConfidenceBuilder)argument;
        c.FusionRule = (ClassifierFusionMethod)comboBoxFusionMethod.SelectedIndex;
        RaiseParametersChangedEvent();
      }
    }
  }
}

