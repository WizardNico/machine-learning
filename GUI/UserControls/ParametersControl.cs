using System;
using System.Windows.Forms;
using BioLab.Classification.Supervised;
using BioLab.Classification.Unsupervised;
using PRLab.EsercitazioniVR;
using BioLab.Common;
using BioLab.Classification.DensityEstimation;
using PRLab.EsercitazioniML;

namespace PRLab
{
  partial class ParametersControl : UserControl
  {
    public ParametersControl()
    {
      InitializeComponent();
    }

    public ParametersControl(object o)
    {
      argument = o;
      InitializeComponent();
    }

    /// <summary>
    /// Crea lo user control adatto al classificatore c
    /// </summary>
    public static ParametersControl CreateClassifierParametersControl(ClassifierBuilder c)
    {
      if (c is KnnClassifierBuilder)
        return new ClassifierParametersKNN((KnnClassifierBuilder)c);
      else if (c is SvmClassifierBuilder)
        return new ClassifierParametersSVM((SvmClassifierBuilder)c);
      else if (c is McsConfidenceBuilder)
        return new ClassifierParametersMCSConfidence((McsConfidenceBuilder)c);
      else if (c is MyMcsConfidenceBuilder)
        return new ClassifierParametersMyMCSConfidence((MyMcsConfidenceBuilder)c);
      else if (c is BayesClassifierBuilder)
      {
        var bayesClBuilder = (BayesClassifierBuilder)c;
        if (bayesClBuilder.DensityEstimator is NormalMLEstimator ||
            bayesClBuilder.DensityEstimator is MyNormalMLEstimator)
        {
          return new ClassifierParametersControlNone(c);
        }
        else if (bayesClBuilder.DensityEstimator is ParzenEstimator ||
                bayesClBuilder.DensityEstimator is MyParzenEstimator)
        {
          return new ClassifierParametersParzen(bayesClBuilder);
        }
        else
        {
          throw new ArgumentException("Density Estimator non supportato"); 
        }
      }
      else if (c is McsMajorVoteBuilder)
      {
        return new ClassifierParametersControlNone(c);
      }
      else if (c is NeuralNetworkClassifierBuilder)
      {
        return new ClassifierParametersNeuralNetwork((NeuralNetworkClassifierBuilder)c);
      }
      else if (c is MyNeuralNetworkClassifierBuilder)
      {
          return new ClassifierParametersMyNeuralNetwork((MyNeuralNetworkClassifierBuilder)c);
      }
      else
      {
        //throw new ArgumentException("Argomento non supportato");
        return new ClassifierParametersControlNone(c);
      }
    }

    /// <summary>
    /// Crea lo user control adatto al VAProbabilityEstimator pe
    /// </summary>
    public static ParametersControl CreateDensityEstParametersControl(DensityEstimator de)
    {
      if (de is ParzenEstimator ||
          de is MyParzenEstimator)
        return new DensityEstParametersParzen(de);
      return new ClassifierParametersControlNone(de);
    }

    /// <summary>
    /// Crea lo user control adatto al Clusterizer cl
    /// </summary>
    /// <param name="vAClusterizer"></param>
    /// <returns></returns>
    public static ParametersControl CreateClusterizerParametersControl(Clusterizer cl)
    {
      return new ClassifierParametersControlNone(cl);
    }


    /// <summary>
    /// Evento (senza parametri) che scatta alla modifica di uno dei parametri
    /// </summary>
    public event EventHandler ParametersChanged;

    /// <summary>
    /// Le classi derivate devono chiamare questo metodo quando un parametro � stato modificato
    /// </summary>
    protected void RaiseParametersChangedEvent()
    {
      EventHandler tmp = ParametersChanged; // copia in una var locale per essere thread-safe!
      if (tmp != null)
        tmp(this, EventArgs.Empty);
    }

    /// <summary>
    /// Le classi derivate sono tenute ad aggiornare l'oggetto associato quando vengono cambiati dei parametri
    /// </summary>
    protected object argument;

  }
}
