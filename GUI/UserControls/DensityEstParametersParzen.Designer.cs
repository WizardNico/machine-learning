namespace PRLab
{
    partial class DensityEstParametersParzen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxKernel = new System.Windows.Forms.ComboBox();
            this.numericUpDownH = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownH)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxKernel
            // 
            this.comboBoxKernel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxKernel.FormattingEnabled = true;
            this.comboBoxKernel.Items.AddRange(new object[] {
            "Ipercubo",
            "Normale"});
            this.comboBoxKernel.Location = new System.Drawing.Point(46, 5);
            this.comboBoxKernel.Name = "comboBoxKernel";
            this.comboBoxKernel.Size = new System.Drawing.Size(116, 21);
            this.comboBoxKernel.TabIndex = 0;
            this.comboBoxKernel.SelectedIndexChanged += new System.EventHandler(this.parameterChanged);
            // 
            // numericUpDownH
            // 
            this.numericUpDownH.DecimalPlaces = 2;
            this.numericUpDownH.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownH.Location = new System.Drawing.Point(46, 32);
            this.numericUpDownH.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownH.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownH.Name = "numericUpDownH";
            this.numericUpDownH.Size = new System.Drawing.Size(116, 20);
            this.numericUpDownH.TabIndex = 1;
            this.numericUpDownH.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownH.ValueChanged += new System.EventHandler(this.parameterChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "h";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Kernel";
            // 
            // DensityEstParametersParzen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownH);
            this.Controls.Add(this.comboBoxKernel);
            this.Name = "DensityEstParametersParzen";
            this.Size = new System.Drawing.Size(165, 204);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownH)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxKernel;
        private System.Windows.Forms.NumericUpDown numericUpDownH;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
