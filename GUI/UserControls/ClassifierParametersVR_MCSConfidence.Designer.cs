namespace PRLab
{
  partial class ClassifierParametersVR_MCSConfidence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxFusionMethod = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxFusionMethod
            // 
            this.comboBoxFusionMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFusionMethod.FormattingEnabled = true;
            this.comboBoxFusionMethod.Items.AddRange(new object[] {
            "Somma",
            "Prodotto",
            "Massimo",
            "Minimo"});
            this.comboBoxFusionMethod.Location = new System.Drawing.Point(99, 18);
            this.comboBoxFusionMethod.Name = "comboBoxFusionMethod";
            this.comboBoxFusionMethod.Size = new System.Drawing.Size(75, 21);
            this.comboBoxFusionMethod.TabIndex = 0;
            this.comboBoxFusionMethod.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Regola di fusione";
            // 
            // ClassifierParametersMCSConfidence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxFusionMethod);
            this.Name = "ClassifierParametersMCSConfidence";
            this.Size = new System.Drawing.Size(183, 204);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxFusionMethod;
        private System.Windows.Forms.Label label1;
    }
}
