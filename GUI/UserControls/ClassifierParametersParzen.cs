using System;
using BioLab.Classification.Supervised;
using BioLab.Classification.DensityEstimation;
using PRLab.EsercitazioniML;

namespace PRLab
{
    internal partial class ClassifierParametersParzen : ParametersControl
    {
        public ClassifierParametersParzen()
        {
            InitializeComponent();
        }

        public ClassifierParametersParzen(BayesClassifierBuilder c)
            : base(c)
        {
            if (!(c.DensityEstimator is ParzenEstimator ||
                  c.DensityEstimator is MyParzenEstimator))
            {
                throw new Exception();
            }

            InitializeComponent();

            ParzenKernelType kernelType=ParzenKernelType.Gaussian;
            double bandwidth=0.0;
            if (c.DensityEstimator is ParzenEstimator)
            {
                var parzenEstimator = (ParzenEstimator)c.DensityEstimator;

                kernelType=parzenEstimator.KernelType;
                bandwidth=parzenEstimator.Bandwidth;
            }
            else if (c.DensityEstimator is MyParzenEstimator)
            {
                var parzenEstimator = (MyParzenEstimator)c.DensityEstimator;

                kernelType = parzenEstimator.KernelType;
                bandwidth = parzenEstimator.Bandwidth;
            }

            comboBoxKernel.SelectedIndex = (int) kernelType;
            numericUpDownH.Value = (decimal) bandwidth;
            
            parametersChangeDisabled = false; // solo ora abilita
        }

        private void parameterChanged(object sender, EventArgs e)
        {
            if (!parametersChangeDisabled)
            {
                var c = (BayesClassifierBuilder)argument;

                if (c.DensityEstimator is ParzenEstimator)
                {
                    var parzenEstimator = (ParzenEstimator)c.DensityEstimator;
                    parzenEstimator.KernelType = (ParzenKernelType)comboBoxKernel.SelectedIndex;
                    parzenEstimator.Bandwidth = (double)numericUpDownH.Value;
                }
                else if (c.DensityEstimator is MyParzenEstimator)
                {
                    var parzenEstimator = (MyParzenEstimator)c.DensityEstimator;
                    parzenEstimator.KernelType = (ParzenKernelType)comboBoxKernel.SelectedIndex;
                    parzenEstimator.Bandwidth = (double)numericUpDownH.Value;
                }

                RaiseParametersChangedEvent();
            }
        }

        private bool parametersChangeDisabled = true;
    }
}

