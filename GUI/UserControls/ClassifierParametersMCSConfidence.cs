using System;
using BioLab.Classification.Supervised;

namespace PRLab
{
  internal partial class ClassifierParametersMCSConfidence : ParametersControl
  {
    private bool RaiseEvent = false;

    public ClassifierParametersMCSConfidence()
    {
      InitializeComponent();
    }

    public ClassifierParametersMCSConfidence(McsConfidenceBuilder c)
      : base(c)
    {
      InitializeComponent();
      comboBoxFusionMethod.SelectedIndex = (int)c.FusionRule;
      RaiseEvent = true;
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (RaiseEvent)
      {
        var c = (McsConfidenceBuilder)argument;
        c.FusionRule = (ClassifierFusionMethod)comboBoxFusionMethod.SelectedIndex;
        RaiseParametersChangedEvent();
      }
    }
  }
}

