using System;
using BioLab.Classification.Supervised;

namespace PRLab
{
  internal partial class ClassifierParametersKNN : ParametersControl
    {
        public ClassifierParametersKNN()
        {
            InitializeComponent();
        }

        public ClassifierParametersKNN(KnnClassifierBuilder b)
            : base(b)
        {
            InitializeComponent();
            numericUpDownK.Value = b.K;
        }

        private void numericUpDownK_ValueChanged(object sender, EventArgs e)
        {
          ((KnnClassifierBuilder)argument).K = (int)numericUpDownK.Value;
            RaiseParametersChangedEvent();
        }
    }
}

