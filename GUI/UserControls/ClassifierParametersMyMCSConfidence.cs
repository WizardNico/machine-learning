using System;
using BioLab.Classification.Supervised;
using PRLab.EsercitazioniVR;
using PRLab.EsercitazioniML;

namespace PRLab
{
  internal partial class ClassifierParametersMyMCSConfidence : ParametersControl
  {
    private bool RaiseEvent = false;

    public ClassifierParametersMyMCSConfidence()
    {
      InitializeComponent();
    }

    public ClassifierParametersMyMCSConfidence(MyMcsConfidenceBuilder c)
      : base(c)
    {
      InitializeComponent();
      comboBoxFusionMethod.SelectedIndex = (int)c.FusionRule;
      RaiseEvent = true;
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (RaiseEvent)
      {
        var c = (MyMcsConfidenceBuilder)argument;
        c.FusionRule = (ClassifierFusionMethod)comboBoxFusionMethod.SelectedIndex;
        RaiseParametersChangedEvent();
      }
    }
  }
}

