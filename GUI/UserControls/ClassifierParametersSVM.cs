using System;
using BioLab.Classification.Supervised;

namespace PRLab
{
    internal partial class ClassifierParametersSVM : ParametersControl
    {
        private bool disableChangeParamEvent = false;

        public ClassifierParametersSVM()
        {
            InitializeComponent();
        }

        public ClassifierParametersSVM(SvmClassifierBuilder c)
            : base(c)
        {
            InitializeComponent();
            disableChangeParamEvent = true;
            comboBoxType.SelectedIndex = (int)c.SvmType;
            comboBoxKernel.SelectedIndex = (int)c.KernelType;
            numericUpDownDegree.Value = c.Degree;
            numericUpDownGamma.Value = (decimal)c.Gamma;
            numericUpDownCoef0.Value =(decimal)c.Coef0;
            numericUpDownCost.Value = (decimal)c.Cost;
            numericUpDownEpsilon.Value = (decimal)c.EpsilonInLossFunction;
            numericUpDownTolerance.Value = (decimal)c.EpsilonTolerance;
            checkBoxShrinking.Checked = c.Shrinking;
            checkBoxProbEstimates.Checked = c.Probability;
            disableChangeParamEvent = false;
            UpdateControls();
        }

        private void UpdateControls()
        {
            var c = (SvmClassifierBuilder)argument;
            numericUpDownDegree.Enabled = c.KernelType == KernelType.Polynomial;
            numericUpDownGamma.Enabled = c.KernelType == KernelType.Polynomial || c.KernelType == KernelType.RadialBasisFunction || c.KernelType == KernelType.Sigmoid;
            numericUpDownCoef0.Enabled = false;// c.KernelType == KernelType.Polynomial || c.KernelType == KernelType.Sigmoid;
            numericUpDownCost.Enabled = c.SvmType == SvmType.CSvc || c.SvmType == SvmType.EpsilonSvr || c.SvmType == SvmType.NuSvr;
            numericUpDownEpsilon.Enabled = c.SvmType == SvmType.EpsilonSvr;
            // TODO: verificare altri vincoli
        }

        private void paramChanged(object sender, EventArgs e)
        {
            if (!disableChangeParamEvent)
            {
                var c = (SvmClassifierBuilder)argument;
                c.SvmType = (SvmType)comboBoxType.SelectedIndex;
                c.KernelType = (KernelType)comboBoxKernel.SelectedIndex;
                c.Degree = (int)numericUpDownDegree.Value;
                c.Gamma = (double)numericUpDownGamma.Value;
                c.Coef0 = (double)numericUpDownCoef0.Value;
                c.Cost = (double)numericUpDownCost.Value;
                c.EpsilonInLossFunction = (double)numericUpDownEpsilon.Value;
                c.EpsilonTolerance = (double)numericUpDownTolerance.Value;
                c.Shrinking = checkBoxShrinking.Checked;
                c.Probability = checkBoxProbEstimates.Checked;
                UpdateControls();
                RaiseParametersChangedEvent();
            }
        }
    }
}

