using System;
using BioLab.Classification.DensityEstimation;
using PRLab.EsercitazioniML;


namespace PRLab
{
    internal partial class DensityEstParametersParzen : ParametersControl
    {
        public DensityEstParametersParzen()
        {
            InitializeComponent();
        }

        public DensityEstParametersParzen(DensityEstimator de)
            : base(de)
        {
            InitializeComponent();

            ParzenKernelType kernelType=ParzenKernelType.UserDefined;
            double bandWidth=0;
            if (de is ParzenEstimator)
            {
                var parzenEstimator = (ParzenEstimator)de;

                kernelType=parzenEstimator.KernelType;
                bandWidth=parzenEstimator.Bandwidth;
            }
            else if (de is MyParzenEstimator)
            {
                var parzenEstimator = (MyParzenEstimator)de;

                kernelType = parzenEstimator.KernelType;
                bandWidth = parzenEstimator.Bandwidth;
            }

            comboBoxKernel.SelectedIndex = (int)kernelType;
            numericUpDownH.Value = (decimal)bandWidth;

            parametersChangeDisabled = false; // solo ora abilita
        }

        private void parameterChanged(object sender, EventArgs e)
        {
            if (!parametersChangeDisabled)
            {
                if (argument is ParzenEstimator)
                {
                    var pe = (ParzenEstimator)argument;
                    pe.KernelType = (ParzenKernelType)comboBoxKernel.SelectedIndex;
                    pe.Bandwidth = (double)numericUpDownH.Value;
                }
                else if (argument is MyParzenEstimator)
                {
                    var pe = (MyParzenEstimator)argument;
                    pe.KernelType = (ParzenKernelType)comboBoxKernel.SelectedIndex;
                    pe.Bandwidth = (double)numericUpDownH.Value;
                }

                RaiseParametersChangedEvent();
            }
        }

        private bool parametersChangeDisabled = true;
    }
}

