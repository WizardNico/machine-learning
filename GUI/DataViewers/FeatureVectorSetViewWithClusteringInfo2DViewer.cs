using System;
using System.Collections.Generic;
using System.Drawing;
using BioLab.GUI.DataViewers;
using BioLab.Common;
using BioLab.Classification.Unsupervised;
using BioLab.Math.LinearAlgebra;
using PRLab.EsercitazioniVR;
using PRLab.EsercitazioniML;

namespace PRLab
{
    internal partial class FeatureVectorSetViewWithClusteringInfo2DViewer : FeatureVectorSetViewer
    {
        public FeatureVectorSetViewWithClusteringInfo2DViewer()
        {
            InitializeComponent();
        }

        public FeatureVectorSetViewWithClusteringInfo2DViewer(FeatureVectorSet vs, Clusterizer cl)
            : base(vs)
        {
            clusterizer = cl;
            InitializeComponent();
        }


        /// <summary>
        /// Se non è null, visualizza la lista dei centri precedenti (per il k-means)
        /// </summary>
        private List<Point[]> previousCentroids = new List<Point[]>();

        bool ShowPreviousCentroids
        {
            get { return previousCentroids != null; }
            set
            {
                if (value)
                {
                    if (previousCentroids == null)
                        previousCentroids = new List<Point[]>();
                }
                else previousCentroids = null;
            }
        }

        protected override void DoPaint(Graphics gr)
        {
            base.DoPaint(gr);
            // Disegna informazioni aggiuntive specifiche per il tipo di clusterizer
            if (clusterizer is IterativeClusterizer)
            {
                int numIter = ((IterativeClusterizer)clusterizer).CurrentIteration;
                if (numIter <= 0)
                {
                    if (previousCentroids != null)
                        previousCentroids.Clear();
                }

                if (numIter >= 0)
                {
                    // Scrive il numero dell'ultima iterazione
                    //Font font = new Font("Arial", 10);
                    //gr.DrawString(numIter > 0 ? string.Format("Iterazione {0}", numIter) : "Inizializzazione", font, Brushes.Black, 0, 0);

                    if (clusterizer is KMeanClustering)
                    {
                        DrawKMeansCentroids(gr);
                    }
                    else if (clusterizer is EMClustering)
                    {
                        DrawEMMixture(gr);
                    }
                    else if (clusterizer is VR_KMeanClustering)
                    {
                        DrawVRKMeansCentroids(gr);
                    }
                    else if (clusterizer is MyKMeansClustering)
                    {
                        DrawMyKMeansCentroids(gr);
                    }
                }

            }
        }

        #region Membri privati
        Clusterizer clusterizer;

        private void DrawMyKMeansCentroids(Graphics gr)
        {
            var cl = (MyKMeansClustering)clusterizer;
            // Disegna i centri
            Size size = new Size(9, 9);
            Size size2 = new Size(size.Width / 2, size.Height / 2);
            Point[] centroids = new Point[cl.ClusterCount];
            for (int i = 0; i < cl.ClusterCount; i++)
                centroids[i] = FeaturesToDevicePoint(cl.GetCentroid(i));

            if (previousCentroids != null)
            {
                // aggiunge i nuovi centri
                previousCentroids.Add(centroids);
                // Collega i centri con linee
                for (int i = 0; i < cl.ClusterCount; i++)
                {
                    Pen pen = new Pen(LabelColors.GetSafeColor(i));
                    Point p = new Point(0, 0);
                    bool first = true;
                    foreach (Point[] pAr in previousCentroids)
                    {
                        if (first)
                            first = false;
                        else
                            gr.DrawLine(pen, p, pAr[i]);
                        p = pAr[i];
                    }
                }
            }

            // Disegna solo i centri attuali
            for (int i = 0; i < cl.ClusterCount; i++)
            {
                Rectangle rect = new Rectangle(centroids[i] - size2, size);
                gr.FillRectangle(new SolidBrush(LabelColors.GetSafeColor(i)), rect);
                gr.DrawRectangle(Pens.Black, rect);
            }
        }

        private void DrawVRKMeansCentroids(Graphics gr)
        {
            var cl = (VR_KMeanClustering)clusterizer;
            // Disegna i centri
            Size size = new Size(9, 9);
            Size size2 = new Size(size.Width / 2, size.Height / 2);
            Point[] centroids = new Point[cl.ClusterCount];
            for (int i = 0; i < cl.ClusterCount; i++)
                centroids[i] = FeaturesToDevicePoint(cl.GetCentroid(i));

            if (previousCentroids != null)
            {
                // aggiunge i nuovi centri
                previousCentroids.Add(centroids);
                // Collega i centri con linee
                for (int i = 0; i < cl.ClusterCount; i++)
                {
                    Pen pen = new Pen(LabelColors.GetSafeColor(i));
                    Point p = new Point(0, 0);
                    bool first = true;
                    foreach (Point[] pAr in previousCentroids)
                    {
                        if (first)
                            first = false;
                        else
                            gr.DrawLine(pen, p, pAr[i]);
                        p = pAr[i];
                    }
                }
            }

            // Disegna solo i centri attuali
            for (int i = 0; i < cl.ClusterCount; i++)
            {
                Rectangle rect = new Rectangle(centroids[i] - size2, size);
                gr.FillRectangle(new SolidBrush(LabelColors.GetSafeColor(i)), rect);
                gr.DrawRectangle(Pens.Black, rect);
            }
        }

        private void DrawKMeansCentroids(Graphics gr)
        {
            var cl = (KMeanClustering)clusterizer;
            // Disegna i centri
            Size size = new Size(9, 9);
            Size size2 = new Size(size.Width / 2, size.Height / 2);
            Point[] centroids = new Point[cl.ClusterCount];
            for (int i = 0; i < cl.ClusterCount; i++)
            {
                centroids[i] = FeaturesToDevicePoint(cl.GetCentroid(i));
            }

            if (previousCentroids != null)
            {
                // aggiunge i nuovi centri
                previousCentroids.Add(centroids);
                // Collega i centri con linee
                for (int i = 0; i < cl.ClusterCount; i++)
                {
                    Pen pen = new Pen(LabelColors.GetSafeColor(i));
                    Point p = new Point(0, 0);
                    bool first = true;
                    foreach (var pAr in previousCentroids)
                    {
                        if (first)
                        {
                            first = false;
                        }
                        else
                        {
                            gr.DrawLine(pen, p, pAr[i]);
                        }

                        p = pAr[i];
                    }
                }
            }

            // Disegna solo i centri attuali
            for (int i = 0; i < cl.ClusterCount; i++)
            {
                Rectangle rect = new Rectangle(centroids[i] - size2, size);
                gr.FillRectangle(new SolidBrush(LabelColors.GetSafeColor(i)), rect);
                gr.DrawRectangle(Pens.Black, rect);
            }
        }

        /// <summary>
        /// Draws the EM mixture.
        /// </summary>
        /// <param name="gr">The gr.</param>
        private void DrawEMMixture(Graphics gr)
        {
            var cl = (EMClustering)clusterizer;

            if (cl.NormalMixture != null)
            {
                var numDist = cl.NormalMixture.Alfa.Length;

                Size size = new Size(9, 9);
                Size size2 = new Size(size.Width / 2, size.Height / 2);
                for (int i = 0; i < numDist; i++)
                {
                    if (cl.NormalMixture.Theta[i].Invalid)
                        continue; // inutile cercare di disegnarla: è degenerata
                    // Disegna la media (facile)
                    Point centerP = FeaturesToDevicePoint(cl.NormalMixture.Theta[i].Mean);
                    Rectangle rect = new Rectangle(centerP - size2, size);
                    gr.FillRectangle(new SolidBrush(LabelColors.GetSafeColor(i)), rect);
                    gr.DrawRectangle(Pens.Black, rect);

                    // Disegna le ellissi corrispondenti alla matrice di covarianza (più difficile)
                    // Nel caso 2D si potrebbero derivare analiticamente le formule, ma è più elegante risolvere
                    // il problema in generale con la whitening transform (al contrario)
                    //Svd svd = new Svd(cl.NormalMixture.Theta[i].Covariance, true);
                    //Matrix P = svd.U();
                    //Matrix D = svd.W();

                    var svd = new SvdDecomposition(cl.NormalMixture.Theta[i].Covariance);
                    svd.Run();
                    var P = svd.U;
                    var s = svd.S;
                    var D = new Matrix(s.Count);
                    for (int j = 0; j < s.Count; j++)
                        D[j, j] = Math.Sqrt(s[j]);
                    P = P.Multiply(D); // Matrice di trasformazione: P*D^(1/2)

                    // Ora si possono generare delle semplici semplici sfere centrate nell'origine e trasformarle nelle corrispondenti
                    // iper-ellissi con la matrice P e la media cl.Θ.θ[i].μ
                    double[] isoP = { 1, 2, 3 }; // i raggi (corrispondenti ai classici σ, 2σ, e 3σ del caso monodimensionale)
                    for (int k = isoP.Length - 1; k >= 0; k--)
                    {
                        // genera punti sulla circonferenza di raggio p (specifico per 2D)
                        double p = isoP[k];
                        const int nSteps = 16;
                        const double step = 2 * Math.PI / nSteps;
                        double angle = 0;
                        var points = new Vector[nSteps];
                        for (int j = 0; j < nSteps; j++, angle += step)
                        {
                            points[j] = new Vector(2);
                            points[j][0] = Math.Cos(angle) * p;
                            points[j][1] = Math.Sin(angle) * p;
                        }

                        // trasforma i punti
                        for (int j = 0; j < nSteps; j++)
                            points[j] = P * points[j] + cl.NormalMixture.Theta[i].Mean;

                        // trasforma in pixel
                        Point[] dPoints = new Point[nSteps];
                        try // si potrebbe verificare overflow nella conversione a coordinate pixel in teoria
                        {
                            for (int j = 0; j < nSteps; j++)
                                dPoints[j] = FeaturesToDevicePoint(points[j]);
                        }
                        catch (OverflowException)
                        {
                            // meglio non disegnare e passare al seguente
                            continue;
                        }

                        // infine disegna le ellissi

                        // Versione con contorno ellissi
                        //int alpha = 255 * (isoP.Length - k) / isoP.Length;
                        //gr.DrawClosedCurve(new Pen(Color.FromArgb(alpha, VALabelColors.GetSafeColor(i)), (isoP.Length - k)), dPoints);

                        // Versione con superfici ellissi
                        int alpha = 64 * (isoP.Length - k) / isoP.Length;
                        gr.FillClosedCurve(new SolidBrush(Color.FromArgb(alpha, LabelColors.GetSafeColor(i))), dPoints);
                    }
                }
            }
        }


        #endregion

    }
}

