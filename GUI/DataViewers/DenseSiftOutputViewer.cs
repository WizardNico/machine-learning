﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BioLab.GUI.DataViewers;
using BioLab.GUI.Forms;
using BioLab.Common;
using PRLab.EsercitazioniVR;
using BioLab.Math.Geometry;
using System.Drawing.Drawing2D;

namespace PRLab.GUI.DataViewers
{
    public partial class DenseSiftOutputViewer : ImageViewer, IAlgorithmPreviewOutput
    {
        private VR_SiftKeyPoint[] siftKeyPoints;
        private int selectedKeyPointIndex = -1;
        private Point2D[,] gridPoints;
        private Point2D[,] centerPoints;
        private Point2D[] angleTranslations;
        private double maxKeyPointsValue;

        private int descriptorDimension;
        private int descriptorDirectionsCount;
        private int regionSize;

        public DenseSiftOutputViewer()
        {
            InitializeComponent();
        }

        public void UpdateOutputViewer(IAlgorithm algorithm)
        {
            var ds = (VR_DenseSift)algorithm;
            siftKeyPoints = ds.Result;
            Image = ds.InputImage;
            descriptorDimension = ds.DescriptorDimension;
            descriptorDirectionsCount = ds.DescriptorDirectionsCount;
            regionSize = (int)(ds.Radius * Math.Sqrt(2));
            UpdateInternalData();

            Invalidate();
        }

        private void UpdateInternalData()
        {
            var gridSize = descriptorDimension + 1;
            gridPoints = new Point2D[gridSize, gridSize];
            var gridStep = (double)regionSize / descriptorDimension;
            var x1 = -regionSize / 2.0;
            var y1 = x1;
            for (int i = 0; i < gridSize; i++, y1 += gridStep)
            {
                var x = x1;
                for (int j = 0; j < gridSize; j++, x += gridStep)
                {
                    gridPoints[i, j] = new Point2D(x, y1);
                }
            }

            centerPoints = new Point2D[descriptorDimension, descriptorDimension];
            var gridStep2 = gridStep / 2.0;
            x1 = gridPoints[0, 0].X + gridStep2;
            y1 = x1;
            for (int i = 0; i < descriptorDimension; i++, y1 += gridStep)
            {
                var x = x1;
                for (int j = 0; j < descriptorDimension; j++, x += gridStep)
                {
                    centerPoints[i, j] = new Point2D(x, y1);
                }
            }

            angleTranslations = new Point2D[descriptorDirectionsCount];
            var angleStep = BioLab.Math.Constants.DoublePI / descriptorDirectionsCount;
            var a = 0.0;
            var arrowMaxLength = gridStep;
            for (int i = 0; i < descriptorDirectionsCount; i++, a += angleStep)
            {
                angleTranslations[i] = new Point2D(Math.Cos(a) * arrowMaxLength, Math.Sin(a) * arrowMaxLength);
                //angleTranslations[i] = new Point2D(-Math.Cos(a) * arrowMaxLength, Math.Sin(a) * arrowMaxLength);
            }

            maxKeyPointsValue = double.MinValue;
            foreach (var p in siftKeyPoints)
            {
                for (int i = 0; i < p.Descriptor.Count; i++)
                {
                    if (p.Descriptor[i] > maxKeyPointsValue)
                    {
                        maxKeyPointsValue = p.Descriptor[i];
                    }
                }
            }
        }

        private const int pointSize = 6;
        private const int pointSize2 = pointSize / 2;
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (siftKeyPoints != null &&
                siftKeyPoints.Length > 0)
            {
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

                AdjustGraphicsToWorldUnits(e.Graphics);

                for (int i = 0; i < siftKeyPoints.Length; i++)
                {
                    var s = siftKeyPoints[i];

                    e.Graphics.DrawEllipse(Pens.Green, s.Position.X - pointSize2, s.Position.Y - pointSize2, pointSize, pointSize);


                    //var or = s.Orientation + Math.PI;
                    var or = s.Orientation;
                    if (Math.Abs(Math.PI / 2 - or) < Math.PI / 4)
                        or += Math.PI;

                    var xa = s.Position.X + (int)Math.Round(Math.Cos(or) * pointSize * 1.5, MidpointRounding.AwayFromZero);
                    var ya = s.Position.Y - (int)Math.Round(Math.Sin(or) * pointSize * 1.5, MidpointRounding.AwayFromZero);
                    e.Graphics.DrawLine(Pens.Green, s.Position.X, s.Position.Y, xa, ya);
                }

                if (selectedKeyPointIndex != -1)
                {
                    DrawKeyPointDescriptor(siftKeyPoints[selectedKeyPointIndex], e.Graphics, Pens.Green);
                }
            }
        }

        private void DrawKeyPointDescriptor(VR_SiftKeyPoint keyPoint, Graphics gr, Pen pen)
        {
            var gridSize = gridPoints.GetLength(0);

            // var or = keyPoint.Orientation + Math.PI;
            var or = keyPoint.Orientation;
            if (Math.Abs(Math.PI / 2 - or) < Math.PI / 4)
                or += Math.PI;

            for (int i = 0; i < gridSize; i++)
            {
                gr.DrawLine(pen,
                            gridPoints[0, i].MapTo(1, or, keyPoint.Position.X, keyPoint.Position.Y).ToPointF(),
                            gridPoints[gridSize - 1, i].MapTo(1, or, keyPoint.Position.X, keyPoint.Position.Y).ToPointF());
                gr.DrawLine(pen,
                            gridPoints[i, 0].MapTo(1, or, keyPoint.Position.X, keyPoint.Position.Y).ToPointF(),
                            gridPoints[i, gridSize - 1].MapTo(1, or, keyPoint.Position.X, keyPoint.Position.Y).ToPointF());
            }

            var arrowPen = (Pen)pen.Clone();
            arrowPen.StartCap = LineCap.Square;
            arrowPen.EndCap = LineCap.ArrowAnchor;
            var k = 0;
            for (int i = 0; i < descriptorDimension; i++)
            {
                for (int j = 0; j < descriptorDimension; j++)
                {
                    var maxV = double.MinValue;
                    var maxIdx = -1;
                    for (int t = 0; t < descriptorDirectionsCount; t++, k++)
                    {
                        if (keyPoint.Descriptor[k] > maxV)
                        {
                            maxV = keyPoint.Descriptor[k];
                            maxIdx = t;
                        }
                    }

                    var p = new Point2D(centerPoints[i, j].X, centerPoints[i, j].Y);
                    var normMaxV = maxV / maxKeyPointsValue;
                    p.Offset(angleTranslations[maxIdx].X * normMaxV, angleTranslations[maxIdx].Y * normMaxV);

                    gr.DrawLine(arrowPen,
                                centerPoints[i, j].MapTo(1, or, keyPoint.Position.X, keyPoint.Position.Y).ToPointF(),
                                p.MapTo(1, or, keyPoint.Position.X, keyPoint.Position.Y).ToPointF());
                }
            }
        }

        private void DenseSiftOutputViewer_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (siftKeyPoints != null &&
                    siftKeyPoints.Length > 0)
                {
                    selectedKeyPointIndex = KeyPointHitTest(siftKeyPoints, e.Location);
                    Invalidate();
                }
            }
        }

        public int KeyPointHitTest(VR_SiftKeyPoint[] keyPoints, Point position)
        {
            double sizeQ = pointSize * pointSize;
            var worldPosition = Point.Round(DeviceToWorld(position));
            var mouseLocation = new IntPoint2D(worldPosition.X, worldPosition.Y);
            double minDist = double.MaxValue;
            int minIndex = -1;
            for (int i = 0; i < keyPoints.Length; i++)
            {
                var dist = IntPoint2D.CalculateSquareEuclideanDistance(keyPoints[i].Position, mouseLocation);
                if (dist < minDist)
                {
                    minDist = dist;
                    minIndex = i;
                }
            }
            return minDist < sizeQ ? minIndex : -1;
        }
    }
}
