using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using BioLab.GUI.DataViewers;
using PRLab.EsercitazioniVR;
using BioLab.Math.Geometry;

namespace PRLab
{
  internal partial class ImageWithMarkedRegionsViewer : ImageViewer
  {
    //private bool modifying = true;
    private bool addingPoints = true;
    private MarkedRegion currentRegion = null;

    public ImageWithMarkedRegionsViewer()
    {
      InitializeComponent();

      Modifying = true;
    }

    public ImageWithMarkedRegionsViewer(ImageWithMarkedRegions imgMR)
    //  : base(imgMR)
      :this()
    {
      //InitializeComponent();

      SetImageWithMarkedRegions(imgMR);

      //if (imgMR.Regions.Count > 0)
      //{
      //  if (!imgMR.Regions[imgMR.Regions.Count - 1].Closed)
      //  {
      //    currentRegion = imgMR.Regions[imgMR.Regions.Count - 1];
      //  }
      //}
      //addingPoints = true;
    }

    public bool Modifying { get; set; }

    public void SetImageWithMarkedRegions(ImageWithMarkedRegions imgMR)
    {
      Image = imgMR;

      if (imgMR!=null &&
          imgMR.Regions.Count > 0)
      {
        if (!imgMR.Regions[imgMR.Regions.Count - 1].Closed)
        {
          currentRegion = imgMR.Regions[imgMR.Regions.Count - 1];
        }
      }
    }

    //public override void UpdateView(Data data)
    //{
    //    Data = data;
    //    currentRegion = null;
    //    if (ImageMR.Regions.Count > 0)
    //    {
    //        if (!ImageMR.Regions[ImageMR.Regions.Count - 1].Closed)
    //            currentRegion = ImageMR.Regions[ImageMR.Regions.Count - 1];
    //    }
    //    addingPoints = true;
    //    imageView.UpdateView(ImageMR.Image);
    //}

    //public override bool IsEditSupported
    //{
    //    get
    //    {
    //        return true;
    //    }
    //}

    //public new bool EditMode
    //{
    //  get
    //  {
    //    return modifying;
    //  }
    //  set
    //  {
    //    if (modifying != value)
    //    {
    //      modifying = value;
    //    }
    //  }
    //}

    //#region Disegno

    private IntPoint2D DP2LPc(Point p)
    {
      //Point p1 = DP2LP(p);
      var p1 = DeviceToWorld(p);
      return (IntPoint2D) new Point2D(p1.X, p1.Y);
    }

    //private Point LP2DPc(IntPoint2D p)
    //{
    //  return imageViewer.WorldToDevice(p.ToPointF());
    //    //return imageView.LP2DP(new Point(p.X,p.Y));
    //}

    //void imageView_CustomPaintEvent(Graphics g, Rectangle rectClip)
    //{
    //    g.SetClip(rectClip);
    //    foreach (MarkedRegion r in ImageMR.Regions)
    //    {
    //        // Converte in coordinate video
    //        Point[] dPoints = new Point[r.Points.Count];
    //        for (int i = 0; i < dPoints.Length; i++)
    //            dPoints[i] = LP2DPc(r.Points[i]);
    //        // Disegna i segmenti
    //        if (dPoints.Length > 1)
    //        {
    //            if (r.Closed)
    //                g.DrawPolygon(Pens.Blue, dPoints);
    //            else g.DrawLines(Pens.Red, dPoints);
    //        }
    //        // Disegna i punti
    //        const int pointSize = 5;
    //        Size size = new Size(pointSize, pointSize);
    //        Size size2 = new Size(pointSize / 2, pointSize / 2);
    //        foreach (Point p in dPoints)
    //        {
    //            Rectangle rect = new Rectangle(p - size2, size);
    //            g.DrawRectangle(Pens.Blue, rect);
    //        }
    //    }
    //}
    //#endregion

    private bool CanCloseCurrentRegion()
    {
      return currentRegion != null && !currentRegion.Closed && currentRegion.Points.Count > 2;
    }
    
    private void cntxMenu_Opening(object sender, CancelEventArgs e)
    {
      addPointToolStripMenuItem.Checked = Modifying && addingPoints;
      closeRegionToolStripMenuItem.Enabled = Modifying && CanCloseCurrentRegion();
      delRegionToolStripMenuItem.Checked = Modifying && !addingPoints;
    }

    void imageView_MouseClick(object sender, MouseEventArgs e)
    {
      if (Modifying &&
          e.Button == MouseButtons.Left)
      {
        var imgMR = (ImageWithMarkedRegions)Data;

        if (addingPoints)
        {
          if (currentRegion == null)
          {
            // crea una nuova regione
            currentRegion = new MarkedRegion();
            imgMR.Regions.Add(currentRegion);
          }
          currentRegion.Points.Add(DP2LPc(e.Location));
          Invalidate();
        }
        else
        {
          // elimina regione pi� vicina
          double dMin = double.MaxValue;
          //Point imgPoint = imageView.DP2LP(e.Location);
          var imgPoint = DeviceToWorld(e.Location);
          MarkedRegion rMin = null;
          foreach (var r in imgMR.Regions)
          {
            foreach (var p in r.Points)
            {
              double d = (double)(p.X - imgPoint.X) * (p.X - imgPoint.X) + (double)(p.Y - imgPoint.Y) * (p.Y - imgPoint.Y);
              if (d < dMin)
              {
                dMin = d;
                rMin = r;
              }
            }
          }
          if (rMin != null)
          {
            if (currentRegion == rMin)
              currentRegion = null;
            imgMR.Regions.Remove(rMin);
            Invalidate();
          }
        }
      }
    }

    private void addPointToolStripMenuItem_Click(object sender, EventArgs e)
    {
      addingPoints = true;
    }

    private void closeRegionToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (Modifying && CanCloseCurrentRegion())
      {
        currentRegion.Closed = true;
        currentRegion = null;
        Invalidate();
      }
    }

    private void delRegionToolStripMenuItem_Click(object sender, EventArgs e)
    {
      addingPoints = false;
    }

    private const int pointSize = 6;
    private const int pointSize2 = pointSize / 2;
    private void ImageWithMarkedRegionsViewer_Paint(object sender, PaintEventArgs e)
    {
      if (Image == null)
        return;
      var imgMR = (ImageWithMarkedRegions)Image;
      AdjustGraphicsToWorldUnits(e.Graphics);
      foreach (var r in imgMR.Regions)
      {
        // Disegna i segmenti
        if (r.Points.Count > 1)
        {
          var points = new Point[r.Points.Count];
          for (int i = 0; i < r.Points.Count; i++)
          {
            points[i] = r.Points[i].ToPoint();
          }
          if (r.Closed)
          {
            e.Graphics.DrawPolygon(Pens.Blue, points);
          }
          else
          {
            e.Graphics.DrawLines(Pens.Red, points);
          }
        }
        // Disegna i punti
        foreach (var p in r.Points)
        {
          var rect = new System.Drawing.Rectangle(p.X - pointSize2, p.Y - pointSize2, pointSize, pointSize);
          e.Graphics.DrawRectangle(Pens.Blue, rect);
        }
      }
    }
  }
}

