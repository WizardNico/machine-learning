﻿namespace PRLab.GUI.DataViewers
{
	partial class ColorSegmentationOutputViewer
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.imageViewer = new BioLab.GUI.DataViewers.ImageViewer();
      this.SuspendLayout();
      // 
      // imageViewer
      // 
      this.imageViewer.BackColor = System.Drawing.SystemColors.Control;
      this.imageViewer.Dock = System.Windows.Forms.DockStyle.Fill;
      this.imageViewer.Location = new System.Drawing.Point(0, 0);
      this.imageViewer.Name = "imageViewer";
      this.imageViewer.Size = new System.Drawing.Size(330, 277);
      this.imageViewer.TabIndex = 0;
      // 
      // ColorSegmentationOutputViewer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.imageViewer);
      this.Name = "ColorSegmentationOutputViewer";
      this.Size = new System.Drawing.Size(330, 277);
      this.ResumeLayout(false);

		}

		#endregion

    private BioLab.GUI.DataViewers.ImageViewer imageViewer;
	}
}
