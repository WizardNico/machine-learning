﻿
namespace PRLab
{
  using System;
  using System.Collections.Generic;
  using System.Drawing;
  using System.Windows.Forms;
  using BioLab.GUI.Forms;
  using BioLab.Common;
  using BioLab.PatternRecognition.Localization;
  using PRLab.EsercitazioniVR;

  public partial class VR_HoughLinesDetectorOutputViewer : UserControl, IAlgorithmPreviewOutput
  {
    private List<Point[]> lines;

    public VR_HoughLinesDetectorOutputViewer()
    {
      InitializeComponent();
    }

    public void UpdateOutputViewer(IAlgorithm algorithm)
    {
      var a = (VR_HoughLinesDetector)algorithm;
      lines = new List<Point[]>();

      var w = a.InputImage.Width;
      var h = a.InputImage.Height;
      foreach (var p in a.Result)
      {
        double ro = p.Ro;
        double theta = p.Theta;
        double sinAbs = Math.Abs(Math.Sin(theta));
        double cos = Math.Cos(theta);
        double sin = Math.Sin(theta);
        int y1;
        int y2;
        int x1;
        int x2;
        Point[] point;

        if (sinAbs > 0.001)
        {
          x1 = 0;
          x2 = w - 1;
          y1 = (int)((ro - x1 * cos) / sin);
          y2 = (int)((ro - x2 * cos) / sin);
          if (y1 < 0)
          {
            y1 = 0;
            x1 = (int)((ro - y1 * sin) / cos);
          }
          else if (y1 > h)
          {
            y1 = h - 1;
            x1 = (int)((ro - y1 * sin) / cos);
          }
          if (y2 < 0)
          {
            y2 = 0;
            x2 = (int)((ro - y2 * sin) / cos);
          }
          else if (y2 > h)
          {
            y2 = h - 1;
            x2 = (int)((ro - y2 * sin) / cos);
          }

          point = new Point[] { new Point(x1, y1), new Point(x2, y2) };
        }
        else
        {
          x1 = (int)((ro - 0 * sin) / cos);
          x2 = (int)((ro - (h - 1) * sin) / cos);
          point = new Point[] { new Point(x1, 0), new Point(x2, h - 1) };
        }

        lines.Add(point);
      }

      imageViewerGradientModuli.Image = a.GradientModuli;
      imageViewerBinarizedGradientModuli.Image = a.BinarizedGradientModuli;
      imageViewerResult.Image = a.InputImage;
      imageViewerResult.Invalidate();
    }

    private void imageViewerResult_Paint(object sender, PaintEventArgs e)
    {
      if (lines != null)
      {
        foreach (var l in lines)
        {
          e.Graphics.DrawLine(Pens.Red,
                              imageViewerResult.WorldToDevice(l[0]),
                              imageViewerResult.WorldToDevice(l[1]));
        }
      }
    }
  }
}
