﻿namespace PRLab.GUI.DataViewers
{
  partial class DenseSiftOutputViewer
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.SuspendLayout();
      // 
      // DensSiftOutputViewer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.LeftMouseButtonTool = BioLab.GUI.DataViewers.MouseButtonTool.None;
      this.Name = "DensSiftOutputViewer";
      this.Size = new System.Drawing.Size(485, 511);
      this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DenseSiftOutputViewer_MouseClick);
      this.ResumeLayout(false);

    }

    #endregion
  }
}
