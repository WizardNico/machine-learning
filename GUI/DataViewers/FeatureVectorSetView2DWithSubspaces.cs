using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BioLab.GUI.DataViewers;
using BioLab.Common;
using BioLab.Math.LinearAlgebra;

namespace PRLab
{
    internal partial class FeatureVectorSetView2DWithSubspaces : FeatureVectorSetViewer
    {
        public FeatureVectorSetView2DWithSubspaces()
        {
            InitializeComponent();
        }

        public FeatureVectorSetView2DWithSubspaces(FeatureVectorSet vs)
            : base(vs)
        {
            InitializeComponent();
        }

        public int NumSubspaces { get { return subspaces.Count; } }

        public void AddSubspace(Vector origin, Matrix baseVectors, int colorIndex)
        {
            subspaces.Add(new SubspaceData(origin, baseVectors.Transpose(), colorIndex));
            Invalidate();
        }
        
        public void RemoveAllSubspaces()
        {
            subspaces.Clear();
            Invalidate();
        }

        #region Membri protetti
        protected override void DoPaint(Graphics gr)
        {
            // Disegna i sottospazi
            foreach (SubspaceData sd in subspaces)
            {
                // disegna la retta corrispondente al primo vettore nella base e l'origine
                DrawSubspaceLine(gr, sd.Origin[0], sd.Origin[1], sd.Base[0, 0], sd.Base[1, 0], LabelColors.Colors[sd.colorIdx]);
            }
            // Disegna il resto
            base.DoPaint(gr);
        }

        #endregion

        #region Membri privati

        private void DrawSubspaceLine(Graphics gr, double cx, double cy, double dx, double dy, Color color)
        {
            const double eps = double.Epsilon;
            var v1 = new Vector(2);
            var v2 = new Vector(2);

            if (Math.Abs(dx) < eps)
            {
                v1[0] = v2[0] = cx;
                v1[1] = WorldRectangle.Top;// Y1;
                v2[1] = WorldRectangle.Bottom;// Y2;

            }
            else if (Math.Abs(dy) < eps)
            {
                v1[0] = WorldRectangle.Left;//X1;
                v2[0] = WorldRectangle.Right;//X2;
                v1[1] = v2[1] = cy;
            }
            else
            {
                double m = dy / dx;
                if (m > 1 || m < -1)
                {
                    v1[1] = WorldRectangle.Top;//Y1;
                    v2[1] = WorldRectangle.Bottom;//Y2;
                    v1[0] = (v1[1] - cy) / m + cx;
                    v2[0] = (v2[1] - cy) / m + cx;
                }
                else
                {
                    v1[0] = WorldRectangle.Left;//X1;
                    v2[0] = WorldRectangle.Right;//X2;
                    v1[1] = (v1[0] - cx) * m + cy;
                    v2[1] = (v2[0] - cx) * m + cy;
                }
            }
            gr.DrawLine(new Pen(color), FeaturesToDevicePoint(v1), FeaturesToDevicePoint(v2));
        }



        /// <summary>
        /// I dati di un sottospazio
        /// </summary>
        class SubspaceData
        {
            public Vector Origin;
            public Matrix Base;
            public int colorIdx;

            public SubspaceData(Vector o, Matrix b, int c)
            {
                Origin = o;
                Base = b;
                colorIdx = c;
            }
        }

        List<SubspaceData> subspaces = new List<SubspaceData>();

        #endregion
    }
}

