namespace PRLab
{
    partial class FeatureVectorSetWithClassSurfaces2DViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.workerThread = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // workerThread
            // 
            this.workerThread.WorkerReportsProgress = true;
            this.workerThread.WorkerSupportsCancellation = true;
            this.workerThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerThread_DoWork);
            this.workerThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.workerThread_RunWorkerCompleted);
            this.workerThread.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.workerThread_ProgressChanged);
            // 
            // VAFeatureVectorSetView2DWithClassSurfaces
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "VAFeatureVectorSetView2DWithClassSurfaces";
            this.Size = new System.Drawing.Size(329, 305);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.VAFeatureVectorSetView2DWithClassSurfaces_MouseDoubleClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker workerThread;
    }
}
