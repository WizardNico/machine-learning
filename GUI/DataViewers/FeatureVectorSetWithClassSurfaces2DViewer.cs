using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Imaging;
using BioLab.GUI.DataViewers;
using BioLab.Classification.DensityEstimation;
using BioLab.Classification.Supervised;
using BioLab.Common;
using System.Diagnostics;
using BioLab.Math.LinearAlgebra;
using System.Collections.Generic;

namespace PRLab
{
    internal partial class FeatureVectorSetWithClassSurfaces2DViewer : FeatureVectorSetViewer
    {
        #region Membri privati
        const int gridStep = 5;
        Classifier theClassifier = null;
        DensityEstimator theDensityEstimator = null;
        Bitmap backBmp = null;
        Bitmap workerThreadBmp = null;
        bool restartThread = false;
        bool workCompleted = true;
        int lastPercDone;
        #endregion

        public event EventHandler<EventArgs> UpdateViewFinished;

        public FeatureVectorSetWithClassSurfaces2DViewer()
        {
            InitializeComponent();
        }

        public FeatureVectorSetWithClassSurfaces2DViewer(FeatureVectorSet vs, Classifier classifier, DensityEstimator densityEstimator)
            : base(vs)
        {
            InitializeComponent();
            theClassifier = classifier;
            theDensityEstimator = densityEstimator;
        }

        public Classifier Classifier
        {
            get { return theClassifier; }
            set { UpdateViewEx((FeatureVectorSet)Data, value, theDensityEstimator); }
        }

        public DensityEstimator DensityEstimator
        {
            get { return theDensityEstimator; }
            set { UpdateViewEx((FeatureVectorSet)Data, theClassifier, value); }
        }


        protected override void DoPaint(Graphics gr)
        {
            if (backBmp != null)
            {
                gr.DrawImage(backBmp, ClientRectangle);
            }
            if (!workCompleted)
            {
                Font font = new Font("Arial", 10, GraphicsUnit.Pixel);
                gr.DrawString(string.Format("Disegno superfice in corso: {0}%", lastPercDone), font, Brushes.Black, new PointF(0, 0));
            }
            base.DoPaint(gr);

            OnUpdateViewFinished(null);
        }

        private void workerThread_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Debug.WriteLine(string.Format("Thread {0} - {1} avviato", Thread.CurrentThread.ManagedThreadId, sender.GetHashCode()));
                var thread = (BackgroundWorker)sender;
                //if (!LayoutFailed && (theClassifier != null || theDensityEstimator != null))
                if ((theClassifier != null ||
                      theDensityEstimator != null))
                {
                    Debug.Assert(workerThreadBmp != null);
                    Rectangle clientRect = (Rectangle)e.Argument;   // il ClientRect � passato allo thread come parametro, perch� potrebbe cambiare durante la sua esecuzione...
                    int border = gridStep / 2;
                    double probMax = 0; // solo per prob. est.
                    bool firstStep = true;
                    for (; ; )
                    {
                        var p = new Point();
                        int i = 0;
                        int lastPerc = 0;
                        for (p.Y = border; p.Y < clientRect.Height - border; p.Y += gridStep, i++)
                        {
                            int j = 0;
                            for (p.X = border; p.X < clientRect.Width - border; p.X += gridStep, j++)
                            {
                                var v = DevicePointToFeatures(p);
                                var color = Color.Black;
                                if (theClassifier != null)
                                {
                                    int alpha = 64;
                                    int c;
                                    // Classifica
                                    if (theClassifier.HasConfidence)
                                    {
                                        double[] arConf;
                                        c = theClassifier.Classify(v, out arConf);

                                        //???
                                        //if (c!=theClassifier.Classify(v))
                                        //{
                                        //    throw new Exception();
                                        //}
                                        //???

                                        Debug.Assert(arConf[c] >= 0);
                                        alpha = Math.Max(Math.Min(200, (int)(arConf[c] * 128)), 10);  // max 230 (meglio non fidarsi delle confidenze, anche se dovrebbero sempre essere fra 0 e 1)
                                    }
                                    else c = theClassifier.Classify(v);
                                    color = c >= 0 ? Color.FromArgb(alpha, LabelColors.Colors[c % LabelColors.Colors.Length]) : Color.Black;
                                }
                                else if (theDensityEstimator != null)
                                {
                                    var classConditionalDensities = theDensityEstimator.Estimate();

                                    double mR = 0, mG = 0, mB = 0, mA = 0;
                                    int alphaMax = 0;
                                    for (int probEstClassIndex = 0; probEstClassIndex < classConditionalDensities.Length; probEstClassIndex++)
                                    {
                                        // Stima della densit� di probabilit� condizionata
                                        double prob = classConditionalDensities[probEstClassIndex].CalculateDensity(v);
                                        if (firstStep)
                                        {
                                            if (prob > probMax)
                                                probMax = prob;
                                        }
                                        else
                                        {
                                            if (probMax > 0)
                                                prob /= probMax; // normalizza sempre rispetto al massimo
                                            int alpha = Math.Min((int)(prob * 300), 255); //Matteo: con 196 il colore era troppo sbiadito, meglio 300
                                            Color c = Color.FromArgb(alpha, LabelColors.Colors[probEstClassIndex % LabelColors.Colors.Length]);
                                            mR += (double)c.R * alpha;
                                            mB += (double)c.B * alpha;
                                            mG += (double)c.G * alpha;
                                            mA += alpha;
                                            if (alphaMax < alpha)
                                                alphaMax = alpha;
                                        }
                                    }
                                    if (!firstStep)
                                    {
                                        if (mA > 0)
                                        {
                                            mR /= mA;
                                            mG /= mA;
                                            mB /= mA;
                                        }
                                        color = Color.FromArgb(alphaMax, (int)mR, (int)mG, (int)mB);
                                    }
                                }

                                if (theClassifier != null || !firstStep)
                                {
                                    // disegna il pixel sulla bitmap
                                    workerThreadBmp.SetPixel(j, i, color);
                                }
                            }
                            if (thread.CancellationPending)
                            {
                                e.Cancel = true;
                                return;
                            }
                            int perc = i * 100 / workerThreadBmp.Height;
                            if (perc - lastPerc > 5)
                                thread.ReportProgress(lastPerc = perc);
                        }
                        if (theClassifier != null)
                            break; // una sola passata per il caso classificatore
                        else
                        {
                            // caso prob est
                            if (firstStep)
                            {
                                // nella prima passata ha trovato il max, ora disegna la bmp
                                firstStep = false;
                            }
                            else break; // fine seconda passata
                        }
                    }
                }
            }
            finally
            {
                Debug.WriteLine(string.Format("Thread {0} - {1} terminato", Thread.CurrentThread.ManagedThreadId, sender.GetHashCode()));
            }
        }

        //private void workerThread_DoWork(object sender, DoWorkEventArgs e)
        //{
        //  try
        //  {
        //    Debug.WriteLine(string.Format("Thread {0} - {1} avviato", Thread.CurrentThread.ManagedThreadId, sender.GetHashCode()));
        //    var thread = (BackgroundWorker)sender;
        //    //if (!LayoutFailed && (theClassifier != null || theDensityEstimator != null))
        //    if ((theClassifier != null ||
        //          theDensityEstimator != null))
        //    {
        //      Debug.Assert(workerThreadBmp != null);
        //      Rectangle clientRect = (Rectangle)e.Argument;   // il ClientRect � passato allo thread come parametro, perch� potrebbe cambiare durante la sua esecuzione...
        //      int border = gridStep / 2;
        //      double probMax = 0; // solo per prob. est.
        //      bool firstStep = true;
        //      for (; ; )
        //      {
        //        var p = new Point();
        //        int i = 0;
        //        int lastPerc = 0;
        //        for (p.Y = border; p.Y < clientRect.Height - border; p.Y += gridStep, i++)
        //        {
        //          int j = 0;
        //          for (p.X = border; p.X < clientRect.Width - border; p.X += gridStep, j++)
        //          {
        //            var v = DevicePointToFeatures(p);
        //            var color = Color.Black;
        //            if (theClassifier != null)
        //            {
        //              int alpha = 64;
        //              int c;
        //              // Classifica
        //              if (theClassifier.HasConfidence)
        //              {
        //                double[] arConf;
        //                c = theClassifier.Classify(v, out arConf);
        //                Debug.Assert(arConf[c] >= 0);
        //                alpha = Math.Min(230, (int)(arConf[c] * 128));  // max 230 (meglio non fidarsi delle confidenze, anche se dovrebbero sempre essere fra 0 e 1)
        //              }
        //              else c = theClassifier.Classify(v);
        //              color = c >= 0 ? Color.FromArgb(alpha, LabelColors.Colors[c % LabelColors.Colors.Length]) : Color.Black;
        //            }
        //            else if (theDensityEstimator!=null)
        //            {
        //              var classConditionalDensities=theDensityEstimator.Estimate();

        //              double mR = 0, mG = 0, mB = 0, mA = 0;
        //              int alphaMax = 0;
        //              for (int probEstClassIndex = 0; probEstClassIndex < classConditionalDensities.Length; probEstClassIndex++)
        //              {
        //                // Stima della densit� di probabilit� condizionata
        //                double prob = classConditionalDensities[probEstClassIndex].CalculateDensity(v);
        //                if (firstStep)
        //                {
        //                  if (prob > probMax)
        //                    probMax = prob;
        //                }
        //                else
        //                {
        //                  if (probMax > 0)
        //                    prob /= probMax; // normalizza sempre rispetto al massimo
        //                  int alpha = Math.Min((int)(prob * 300),255); //Matteo: con 196 il colore era troppo sbiadito, meglio 300
        //                  Color c = Color.FromArgb(alpha, LabelColors.Colors[probEstClassIndex % LabelColors.Colors.Length]);
        //                  mR += (double)c.R * alpha;
        //                  mB += (double)c.B * alpha;
        //                  mG += (double)c.G * alpha;
        //                  mA += alpha;
        //                  if (alphaMax < alpha)
        //                    alphaMax = alpha;
        //                }
        //              }
        //              if (!firstStep)
        //              {
        //                if (mA > 0)
        //                {
        //                  mR /= mA;
        //                  mG /= mA;
        //                  mB /= mA;
        //                }
        //                color = Color.FromArgb(alphaMax, (int)mR, (int)mG, (int)mB);
        //              }
        //            }

        //            if (theClassifier != null || !firstStep)
        //            {
        //              // disegna il pixel sulla bitmap
        //              workerThreadBmp.SetPixel(j, i, color);
        //            }
        //          }
        //          if (thread.CancellationPending)
        //          {
        //            e.Cancel = true;
        //            return;
        //          }
        //          int perc = i * 100 / workerThreadBmp.Height;
        //          if (perc - lastPerc > 5)
        //            thread.ReportProgress(lastPerc = perc);
        //        }
        //        if (theClassifier != null)
        //          break; // una sola passata per il caso classificatore
        //        else
        //        {
        //          // caso prob est
        //          if (firstStep)
        //          {
        //            // nella prima passata ha trovato il max, ora disegna la bmp
        //            firstStep = false;
        //          }
        //          else break; // fine seconda passata
        //        }
        //      }
        //    }
        //  }
        //  finally
        //  {
        //    Debug.WriteLine(string.Format("Thread {0} - {1} terminato", Thread.CurrentThread.ManagedThreadId, sender.GetHashCode()));
        //  }
        //}

        private void workerThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage != lastPercDone)
            {
                lastPercDone = e.ProgressPercentage;
                Invalidate(new Rectangle(0, 0, ClientRectangle.Width, 10));
            }
        }

        private void workerThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Diagnostics.Debug.Assert(!workCompleted);
            if (e.Error != null)
            {
                MessageBox.Show(string.Format("Errore durante l'aggiornamento in background.\n{0}", e.Error.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (!e.Cancelled)
                {
                    if (!restartThread)
                    {
                        workCompleted = true;
                        backBmp = workerThreadBmp;
                        Invalidate(); // ora � pronto per disegnare tutto
                    }
                }
            }

            if (restartThread)
                StartThread();
        }

        private void OnUpdateViewFinished(EventArgs e)
        {   // Copy to a temporary variable to be thread-safe.
            EventHandler<EventArgs> temp = UpdateViewFinished;
            if (temp != null)
                temp(this, e);
        }

        protected override void CalculateLayout()
        {
            base.CalculateLayout();

            //if (!LayoutFailed && !restartThread && (theClassifier != null || theDensityEstimator != null))
            if (!restartThread && (theClassifier != null || theDensityEstimator != null))
            {
                // chiede allo thread di fare il lavoro                
                if (!workCompleted)
                {
                    if (workerThread.IsBusy)
                    {
                        restartThread = true;
                        workerThread.CancelAsync();
                    }
                }
                else
                {
                    StartThread();
                }
            }
        }

        private void StartThread()
        {
            System.Diagnostics.Debug.Assert(!workerThread.IsBusy);
            // avvia lo thread
            // qui viene ricreato classGrid, ma dato che lo thread non � in esecuzione, non ci sono problemi
            workCompleted = false;
            restartThread = false;
            workerThreadBmp = null;
            int ySteps = ClientRectangle.Height / gridStep;
            int xSteps = ClientRectangle.Width / gridStep;
            if (ySteps <= 0 || xSteps <= 0)
                return; // nulla da fare
            workerThreadBmp = new Bitmap(xSteps, ySteps, PixelFormat.Format32bppArgb);
            workerThread.RunWorkerAsync(ClientRectangle);
        }

        private void StopThread()
        {
            if (workerThread.IsBusy)
            {
                restartThread = false;
                workerThread.CancelAsync();
                bool enabled = ParentForm.Enabled;
                ParentForm.Enabled = false;
                while (workerThread.IsBusy)
                    Application.DoEvents(); // E' necessario per permetterne la terminazione
                ParentForm.Enabled = enabled;
                workCompleted = true; // lo thread � fermo: si torna alla situazione iniziale (come se il lavoro fosse fatto)
            }
        }

        //public override void Cleanup()
        //{
        //    base.Cleanup();
        //    if (!workCompleted)
        //        StopThread();
        //}

        //public override void UpdateView(Data data)
        //{
        //    UpdateViewEx((FeatureVectorSet)data, theClassifier, theDensityEstimator);
        //}

        public override void UpdateViewer()
        {
            UpdateViewEx((FeatureVectorSet)Data, theClassifier, theDensityEstimator);
        }

        public void UpdateViewEx(FeatureVectorSet data, Classifier classifier, DensityEstimator densityEst)
        {
            if (workerThread != null &&
                workerThread.IsBusy)
            {
                StopThread();
            }

            if (Data != data || theClassifier != classifier || theDensityEstimator != densityEst)
            {
                backBmp = null; // se � cambiato il vector set, meglio fare sparire la vecchia superficie subito
            }
            Data = data;
            theClassifier = classifier;
            theDensityEstimator = densityEst;
            base.UpdateViewer();
        }

        private void VAFeatureVectorSetView2DWithClassSurfaces_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // utile per fare debugging dei classificatori provando la classificazione di un punto specifico con il mouse...
            var vs = (FeatureVectorSet)Data;
            if (theClassifier != null && vs.Dim == 2)
            {
                var v = DevicePointToFeatures(e.Location);
                theClassifier.Classify(v);
            }
        }
    }
}

