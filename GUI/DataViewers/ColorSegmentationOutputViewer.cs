﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BioLab.GUI.Forms;
using BioLab.Common;
using PRLab.EsercitazioniVR;
using BioLab.ImageProcessing;

namespace PRLab.GUI.DataViewers
{
  public partial class ColorSegmentationOutputViewer : UserControl, IAlgorithmPreviewOutput
	{
    public ColorSegmentationOutputViewer()
    {
      InitializeComponent();
    }

    public void UpdateOutputViewer(IAlgorithm algorithm)
    {
      var cs = (VR_ColorSegmentation)algorithm;
      
      imageViewer.Image=CreateOutputImage(cs.ImageToSegment, cs.SegmentationMask);
    }

    private const double backgroundFactor = 0.25;
    private static RgbImage<byte> CreateOutputImage(RgbImage<byte> image, Image<byte> foregroundMask)
    {
      if (foregroundMask == null)
      {
        return image;
      }

      var outputImg = new RgbImage<byte>(image.Width, image.Height);
      for (int i = 0; i < outputImg.PixelCount; i++)
      {
        if (foregroundMask[i] == 255)
        {
          outputImg[i] = image[i];
        }
        else
        {
          outputImg[i] = new RgbPixel<byte>(ImageUtilities.RoundAndClipToByte(backgroundFactor * image[i].Red),
                                            ImageUtilities.RoundAndClipToByte(backgroundFactor * image[i].Green),
                                            ImageUtilities.RoundAndClipToByte(backgroundFactor * image[i].Blue));
        }
      }

      return outputImg;
    }
	}
}
