﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BioLab.GUI.DataViewers;
using PRLab.GUI.Forms;
using BioLab.GUI.Common;
using BioLab.Common;

namespace PRLab.GUI.DataViewers
{
    public partial class PRFeatureVectorSetViewer : FeatureVectorSetViewer
    {
        private static Normal2DDistributionGenerationForm parameterForm = new Normal2DDistributionGenerationForm();

        public PRFeatureVectorSetViewer(FeatureVectorSet featureVectorSet)
            :base(featureVectorSet)
        {
            InitializeComponent();

            DefaultContextMenu.Items.Add("Generazione distribuzioni MultiNormali", PRLab.Properties.Resources.MNIcon.ToBitmap(), normal2DDistributionGenerationToolStripMenuItem_Click);
        }

        public PRFeatureVectorSetViewer()
        :this(null)
        {
        }

        private void normal2DDistributionGenerationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (parameterForm.ShowDialog() == DialogResult.OK)
            {
                using (new WaitCursor())
                {
                    var fvs=CreateFeatureVectorSet(parameterForm.Obeservations, parameterForm.AddClassId);
                    
                    if (Data!=null)
                    {
                        fvs.Description = ((FeatureVectorSet)Data).Description;
                    }

                    Data =fvs ;
                }
            }
        }

        private static FeatureVectorSet CreateFeatureVectorSet(double[][][] samples,bool addClassId)
        {
            var featureSet = new FeatureVectorSet();
            for (int i = 0; i < samples.Length; i++)
            {
                foreach (var classObservation in samples[i])
                {
                    var fv = new FeatureVector(classObservation, false);

                    if (addClassId)
                    {
                        fv.Class = i;
                    }

                    featureSet.Add(fv, false);        
                }
            }

            return featureSet;
        }
    }
}
