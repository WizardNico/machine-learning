﻿
using BioLab.GUI.DataViewers;

namespace PRLab
{
  partial class VR_HoughLinesDetectorOutputViewer
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.imageViewerGradientModuli = new BioLab.GUI.DataViewers.ImageViewer();
      this.imageViewerBinarizedGradientModuli = new BioLab.GUI.DataViewers.ImageViewer();
      this.imageViewerResult = new BioLab.GUI.DataViewers.ImageViewer();
      this.tableLayoutPanel1.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.imageViewerResult, 1, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(451, 462);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 1;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.Controls.Add(this.imageViewerGradientModuli, 0, 0);
      this.tableLayoutPanel2.Controls.Add(this.imageViewerBinarizedGradientModuli, 0, 1);
      this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 2;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(219, 456);
      this.tableLayoutPanel2.TabIndex = 0;
      // 
      // imageViewerGradientModuli
      // 
      this.imageViewerGradientModuli.BackColor = System.Drawing.SystemColors.Control;
      this.imageViewerGradientModuli.Dock = System.Windows.Forms.DockStyle.Fill;
      this.imageViewerGradientModuli.Location = new System.Drawing.Point(3, 3);
      this.imageViewerGradientModuli.Name = "imageViewerGradientModuli";
      this.imageViewerGradientModuli.Size = new System.Drawing.Size(213, 222);
      this.imageViewerGradientModuli.TabIndex = 0;
      // 
      // imageViewerBinarizedGradientModuli
      // 
      this.imageViewerBinarizedGradientModuli.BackColor = System.Drawing.SystemColors.Control;
      this.imageViewerBinarizedGradientModuli.Dock = System.Windows.Forms.DockStyle.Fill;
      this.imageViewerBinarizedGradientModuli.Location = new System.Drawing.Point(3, 231);
      this.imageViewerBinarizedGradientModuli.Name = "imageViewerBinarizedGradientModuli";
      this.imageViewerBinarizedGradientModuli.Size = new System.Drawing.Size(213, 222);
      this.imageViewerBinarizedGradientModuli.TabIndex = 1;
      // 
      // imageViewerResult
      // 
      this.imageViewerResult.BackColor = System.Drawing.SystemColors.Control;
      this.imageViewerResult.Dock = System.Windows.Forms.DockStyle.Fill;
      this.imageViewerResult.Location = new System.Drawing.Point(228, 3);
      this.imageViewerResult.Name = "imageViewerResult";
      this.imageViewerResult.Size = new System.Drawing.Size(220, 456);
      this.imageViewerResult.TabIndex = 1;
      this.imageViewerResult.Paint += new System.Windows.Forms.PaintEventHandler(this.imageViewerResult_Paint);
      // 
      // HoughLinesDetectorOutputViewer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.tableLayoutPanel1);
      this.Name = "HoughLinesDetectorOutputViewer";
      this.Size = new System.Drawing.Size(451, 462);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private ImageViewer imageViewerGradientModuli;
    private ImageViewer imageViewerBinarizedGradientModuli;
    private ImageViewer imageViewerResult;
  }
}
