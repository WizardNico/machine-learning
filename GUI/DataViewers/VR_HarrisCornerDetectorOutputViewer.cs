﻿using BioLab.GUI.Forms;
using BioLab.Common;
using BioLab.Math.Geometry;
using System.Windows.Forms;
using System.Drawing;
using BioLab.GUI.DataViewers;
using PRLab.EsercitazioniVR;

namespace PRLab
{
  public partial class VR_HarrisCornerDetectorOutputViewer : ImageViewer, IAlgorithmPreviewOutput
  {
    private IntPoint2D[] corners;

    public void UpdateOutputViewer(IAlgorithm algorithm)
    {
      var cd = (VR_HarrisCornerDetector)algorithm;
      corners = cd.Result;
      Image = cd.InputImage;
      Invalidate();
    }

    private const int pointSize=6;
    private const int pointSize2 = pointSize/2;
    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);

      if (corners != null &&
          corners.Length>0)
      {
        AdjustGraphicsToWorldUnits(e.Graphics);
        var pen = Pens.Red;

        foreach (var c in corners)
        {
          e.Graphics.DrawEllipse(pen,c.X-pointSize2,c.Y-pointSize2,pointSize,pointSize );
        }
      }
    }
  }
}
