namespace PRLab
{
    partial class ImageWithMarkedRegionsViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      this.cntxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.addPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.closeRegionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.delRegionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.cntxMenu.SuspendLayout();
      this.SuspendLayout();
      // 
      // cntxMenu
      // 
      this.cntxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPointToolStripMenuItem,
            this.closeRegionToolStripMenuItem,
            this.delRegionToolStripMenuItem});
      this.cntxMenu.Name = "cntxMenu";
      this.cntxMenu.Size = new System.Drawing.Size(145, 70);
      this.cntxMenu.Opening += new System.ComponentModel.CancelEventHandler(this.cntxMenu_Opening);
      // 
      // addPointToolStripMenuItem
      // 
      this.addPointToolStripMenuItem.Name = "addPointToolStripMenuItem";
      this.addPointToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
      this.addPointToolStripMenuItem.Text = "Add point";
      this.addPointToolStripMenuItem.Click += new System.EventHandler(this.addPointToolStripMenuItem_Click);
      // 
      // closeRegionToolStripMenuItem
      // 
      this.closeRegionToolStripMenuItem.Name = "closeRegionToolStripMenuItem";
      this.closeRegionToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
      this.closeRegionToolStripMenuItem.Text = "Close region";
      this.closeRegionToolStripMenuItem.Click += new System.EventHandler(this.closeRegionToolStripMenuItem_Click);
      // 
      // delRegionToolStripMenuItem
      // 
      this.delRegionToolStripMenuItem.Name = "delRegionToolStripMenuItem";
      this.delRegionToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
      this.delRegionToolStripMenuItem.Text = "Delete region";
      this.delRegionToolStripMenuItem.Click += new System.EventHandler(this.delRegionToolStripMenuItem_Click);
      // 
      // ImageWithMarkedRegionsViewer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.ContextMenuStrip = this.cntxMenu;
      this.LeftMouseButtonTool = BioLab.GUI.DataViewers.MouseButtonTool.None;
      this.Name = "ImageWithMarkedRegionsViewer";
      this.Size = new System.Drawing.Size(292, 272);
      this.UseDefaultContextMenu = false;
      this.Paint += new System.Windows.Forms.PaintEventHandler(this.ImageWithMarkedRegionsViewer_Paint);
      this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.imageView_MouseClick);
      this.cntxMenu.ResumeLayout(false);
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip cntxMenu;
        private System.Windows.Forms.ToolStripMenuItem closeRegionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem delRegionToolStripMenuItem;
    }
}
