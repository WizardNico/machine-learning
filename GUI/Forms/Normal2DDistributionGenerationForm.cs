﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BioLab.Common;
using BioLab.GUI.Common;
using BioLab.Math.LinearAlgebra;

namespace PRLab.GUI.Forms
{
    public partial class Normal2DDistributionGenerationForm : Form
    {
        private const int dimCount = 2;
        private const int maxIterationCount=1000000;  

        private const int defaultDistributionCount = 5;
        private const int defaultMinPatternCountPerDistribution = 25;
        private const int defaultMaxPatternCountPerDistribution = 50;
        private const double defaultMinMeanVectorDistance = 0.2;
        private const double defaultMinDistanceOnFirstAxis = 0.2;
        private const double defaultMaxDistanceOnFirstAxis = 0.6;
        private const double defaultMinDistanceOnSecondAxis = 0.3;
        private const double defaultMaxDistanceOnSecondAxis = 0.8;
        private const int defaultPredefinedSeed = 1;
        private const bool defaultAddClassId=true;

        public double[][][] Obeservations { get; private set; }

        public bool AddClassId
        {
            get { return checkBoxAddClassId.Checked; }
        }

        public Normal2DDistributionGenerationForm()
        {
            InitializeComponent();

            SetDefaultParameters();
        }

        private void SetDefaultParameters()
        {
            numericUpDownDistributionCount.Value = defaultDistributionCount;
            numericUpDownMinPatternCountPerDistribution.Value = defaultMinPatternCountPerDistribution;
            numericUpDownMaxPatternCountPerDistribution.Value = defaultMaxPatternCountPerDistribution;
            numericUpDownMinDistanceBetweenMeanVectors.Value =(decimal) defaultMinMeanVectorDistance;
            numericUpDownMinDistanceOnFirstAxis.Value = (decimal)defaultMinDistanceOnFirstAxis;
            numericUpDownMaxDistanceOnFirstAxis.Value = (decimal) defaultMaxDistanceOnFirstAxis;
            numericUpDownMinDistanceOnSecondAxis.Value = (decimal)defaultMinDistanceOnSecondAxis;
            numericUpDownMaxDistanceOnSecondAxis.Value = (decimal) defaultMaxDistanceOnSecondAxis;
            numericUpDownPredefinedSeed.Value = defaultPredefinedSeed;
            radioButtonPredefinedSeed.Checked=true;
            checkBoxAddClassId.Checked = defaultAddClassId;
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            using (new WaitCursor())
            {
                Random randomGenerator=null;
                if (radioButtonPredefinedSeed.Checked)
                {
                    randomGenerator = new Random((int) numericUpDownPredefinedSeed.Value);
                }
                else
                {
                    randomGenerator = new Random();
                }

                var validMeans = false;
                var iterationCount = 0;
                double[][] means = null;
                while (!validMeans &&
                        iterationCount < maxIterationCount)
                {
                    means = GenerateRandomMeans(randomGenerator,
                                                (int)numericUpDownDistributionCount.Value,
                                                dimCount);

                    validMeans = RandomMeansAreValid(means,
                                                    (double) numericUpDownMinDistanceBetweenMeanVectors.Value);

                    iterationCount++;
                }

                if (!validMeans &&
                    iterationCount == maxIterationCount)
                {
                    MessageBox.Show("Impossibile generare vettori medi con distanza minima prefissata.",
                                    "Errore",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    return;
                }

                Obeservations = new double[(int)numericUpDownDistributionCount.Value][][];
                for (int i = 0; i < Obeservations.Length; i++)
                {
                    var covariance = GenerateCovarianceMatrix(randomGenerator,
                                                                new double[]
                                                                {
                                                                    (double)numericUpDownMinDistanceOnFirstAxis.Value,
                                                                    (double)numericUpDownMinDistanceOnSecondAxis.Value
                                                                },
                                                                new double[]
                                                                {
                                                                    (double)numericUpDownMaxDistanceOnFirstAxis.Value,
                                                                    (double)numericUpDownMaxDistanceOnSecondAxis.Value
                                                                });

                    var rotationAngle=randomGenerator.NextDouble() * BioLab.Math.Constants.DoublePI;

                    var generator = new Normal2DDistributionGenerator(means[i],
                                                                    covariance,
                                                                    rotationAngle);

                    var observationCount = randomGenerator.Next((int)numericUpDownMinPatternCountPerDistribution.Value, (int)numericUpDownMaxPatternCountPerDistribution.Value + 1);

                    Obeservations[i] = generator.Generate(observationCount);
                }

                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private double[,] GenerateCovarianceMatrix(Random randomGenerator, double[] minDistanceOnAxes, double[] maxDistanceOnAxes)
        {
            var covariance = new double[dimCount, dimCount];
            for (int i = 0; i < dimCount; i++)
            {
                var validRange = maxDistanceOnAxes[i] - minDistanceOnAxes[i];

                var maxStretch = minDistanceOnAxes[i]+randomGenerator.NextDouble() * validRange;
                var stdDev = maxStretch / 3.0;
                covariance[i, i] = stdDev * stdDev;
            }

            return covariance;
        }

        private static bool RandomMeansAreValid(double[][] means, double minMeanDistance)
        {
            var squareMinDist = minMeanDistance * minMeanDistance;
            for (int i = 0; i < means.Length - 1; i++)
            {
                var v1 = new Vector(means[i], false);

                for (int j = i + 1; j < means.Length; j++)
                {
                    var d = Vector.ComputeSquareEuclideanDistance(v1, new Vector(means[j], false));
                    if (d < squareMinDist)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static double[][] GenerateRandomMeans(Random randomGenerator, int classCount, int dimCount)
        {
            var means = new double[classCount][];
            for (int i = 0; i < classCount; i++)
            {
                means[i] = new double[dimCount];
                for (int j = 0; j < dimCount; j++)
                {
                    means[i][j] = randomGenerator.NextDouble();
                }
            }

            return means;
        }

        private void radioButtonSeed_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownPredefinedSeed.Enabled = radioButtonPredefinedSeed.Checked;
        }

        private void buttonSetDefaultParams_Click(object sender, EventArgs e)
        {
            SetDefaultParameters();
        }
    }
}
