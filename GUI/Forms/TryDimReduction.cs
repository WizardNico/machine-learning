using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BioLab.Common;
using BioLab.DimensionalityReduction;

namespace PRLab
{
    partial class TryDimReduction : Form
    {
        #region Membri privati
        FeatureVectorSet[] availableSets;
        FeatureVectorSetView2DWithSubspaces preview;
        bool calculated = false;
        #endregion

        DimensionalityReductionBuilder currentBuilder { get { return (DimensionalityReductionBuilder)comboBoxAlg.SelectedItem; } }
        SubspaceProjection currentSubspaceProjection;

        public TryDimReduction(DimensionalityReductionBuilder[] spAr, FeatureVectorSet[] sets)
        {
            InitializeComponent();
            availableSets = sets;
            // riempie i combo
            if (availableSets.Length == 0)
                throw new Exception("Nessun set di vettori definito");
            foreach (var vs in availableSets)
            {
                string desc = vs.Description == null ? "Nessuna descrizione" : vs.Description;
                comboBoxDataSets.Items.Add(desc);
            }
            comboBoxDataSets.SelectedIndex = 0;
            comboBoxAlg.Items.AddRange(spAr);
            comboBoxAlg.SelectedIndex = 0;
        }

        private void UpdatePreview()
        {
            var vs = availableSets[comboBoxDataSets.SelectedIndex];
            if (calculated && 
                radioButtonShowReducedVS.Checked)
            {
                // Calcola tutti i vettori ridotti
                Cursor = Cursors.WaitCursor;
                try
                {
                    vs = currentSubspaceProjection.Reduce(vs);
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }

            if (preview == null)
            {
                preview = new FeatureVectorSetView2DWithSubspaces(vs);
                preview.Dock = DockStyle.Fill;
                preview.EnableEditModeInDefaultContextMenu = false;
                panelPreview.Controls.Add(preview);
            }
            else
            {
                preview.FeatureVectorSet = vs;
                preview.UpdateViewer();
            }
            preview.RemoveAllSubspaces();
            if (calculated &&
                radioButtonShowSubspace.Checked /*&& !checkBoxEditMode.Checked*/)
            {
                preview.AddSubspace(currentSubspaceProjection.Origin, currentSubspaceProjection.Bases, 0);
            }
        }

        //private void UpdatePreview()
        //{
        //    if (trainingSet.Dim != 2)   // solo in 2D si puo' disegnare la superficie
        //        showSurface = false;
        //    if (preview == null)
        //    {
        //        preview = new FeatureVectorSetWithClassSurfaces2DViewer(vs, showSurface ? theClassifier : null, showSurface ? theDensityEstimator : null);
        //        preview.Dock = DockStyle.Fill;
        //        preview.EnableEditModeInDefaultContextMenu = false;
        //        panelPreview.Controls.Add(preview);
        //    }
        //    else
        //    {
        //        preview.UpdateViewEx(vs, showSurface ? theClassifier : null, showSurface ? theDensityEstimator : null);
        //    }
        //}

        private void buttonStart_Click(object sender, EventArgs e)
        {
            Calculate();
        }

        private void Calculate()
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                var dataSet = availableSets[comboBoxDataSets.SelectedIndex];

                if (currentBuilder is LdaTransformBuilder &&
                    ContainsUnlabeledData(dataSet))
                {
                    MessageBox.Show("Dataset non etichettato: impossibile eseguire la LDA.");
                }
                else
                {
                    currentBuilder.TrainingSet = dataSet;
                    currentBuilder.ReducedDimensionality = (int)numericUpDownK.Value;
                    currentSubspaceProjection = (SubspaceProjection)currentBuilder.Calculate();
                    calculated = true;
                    UpdatePreview();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Si � verificato un errore imprevisto durante l'esecuzione della trasformata.\n{0}", ex.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private static bool ContainsUnlabeledData(FeatureVectorSet dataSet)
        {
            foreach (var vs in dataSet)
            {
                if (vs.Class == -1)
                {
                    return true;
                }
            }

            return false;
        }

        private void splitPreview_Panel2_Paint(object sender, PaintEventArgs e)
        {
            // Imposta ora il Panel2MinSize: non pu� essere fatto a design time per un bug del VS2005...
            splitPreview.Panel2MinSize = 250;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBoxDataSets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDataSets.SelectedIndex >= 0)
            {
                UpdateDimensions();
            }
            Reset(true);
        }

        private void UpdateDimensions()
        {
            int dim = availableSets[comboBoxDataSets.SelectedIndex].Dim;
            labelDimOrig.Text = string.Format("Dimensione originale: {0}", dim);
            if (dim > 1)
            {
                numericUpDownK.Enabled = true;
                numericUpDownK.Maximum = dim - 1;
            }
            else
            {
                numericUpDownK.Enabled = false;
            }
        }

        private void Reset(bool recalculate)
        {
            if (recalculate)
                calculated = false;
            buttonStart.Enabled = !checkBoxAutoStart.Checked;
            if (checkBoxAutoStart.Checked && !calculated)
            {
                Calculate();
            }
            UpdatePreview();
            //checkBoxEditMode.Enabled = availableSets[comboBoxDataSets.SelectedIndex].Dim == 2 || availableSets[comboBoxDataSets.SelectedIndex].Dim == 0;
        }

        private void DoReset(object sender, EventArgs e)
        {
            Reset(true);
        }

        private void DoResetCalcIfNecessary(object sender, EventArgs e)
        {
            Reset(false);
        }

        //private void checkBoxEditMode_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (checkBoxEditMode.Checked)
        //    {
        //        radioButtonShowSubspace.Checked = true; // Deve essere visualizzato il sottospazio originale
        //        UpdatePreview();
        //        comboBoxAlg.Enabled = comboBoxDataSets.Enabled = checkBoxAutoStart.Enabled = buttonStart.Enabled = 
        //            radioButtonShowSubspace.Enabled = radioButtonShowReducedVS.Enabled = numericUpDownK.Enabled = false;
        //        preview.EditMode = true;
        //    }
        //    else
        //    {
        //        preview.EditMode = false;
        //        comboBoxAlg.Enabled = comboBoxDataSets.Enabled = checkBoxAutoStart.Enabled = buttonStart.Enabled =
        //            radioButtonShowSubspace.Enabled = radioButtonShowReducedVS.Enabled = numericUpDownK.Enabled = true;
        //        UpdateDimensions();
        //        Reset(true);
        //    }
        //}
    }
}