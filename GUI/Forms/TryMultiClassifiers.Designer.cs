namespace PRLab
{
  partial class TryMultiClassifiers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TryMultiClassifiers));
      this.splitPreview = new System.Windows.Forms.SplitContainer();
      this.groupBoxSettings = new System.Windows.Forms.GroupBox();
      this.panelChooseSets = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.labelClassifierOrMethod = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.comboBoxTestSet = new System.Windows.Forms.ComboBox();
      this.comboBoxClassifier = new System.Windows.Forms.ComboBox();
      this.comboBoxTrainingSet = new System.Windows.Forms.ComboBox();
      this.labelMessage = new System.Windows.Forms.Label();
      this.buttonTest = new System.Windows.Forms.Button();
      this.buttonTraining = new System.Windows.Forms.Button();
      this.buttonClose = new System.Windows.Forms.Button();
      this.panelPreview = new System.Windows.Forms.Panel();
      ((System.ComponentModel.ISupportInitialize)(this.splitPreview)).BeginInit();
      this.splitPreview.Panel1.SuspendLayout();
      this.splitPreview.Panel2.SuspendLayout();
      this.splitPreview.SuspendLayout();
      this.panelChooseSets.SuspendLayout();
      this.SuspendLayout();
      // 
      // splitPreview
      // 
      this.splitPreview.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitPreview.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      this.splitPreview.Location = new System.Drawing.Point(0, 0);
      this.splitPreview.Name = "splitPreview";
      // 
      // splitPreview.Panel1
      // 
      this.splitPreview.Panel1.Controls.Add(this.groupBoxSettings);
      this.splitPreview.Panel1.Controls.Add(this.panelChooseSets);
      this.splitPreview.Panel1MinSize = 200;
      // 
      // splitPreview.Panel2
      // 
      this.splitPreview.Panel2.Controls.Add(this.labelMessage);
      this.splitPreview.Panel2.Controls.Add(this.buttonTest);
      this.splitPreview.Panel2.Controls.Add(this.buttonTraining);
      this.splitPreview.Panel2.Controls.Add(this.buttonClose);
      this.splitPreview.Panel2.Controls.Add(this.panelPreview);
      this.splitPreview.Size = new System.Drawing.Size(648, 418);
      this.splitPreview.SplitterDistance = 210;
      this.splitPreview.TabIndex = 0;
      // 
      // groupBoxSettings
      // 
      this.groupBoxSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBoxSettings.Location = new System.Drawing.Point(4, 100);
      this.groupBoxSettings.Name = "groupBoxSettings";
      this.groupBoxSettings.Size = new System.Drawing.Size(199, 312);
      this.groupBoxSettings.TabIndex = 1;
      this.groupBoxSettings.TabStop = false;
      this.groupBoxSettings.Text = "Parametri";
      // 
      // panelChooseSets
      // 
      this.panelChooseSets.Controls.Add(this.label2);
      this.panelChooseSets.Controls.Add(this.labelClassifierOrMethod);
      this.panelChooseSets.Controls.Add(this.label1);
      this.panelChooseSets.Controls.Add(this.comboBoxTestSet);
      this.panelChooseSets.Controls.Add(this.comboBoxClassifier);
      this.panelChooseSets.Controls.Add(this.comboBoxTrainingSet);
      this.panelChooseSets.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelChooseSets.Location = new System.Drawing.Point(0, 0);
      this.panelChooseSets.Name = "panelChooseSets";
      this.panelChooseSets.Size = new System.Drawing.Size(210, 94);
      this.panelChooseSets.TabIndex = 0;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 64);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(45, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Test set";
      // 
      // labelClassifierOrMethod
      // 
      this.labelClassifierOrMethod.AutoSize = true;
      this.labelClassifierOrMethod.Location = new System.Drawing.Point(3, 9);
      this.labelClassifierOrMethod.Name = "labelClassifierOrMethod";
      this.labelClassifierOrMethod.Size = new System.Drawing.Size(69, 13);
      this.labelClassifierOrMethod.TabIndex = 2;
      this.labelClassifierOrMethod.Text = "Classificatore";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 37);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(62, 13);
      this.label1.TabIndex = 4;
      this.label1.Text = "Training set";
      // 
      // comboBoxTestSet
      // 
      this.comboBoxTestSet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBoxTestSet.FormattingEnabled = true;
      this.comboBoxTestSet.Location = new System.Drawing.Point(78, 60);
      this.comboBoxTestSet.Name = "comboBoxTestSet";
      this.comboBoxTestSet.Size = new System.Drawing.Size(121, 21);
      this.comboBoxTestSet.TabIndex = 7;
      this.comboBoxTestSet.SelectedIndexChanged += new System.EventHandler(this.comboBoxTestSet_SelectedIndexChanged);
      // 
      // comboBoxClassifier
      // 
      this.comboBoxClassifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBoxClassifier.FormattingEnabled = true;
      this.comboBoxClassifier.Location = new System.Drawing.Point(78, 6);
      this.comboBoxClassifier.Name = "comboBoxClassifier";
      this.comboBoxClassifier.Size = new System.Drawing.Size(121, 21);
      this.comboBoxClassifier.TabIndex = 3;
      this.comboBoxClassifier.SelectedIndexChanged += new System.EventHandler(this.comboBoxClassifier_SelectedIndexChanged);
      // 
      // comboBoxTrainingSet
      // 
      this.comboBoxTrainingSet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBoxTrainingSet.FormattingEnabled = true;
      this.comboBoxTrainingSet.Location = new System.Drawing.Point(78, 33);
      this.comboBoxTrainingSet.Name = "comboBoxTrainingSet";
      this.comboBoxTrainingSet.Size = new System.Drawing.Size(121, 21);
      this.comboBoxTrainingSet.TabIndex = 5;
      this.comboBoxTrainingSet.SelectedIndexChanged += new System.EventHandler(this.comboBoxTrainingSet_SelectedIndexChanged);
      // 
      // labelMessage
      // 
      this.labelMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.labelMessage.Location = new System.Drawing.Point(8, 364);
      this.labelMessage.Name = "labelMessage";
      this.labelMessage.Size = new System.Drawing.Size(414, 20);
      this.labelMessage.TabIndex = 4;
      this.labelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // buttonTest
      // 
      this.buttonTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.buttonTest.Enabled = false;
      this.buttonTest.Location = new System.Drawing.Point(89, 389);
      this.buttonTest.Name = "buttonTest";
      this.buttonTest.Size = new System.Drawing.Size(75, 23);
      this.buttonTest.TabIndex = 2;
      this.buttonTest.Text = "Test";
      this.buttonTest.UseVisualStyleBackColor = true;
      this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
      // 
      // buttonTraining
      // 
      this.buttonTraining.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.buttonTraining.Location = new System.Drawing.Point(8, 389);
      this.buttonTraining.Name = "buttonTraining";
      this.buttonTraining.Size = new System.Drawing.Size(75, 23);
      this.buttonTraining.TabIndex = 1;
      this.buttonTraining.Text = "Training";
      this.buttonTraining.UseVisualStyleBackColor = true;
      this.buttonTraining.Click += new System.EventHandler(this.buttonTraining_Click);
      // 
      // buttonClose
      // 
      this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonClose.Location = new System.Drawing.Point(350, 389);
      this.buttonClose.Name = "buttonClose";
      this.buttonClose.Size = new System.Drawing.Size(75, 23);
      this.buttonClose.TabIndex = 3;
      this.buttonClose.Text = "Chiudi";
      this.buttonClose.UseVisualStyleBackColor = true;
      this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
      // 
      // panelPreview
      // 
      this.panelPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.panelPreview.Location = new System.Drawing.Point(0, 0);
      this.panelPreview.Name = "panelPreview";
      this.panelPreview.Size = new System.Drawing.Size(434, 360);
      this.panelPreview.TabIndex = 0;
      // 
      // VR_TryMultiClassifiers
      // 
      this.AcceptButton = this.buttonTraining;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonClose;
      this.ClientSize = new System.Drawing.Size(648, 418);
      this.Controls.Add(this.splitPreview);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MinimumSize = new System.Drawing.Size(656, 452);
      this.Name = "VR_TryMultiClassifiers";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Prova classificatori";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UseClassifier_FormClosed);
      this.Load += new System.EventHandler(this.UseClassifier_Load);
      this.splitPreview.Panel1.ResumeLayout(false);
      this.splitPreview.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitPreview)).EndInit();
      this.splitPreview.ResumeLayout(false);
      this.panelChooseSets.ResumeLayout(false);
      this.panelChooseSets.PerformLayout();
      this.ResumeLayout(false);

}

        #endregion

        private System.Windows.Forms.SplitContainer splitPreview;
        private System.Windows.Forms.Panel panelChooseSets;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTestSet;
        private System.Windows.Forms.ComboBox comboBoxTrainingSet;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel panelPreview;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.Button buttonTraining;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Label labelClassifierOrMethod;
        private System.Windows.Forms.ComboBox comboBoxClassifier;
    }
}