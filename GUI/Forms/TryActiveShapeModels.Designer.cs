namespace PRLab
{
    partial class TryActiveShapeModels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TryActiveShapeModels));
      this.imageWithMarkedRegionsViewerTraining = new PRLab.ImageWithMarkedRegionsViewer();
      this.buttonLoadTrainingData = new System.Windows.Forms.Button();
      this.imageWithMarkedRegionsViewerCurrentShape = new PRLab.ImageWithMarkedRegionsViewer();
      this.groupBoxModes = new System.Windows.Forms.GroupBox();
      this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
      this.buttonResetModes = new System.Windows.Forms.Button();
      this.numericUpDownPreviewTrainImg = new System.Windows.Forms.NumericUpDown();
      this.buttonCalculate = new System.Windows.Forms.Button();
      this.checkBoxCenter = new System.Windows.Forms.CheckBox();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel2 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.panel4 = new System.Windows.Forms.Panel();
      this.numericUpDownModeCount = new System.Windows.Forms.NumericUpDown();
      this.groupBoxModes.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPreviewTrainImg)).BeginInit();
      this.tableLayoutPanel1.SuspendLayout();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.panel3.SuspendLayout();
      this.panel4.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownModeCount)).BeginInit();
      this.SuspendLayout();
      // 
      // imageWithMarkedRegionsViewerTraining
      // 
      this.imageWithMarkedRegionsViewerTraining.BackColor = System.Drawing.SystemColors.Control;
      this.imageWithMarkedRegionsViewerTraining.Dock = System.Windows.Forms.DockStyle.Fill;
      this.imageWithMarkedRegionsViewerTraining.LeftMouseButtonTool = BioLab.GUI.DataViewers.MouseButtonTool.None;
      this.imageWithMarkedRegionsViewerTraining.Location = new System.Drawing.Point(3, 3);
      this.imageWithMarkedRegionsViewerTraining.Modifying = true;
      this.imageWithMarkedRegionsViewerTraining.Name = "imageWithMarkedRegionsViewerTraining";
      this.imageWithMarkedRegionsViewerTraining.Size = new System.Drawing.Size(367, 615);
      this.imageWithMarkedRegionsViewerTraining.TabIndex = 0;
      this.imageWithMarkedRegionsViewerTraining.UseDefaultContextMenu = false;
      // 
      // buttonLoadTrainingData
      // 
      this.buttonLoadTrainingData.Location = new System.Drawing.Point(3, 3);
      this.buttonLoadTrainingData.Name = "buttonLoadTrainingData";
      this.buttonLoadTrainingData.Size = new System.Drawing.Size(112, 23);
      this.buttonLoadTrainingData.TabIndex = 2;
      this.buttonLoadTrainingData.Text = "Carica immagini...";
      this.buttonLoadTrainingData.UseVisualStyleBackColor = true;
      this.buttonLoadTrainingData.Click += new System.EventHandler(this.buttonLoadTrainingData_Click);
      // 
      // imageWithMarkedRegionsViewerCurrentShape
      // 
      this.imageWithMarkedRegionsViewerCurrentShape.BackColor = System.Drawing.SystemColors.Control;
      this.imageWithMarkedRegionsViewerCurrentShape.Dock = System.Windows.Forms.DockStyle.Fill;
      this.imageWithMarkedRegionsViewerCurrentShape.LeftMouseButtonTool = BioLab.GUI.DataViewers.MouseButtonTool.None;
      this.imageWithMarkedRegionsViewerCurrentShape.Location = new System.Drawing.Point(376, 3);
      this.imageWithMarkedRegionsViewerCurrentShape.Modifying = true;
      this.imageWithMarkedRegionsViewerCurrentShape.Name = "imageWithMarkedRegionsViewerCurrentShape";
      this.imageWithMarkedRegionsViewerCurrentShape.Size = new System.Drawing.Size(367, 615);
      this.imageWithMarkedRegionsViewerCurrentShape.TabIndex = 0;
      this.imageWithMarkedRegionsViewerCurrentShape.UseDefaultContextMenu = false;
      // 
      // groupBoxModes
      // 
      this.groupBoxModes.Controls.Add(this.tableLayoutPanel);
      this.groupBoxModes.Dock = System.Windows.Forms.DockStyle.Fill;
      this.groupBoxModes.Location = new System.Drawing.Point(0, 0);
      this.groupBoxModes.Name = "groupBoxModes";
      this.groupBoxModes.Size = new System.Drawing.Size(194, 615);
      this.groupBoxModes.TabIndex = 3;
      this.groupBoxModes.TabStop = false;
      this.groupBoxModes.Text = "Modi di variazione";
      // 
      // tableLayoutPanel
      // 
      this.tableLayoutPanel.ColumnCount = 1;
      this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
      this.tableLayoutPanel.Name = "tableLayoutPanel";
      this.tableLayoutPanel.RowCount = 20;
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
      this.tableLayoutPanel.Size = new System.Drawing.Size(188, 596);
      this.tableLayoutPanel.TabIndex = 0;
      // 
      // buttonResetModes
      // 
      this.buttonResetModes.Location = new System.Drawing.Point(3, 3);
      this.buttonResetModes.Name = "buttonResetModes";
      this.buttonResetModes.Size = new System.Drawing.Size(112, 23);
      this.buttonResetModes.TabIndex = 1;
      this.buttonResetModes.Text = "Reset";
      this.buttonResetModes.UseVisualStyleBackColor = true;
      this.buttonResetModes.Click += new System.EventHandler(this.buttonResetModes_Click);
      // 
      // numericUpDownPreviewTrainImg
      // 
      this.numericUpDownPreviewTrainImg.Location = new System.Drawing.Point(121, 6);
      this.numericUpDownPreviewTrainImg.Name = "numericUpDownPreviewTrainImg";
      this.numericUpDownPreviewTrainImg.Size = new System.Drawing.Size(54, 20);
      this.numericUpDownPreviewTrainImg.TabIndex = 4;
      this.numericUpDownPreviewTrainImg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.numericUpDownPreviewTrainImg.ValueChanged += new System.EventHandler(this.numericUpDownPreviewTrainImg_ValueChanged);
      // 
      // buttonCalculate
      // 
      this.buttonCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.buttonCalculate.Location = new System.Drawing.Point(3, 3);
      this.buttonCalculate.Name = "buttonCalculate";
      this.buttonCalculate.Size = new System.Drawing.Size(112, 23);
      this.buttonCalculate.TabIndex = 2;
      this.buttonCalculate.Text = "Crea modello";
      this.buttonCalculate.UseVisualStyleBackColor = true;
      this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
      // 
      // checkBoxCenter
      // 
      this.checkBoxCenter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.checkBoxCenter.AutoSize = true;
      this.checkBoxCenter.Location = new System.Drawing.Point(121, 4);
      this.checkBoxCenter.Name = "checkBoxCenter";
      this.checkBoxCenter.Size = new System.Drawing.Size(102, 17);
      this.checkBoxCenter.TabIndex = 5;
      this.checkBoxCenter.Text = "Usa il baricentro";
      this.checkBoxCenter.UseVisualStyleBackColor = true;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 3;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
      this.tableLayoutPanel1.Controls.Add(this.imageWithMarkedRegionsViewerCurrentShape, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.imageWithMarkedRegionsViewerTraining, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 1);
      this.tableLayoutPanel1.Controls.Add(this.panel3, 2, 0);
      this.tableLayoutPanel1.Controls.Add(this.panel4, 2, 1);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(946, 661);
      this.tableLayoutPanel1.TabIndex = 6;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.buttonLoadTrainingData);
      this.panel1.Controls.Add(this.numericUpDownPreviewTrainImg);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(3, 624);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(367, 34);
      this.panel1.TabIndex = 0;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.buttonCalculate);
      this.panel2.Controls.Add(this.checkBoxCenter);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(376, 624);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(367, 34);
      this.panel2.TabIndex = 1;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.groupBoxModes);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel3.Location = new System.Drawing.Point(749, 3);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(194, 615);
      this.panel3.TabIndex = 2;
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.numericUpDownModeCount);
      this.panel4.Controls.Add(this.buttonResetModes);
      this.panel4.Location = new System.Drawing.Point(749, 624);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(194, 34);
      this.panel4.TabIndex = 3;
      // 
      // numericUpDownModeCount
      // 
      this.numericUpDownModeCount.Location = new System.Drawing.Point(132, 3);
      this.numericUpDownModeCount.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
      this.numericUpDownModeCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDownModeCount.Name = "numericUpDownModeCount";
      this.numericUpDownModeCount.Size = new System.Drawing.Size(59, 20);
      this.numericUpDownModeCount.TabIndex = 2;
      this.numericUpDownModeCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.numericUpDownModeCount.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
      this.numericUpDownModeCount.ValueChanged += new System.EventHandler(this.numericUpDownModeCount_ValueChanged);
      // 
      // TryActiveShapeModels
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(946, 661);
      this.Controls.Add(this.tableLayoutPanel1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MinimumSize = new System.Drawing.Size(688, 357);
      this.Name = "TryActiveShapeModels";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Active Shape Models";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.Load += new System.EventHandler(this.TryActiveShapeModels_Load);
      this.groupBoxModes.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPreviewTrainImg)).EndInit();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      this.panel3.ResumeLayout(false);
      this.panel4.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownModeCount)).EndInit();
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonLoadTrainingData;
        private System.Windows.Forms.GroupBox groupBoxModes;
        private System.Windows.Forms.NumericUpDown numericUpDownPreviewTrainImg;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.CheckBox checkBoxCenter;
        private System.Windows.Forms.Button buttonResetModes;
        private ImageWithMarkedRegionsViewer imageWithMarkedRegionsViewerTraining;
        private ImageWithMarkedRegionsViewer imageWithMarkedRegionsViewerCurrentShape;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.NumericUpDown numericUpDownModeCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
    }
}