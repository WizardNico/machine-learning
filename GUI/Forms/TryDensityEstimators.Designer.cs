namespace PRLab
{
    partial class TryDensityEstimators
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TryDensityEstimators));
            this.splitPreview = new System.Windows.Forms.SplitContainer();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.panelChooseSets = new System.Windows.Forms.Panel();
            this.comboBoxDensityEst = new System.Windows.Forms.ComboBox();
            this.labelClassifierOrMethod = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxTrainingSet = new System.Windows.Forms.ComboBox();
            this.labelMessage = new System.Windows.Forms.Label();
            this.buttonTraining = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.panelPreview = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.splitPreview)).BeginInit();
            this.splitPreview.Panel1.SuspendLayout();
            this.splitPreview.Panel2.SuspendLayout();
            this.splitPreview.SuspendLayout();
            this.panelChooseSets.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitPreview
            // 
            this.splitPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitPreview.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitPreview.Location = new System.Drawing.Point(0, 0);
            this.splitPreview.Name = "splitPreview";
            // 
            // splitPreview.Panel1
            // 
            this.splitPreview.Panel1.Controls.Add(this.groupBoxSettings);
            this.splitPreview.Panel1.Controls.Add(this.panelChooseSets);
            this.splitPreview.Panel1MinSize = 200;
            // 
            // splitPreview.Panel2
            // 
            this.splitPreview.Panel2.Controls.Add(this.labelMessage);
            this.splitPreview.Panel2.Controls.Add(this.buttonTraining);
            this.splitPreview.Panel2.Controls.Add(this.buttonClose);
            this.splitPreview.Panel2.Controls.Add(this.panelPreview);
            this.splitPreview.Size = new System.Drawing.Size(651, 418);
            this.splitPreview.SplitterDistance = 210;
            this.splitPreview.TabIndex = 0;
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSettings.Location = new System.Drawing.Point(4, 73);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(199, 342);
            this.groupBoxSettings.TabIndex = 1;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Parametri";
            // 
            // panelChooseSets
            // 
            this.panelChooseSets.Controls.Add(this.comboBoxDensityEst);
            this.panelChooseSets.Controls.Add(this.labelClassifierOrMethod);
            this.panelChooseSets.Controls.Add(this.label1);
            this.panelChooseSets.Controls.Add(this.comboBoxTrainingSet);
            this.panelChooseSets.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChooseSets.Location = new System.Drawing.Point(0, 0);
            this.panelChooseSets.Name = "panelChooseSets";
            this.panelChooseSets.Size = new System.Drawing.Size(210, 67);
            this.panelChooseSets.TabIndex = 0;
            // 
            // comboBoxDensityEst
            // 
            this.comboBoxDensityEst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDensityEst.FormattingEnabled = true;
            this.comboBoxDensityEst.Location = new System.Drawing.Point(78, 6);
            this.comboBoxDensityEst.Name = "comboBoxDensityEst";
            this.comboBoxDensityEst.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDensityEst.Sorted = true;
            this.comboBoxDensityEst.TabIndex = 3;
            this.comboBoxDensityEst.SelectedIndexChanged += new System.EventHandler(this.comboBoxProbEst_SelectedIndexChanged);
            // 
            // labelClassifierOrMethod
            // 
            this.labelClassifierOrMethod.AutoSize = true;
            this.labelClassifierOrMethod.Location = new System.Drawing.Point(3, 9);
            this.labelClassifierOrMethod.Name = "labelClassifierOrMethod";
            this.labelClassifierOrMethod.Size = new System.Drawing.Size(50, 13);
            this.labelClassifierOrMethod.TabIndex = 2;
            this.labelClassifierOrMethod.Text = "Estimator";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Data set";
            // 
            // comboBoxTrainingSet
            // 
            this.comboBoxTrainingSet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTrainingSet.FormattingEnabled = true;
            this.comboBoxTrainingSet.Location = new System.Drawing.Point(78, 33);
            this.comboBoxTrainingSet.Name = "comboBoxTrainingSet";
            this.comboBoxTrainingSet.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTrainingSet.TabIndex = 5;
            this.comboBoxTrainingSet.SelectedIndexChanged += new System.EventHandler(this.comboBoxTrainingSet_SelectedIndexChanged);
            // 
            // labelMessage
            // 
            this.labelMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMessage.Location = new System.Drawing.Point(3, 369);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(431, 20);
            this.labelMessage.TabIndex = 4;
            this.labelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonTraining
            // 
            this.buttonTraining.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTraining.Location = new System.Drawing.Point(3, 392);
            this.buttonTraining.Name = "buttonTraining";
            this.buttonTraining.Size = new System.Drawing.Size(75, 23);
            this.buttonTraining.TabIndex = 1;
            this.buttonTraining.Text = "Estimate";
            this.buttonTraining.UseVisualStyleBackColor = true;
            this.buttonTraining.Click += new System.EventHandler(this.buttonTraining_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(359, 392);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Chiudi";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // panelPreview
            // 
            this.panelPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPreview.Location = new System.Drawing.Point(0, 0);
            this.panelPreview.Name = "panelPreview";
            this.panelPreview.Size = new System.Drawing.Size(434, 366);
            this.panelPreview.TabIndex = 0;
            // 
            // TryDensityEstimators
            // 
            this.AcceptButton = this.buttonTraining;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(651, 418);
            this.Controls.Add(this.splitPreview);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(656, 452);
            this.Name = "TryDensityEstimators";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Prova density estimator";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UseClassifier_FormClosed);
            this.Load += new System.EventHandler(this.UseClassifier_Load);
            this.splitPreview.Panel1.ResumeLayout(false);
            this.splitPreview.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPreview)).EndInit();
            this.splitPreview.ResumeLayout(false);
            this.panelChooseSets.ResumeLayout(false);
            this.panelChooseSets.PerformLayout();
            this.ResumeLayout(false);

}

        #endregion

        private System.Windows.Forms.SplitContainer splitPreview;
        private System.Windows.Forms.Panel panelChooseSets;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTrainingSet;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel panelPreview;
        private System.Windows.Forms.Button buttonTraining;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Label labelClassifierOrMethod;
        private System.Windows.Forms.ComboBox comboBoxDensityEst;
    }
}