using System;
using System.Windows.Forms;
using BioLab.Common;
using BioLab.Classification.Unsupervised;
using PRLab.EsercitazioniVR;
using PRLab.EsercitazioniML;
using System.Globalization;

namespace PRLab
{
    partial class TryClusterizers : Form
    {
        #region Membri privati
        FeatureVectorSet[] availableSets;
        FeatureVectorSetViewWithClusteringInfo2DViewer preview;
        ParametersControl userControlParameters;
        bool running = false;

        Clusterizer currentClusterizer { get { return (Clusterizer)comboBoxAlg.SelectedItem; } }

        #endregion


        public TryClusterizers(Clusterizer[] clAr, FeatureVectorSet[] sets)
        {
            InitializeComponent();
            availableSets = sets;
            // riempie i combo
            if (availableSets.Length == 0)
                throw new Exception("Nessun set di vettori definito");
            foreach (var vs in availableSets)
            {
                string desc = vs.Description == null ? "Nessuna descrizione" : vs.Description;
                comboBoxDataSets.Items.Add(desc);
            }
            comboBoxDataSets.SelectedIndex = 0;
            comboBoxAlg.Items.AddRange(clAr);
            comboBoxAlg.SelectedIndex = 0;

            labelScore.Text = string.Empty;
        }

        private void TestClusterizers_Load(object sender, EventArgs e)
        {
            // Imposta ora il Panel2MinSize: non pu� essere fatto a design time per un bug del VS2005...
            splitPreview.Panel2MinSize = 250;
            timerIteration.Interval = trackBarTimerSpeed.Maximum - trackBarTimerSpeed.Value + 1;
        }

        void userControlParameters_ParametersChanged(object sender, EventArgs e)
        {
            Reset();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdatePreview()
        {
            if (preview == null)
            {
                preview = new FeatureVectorSetViewWithClusteringInfo2DViewer(availableSets[comboBoxDataSets.SelectedIndex], currentClusterizer);
                preview.Dock = DockStyle.Fill;
                preview.EnableEditModeInDefaultContextMenu = false;
                panelPreview.Controls.Add(preview);
            }
            else
            {
                preview.Data = availableSets[comboBoxDataSets.SelectedIndex];
                preview.UpdateViewer();
            }

            if (currentClusterizer is KMeanClustering)
            {
                labelScore.Text = string.Format(CultureInfo.InvariantCulture,"Somma quadrati distanze: {0:0.0}",((KMeanClustering)currentClusterizer).SumSquareDistances); 
            }
            else if (currentClusterizer is MyKMeansClustering)
            {
                labelScore.Text = string.Format(CultureInfo.InvariantCulture, "Somma quadrati distanze: {0:0.0}", ((MyKMeansClustering)currentClusterizer).SumSquareDistances); 
            }
            else if (currentClusterizer is EMClustering)
            {
                labelScore.Text = string.Format(CultureInfo.InvariantCulture, "Log-Likelihood: {0:0.0}", ((EMClustering)currentClusterizer).LogaritmicLikelihood);
            }
        }

        private void comboBoxTrainingSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            Reset();
            ResetClusterizer();

            labelScore.Text = string.Empty;

            UpdatePreview();
        }

        private void ResetClusterizer()
        {
            if (currentClusterizer == null)
                return;

            if (preview != null)
            {
                panelPreview.Controls.Remove(preview);
                preview = null;
            }
            CreateParametersControl();
            checkBoxAutoIteration.Enabled = currentClusterizer is IterativeClusterizer;
        }

        private void Start()
        {
            if (running)
            {
                DoIteration();
            }
            else
            {
                // Diverso comportamento a seconda del tipo di clusterizer
                var cl = currentClusterizer;
                int numClusters = (int)numericUpDownNumClsuters.Value;
                var vs = availableSets[comboBoxDataSets.SelectedIndex];
                try
                {
                    if (cl is IterativeClusterizer)
                    {
                        // usa un timer per le iterazioni (o la pressione di un pulsante)
                        comboBoxAlg.Enabled = false;
                        comboBoxDataSets.Enabled = false;
                        numericUpDownNumClsuters.Enabled = false;
                        var iterativeClusterizer = (IterativeClusterizer)cl;
                        iterativeClusterizer.VectorSet = vs;
                        if (iterativeClusterizer is KMeanClustering)
                        {
                            var kmc = (KMeanClustering)iterativeClusterizer;
                            kmc.ClusterCount = numClusters;
                        }
                        else if (iterativeClusterizer is EMClustering)
                        {
                            var emc = (EMClustering)iterativeClusterizer;
                            emc.ClusterCount = numClusters;
                        }
                        else if (iterativeClusterizer is VR_KMeanClustering)
                        {
                            var es4kmc = (VR_KMeanClustering)iterativeClusterizer;
                            es4kmc.ClusterCount = numClusters;
                        }
                        else if (iterativeClusterizer is MyKMeansClustering)
                        {
                            var es4kmc = (MyKMeansClustering)iterativeClusterizer;
                            es4kmc.ClusterCount = numClusters;
                        }

                        iterativeClusterizer.InitializeClusterizer();

                        UpdatePreview();
                        buttonStop.Enabled = true;
                        buttonStart.Text = "Iterazione";
                        if (checkBoxAutoIteration.Checked)
                        {
                            timerIteration.Enabled = true;
                            buttonStart.Enabled = false;
                            buttonStart.Visible = false;
                            trackBarTimerSpeed.Visible = true;
                        }
                        else
                        {
                            timerIteration.Enabled = false;
                            buttonStart.Enabled = true;
                            buttonStop.Enabled = true;
                        }
                        running = true;
                    }
                    else
                    {
                        // Esegue tutto il clustering qui
                        //Cursor = Cursors.WaitCursor;
                        //cl.Clusterize(vs, numClusters);
                        //UpdatePreview();

                        throw new NotImplementedException();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Si � verificato un errore imprevisto durante l'esecuzione dell'algoritmo.\n{0}", ex.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Reset();
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            // nel caso sia stato usato Invio (� l'AcceptButton), sposta il fuoco per fare eventuali aggiornamenti
            buttonStart.Focus();
            Start();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            running = false;
            timerIteration.Enabled = false;
            buttonStart.Text = "Avvia";
            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
            comboBoxAlg.Enabled = true;
            comboBoxDataSets.Enabled = true;
            numericUpDownNumClsuters.Enabled = true;
            buttonStart.Visible = true;
            trackBarTimerSpeed.Visible = false;
        }

        private void UseClusterizer_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (preview != null)
            //  preview.Cleanup();
        }

        private void CreateParametersControl()
        {
            if (userControlParameters != null)
            {
                groupBoxSettings.Controls.Remove(userControlParameters);
            }
            userControlParameters = ParametersControl.CreateClusterizerParametersControl(currentClusterizer);
            userControlParameters.Dock = DockStyle.Fill;
            userControlParameters.ParametersChanged += new EventHandler(userControlParameters_ParametersChanged);
            groupBoxSettings.Controls.Add(userControlParameters);
        }

        private void comboBoxAlg_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetClusterizer();

            labelScore.Text = string.Empty;

            //UpdatePreview();
        }

        private void checkBoxAutoIteration_CheckedChanged(object sender, EventArgs e)
        {
            if (running)
            {
                timerIteration.Enabled = checkBoxAutoIteration.Checked;
                buttonStart.Enabled = !timerIteration.Enabled;
                buttonStart.Visible = !timerIteration.Enabled;
                trackBarTimerSpeed.Visible = timerIteration.Enabled;
            }
        }

        private void timerIteration_Tick(object sender, EventArgs e)
        {
            DoIteration();
        }

        private void DoIteration()
        {
            var cl = (IterativeClusterizer)currentClusterizer;
            var vs = availableSets[comboBoxDataSets.SelectedIndex];
            try
            {
                if (cl.ExecuteIteration())
                {
                    // fine
                    UpdatePreview();
                    Reset();
                    MessageBox.Show("L'algoritmo � terminato", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    UpdatePreview();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Si � verificato un errore imprevisto durante l'esecuzione dell'algoritmo.\n{0}", ex.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Reset();
            }
        }

        private void trackBarTimerSpeed_ValueChanged(object sender, EventArgs e)
        {
            timerIteration.Interval = trackBarTimerSpeed.Maximum - trackBarTimerSpeed.Value + 1;
        }
    }
}