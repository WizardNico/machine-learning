using System;
using System.Windows.Forms;
using BioLab.Common;
using BioLab.Classification.Supervised;
using BioLab.Classification.DensityEstimation;

namespace PRLab
{
    partial class TryClassifiers : Form
    {
        #region Membri privati
        private bool ClassifierMode
        {
            get { return radioButtonClassifierMode.Checked; }
        }
        private ClassifierBuilder theClassifierBuilder  // il classifier builder corrente
        {
            get { return ClassifierMode ? (ClassifierBuilder)comboBoxClassifier.SelectedItem : null; }
        }
        private Classifier TheClassifier  // il classifier corrente
        {
            get { return ClassifierMode ? (Classifier)theClassifier : null; }
        }
        private DensityEstimator theDensityEstimator  // il density est. corrente
        {
            get { return !ClassifierMode ? (DensityEstimator)comboBoxDensityEst.SelectedItem : null; }
        }
        FeatureVectorSet[] availableSets;
        FeatureVectorSet trainingSet, testSet;
        FeatureVectorSetWithClassSurfaces2DViewer preview;
        ParametersControl userControlParameters;
        private Classifier theClassifier;
        #endregion


        public TryClassifiers(ClassifierBuilder[] classifierBuilders, DensityEstimator[] densityEstimators, FeatureVectorSet[] sets, bool createMCs)
        {
            InitializeComponent();
            availableSets = sets;
            // riempie i combo
            if (availableSets.Length == 0)
                throw new Exception("Nessun set di vettori definito");
            foreach (var vs in availableSets)
            {
                string desc = vs.Description == null ? "Nessuna descrizione" : vs.Description;
                comboBoxTrainingSet.Items.Add(desc);
                comboBoxTestSet.Items.Add(desc);
            }
            comboBoxTrainingSet.SelectedIndex = 0;
            comboBoxTestSet.SelectedIndex = comboBoxTestSet.Items.Count > 1 ? 1 : 0;

            if (classifierBuilders != null &&
                classifierBuilders.Length > 0)
            {
                comboBoxClassifier.Items.AddRange(classifierBuilders);
            }

            if (createMCs)
            {
                comboBoxClassifier.Items.Add("Crea MCS...");
            }

            if (comboBoxClassifier.Items.Count > 0)
            {
                comboBoxClassifier.SelectedIndex = 0;
            }
            
            if (densityEstimators != null &&
                densityEstimators.Length > 0)
            {
                comboBoxDensityEst.Items.AddRange(densityEstimators);
                comboBoxDensityEst.SelectedIndex = 0;
            }
            else
            {
                comboBoxDensityEst.Visible = false;
                radioButtonProbEstMode.Visible = false;
                radioButtonClassifierMode.Visible = false;
            }
        }

        private void UseClassifier_Load(object sender, EventArgs e)
        {
            // Imposta ora il Panel2MinSize: non pu� essere fatto a design time per un bug del VS2005...
            splitPreview.Panel2MinSize = 250;
            // posiziona il combo (a design � spostato per poterlo vedere)
            comboBoxDensityEst.Location = comboBoxClassifier.Location;
            comboBoxDensityEst.Visible = false;
        }

        void userControlParameters_ParametersChanged(object sender, EventArgs e)
        {
            // annulla il training
            ClearTrainingState();
            UpdatePreview(trainingSet, false);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdatePreview(FeatureVectorSet vs, bool showSurface)
        {
            if (trainingSet.Dim != 2)   // solo in 2D si puo' disegnare la superficie
                showSurface = false;
            if (preview == null)
            {
                preview = new FeatureVectorSetWithClassSurfaces2DViewer(vs, showSurface ? theClassifier : null, showSurface ? theDensityEstimator : null);
                preview.Dock = DockStyle.Fill;
                preview.EnableEditModeInDefaultContextMenu = false;
                panelPreview.Controls.Add(preview);
            }
            else
            {
                preview.UpdateViewEx(vs, showSurface ? theClassifier : null, showSurface ? theDensityEstimator : null);
            }
        }

        private void comboBoxTrainingSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearTrainingState();
            trainingSet = availableSets[comboBoxTrainingSet.SelectedIndex];
            UpdatePreview(trainingSet, false);
        }

        private void ClearTrainingState()
        {
            buttonTest.Enabled = false;
            labelMessage.Text = "";
        }

        private void comboBoxTestSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearTrainingState();
            testSet = availableSets[comboBoxTestSet.SelectedIndex];
            UpdatePreview(testSet, false);
        }

        private void buttonTraining_Click(object sender, EventArgs e)
        {
            // nel caso sia stato usato Invio (� l'AcceptButton), sposta il fuoco per fare eventuali aggiornamenti
            buttonTraining.Focus();
            ClearTrainingState();
            Cursor = Cursors.WaitCursor;
            try
            {
                if (ClassifierMode)
                {
                    theClassifier = theClassifierBuilder.Train(trainingSet);

                    //if (!theClassifier.Train(trainingSet, null))
                    //{
                    //  MessageBox.Show("Il training non ha avuto successo", "Errore");
                    //  return;
                    //}
                    buttonTest.Enabled = true;
                }
                else
                {
                    theDensityEstimator.TrainingSet = trainingSet;
                    //theProbEstimator.Estimate();
                }
                UpdatePreview(trainingSet, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Si � verificato un errore imprevisto durante il training.\n{0}", ex.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            ClearTrainingState();
            Cursor = Cursors.WaitCursor;
            try
            {
                if (testSet.Count > 0)
                {
                    ElapsedTime et = new ElapsedTime();
                    et.Start();
                    int nErrors = 0;
                    foreach (var v in testSet)
                    {
                        int c = TheClassifier.Classify(v);
                        if (c != v.Class)
                            nErrors++;
                    }
                    et.Stop();
                    labelMessage.Text = string.Format("Errore di classificazione: {0:0.0%} [Tempo: {1}]", (double)nErrors / testSet.Count, et);
                }
                UpdatePreview(testSet, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Si � verificato un errore imprevisto durante il test.\n{0}", ex.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void UseClassifier_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (preview != null)
            //    preview.Cleanup();
        }

        private void CreateParametersControl()
        {
            if (userControlParameters != null)
            {
                groupBoxSettings.Controls.Remove(userControlParameters);
            }
            if (ClassifierMode)
                userControlParameters = ParametersControl.CreateClassifierParametersControl((ClassifierBuilder)comboBoxClassifier.SelectedItem);
            else userControlParameters = ParametersControl.CreateDensityEstParametersControl((DensityEstimator)comboBoxDensityEst.SelectedItem);
            userControlParameters.Dock = DockStyle.Fill;
            userControlParameters.ParametersChanged += new EventHandler(userControlParameters_ParametersChanged);
            groupBoxSettings.Controls.Add(userControlParameters);
        }


        private void comboBoxClassifier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxClassifier.SelectedItem == null)
                return;
            if (comboBoxClassifier.SelectedItem is ClassifierBuilder)
            {
                CreateParametersControl();
            }
            else
            {
                // creazione MCS
                using (var dlg = new CreateMulticlassifierForm())
                {
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        var clBuilderArray = new ClassifierBuilder[comboBoxClassifier.Items.Count];
                        for (int i = 0; i < comboBoxClassifier.Items.Count - 1; i++)
                        {
                            clBuilderArray[i] = (ClassifierBuilder)comboBoxClassifier.Items[i];
                        }
                        clBuilderArray[clBuilderArray.Length - 1] = dlg.MCSBuilder;
                        // Ricrea l'array aggiungendo il nuovo classificatore
                        comboBoxClassifier.Items.Clear();
                        comboBoxClassifier.Items.AddRange(clBuilderArray);
                        comboBoxClassifier.Items.Add("Crea MCS...");
                        comboBoxClassifier.SelectedItem = dlg.MCSBuilder; // seleziona l'MCS appena creato
                    }
                    else
                    {
                        // seleziona comunque un classificatore
                        comboBoxClassifier.SelectedIndex = 0;
                    }
                }
            }
        }

        private void radioButtonClassifierMode_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == radioButtonClassifierMode && ClassifierMode)
            {
                EnableClassificationMode();
            }
            else if (sender == radioButtonProbEstMode && !ClassifierMode)
            {
                EnableProbEstMode();
            }
        }

        private void EnableClassificationMode()
        {
            comboBoxDensityEst.Visible = false;
            comboBoxClassifier.Visible = true;
            labelClassifierOrMethod.Text = "Classificatore";
            buttonTraining.Text = "Training";
            comboBoxTestSet.Enabled = true;
            ClearTrainingState();
            CreateParametersControl();
            UpdatePreview(trainingSet, false);
        }

        private void EnableProbEstMode()
        {
            comboBoxDensityEst.Visible = true;
            comboBoxClassifier.Visible = false;
            labelClassifierOrMethod.Text = "Metodo";
            buttonTraining.Text = "Stima";
            comboBoxTestSet.Enabled = false;
            ClearTrainingState();
            CreateParametersControl();
            UpdatePreview(trainingSet, false);
        }

        private void comboBoxProbEst_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateParametersControl();
        }
    }
}