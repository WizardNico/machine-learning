namespace PRLab
{
    partial class TryNeuralNetworks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TryNeuralNetworks));
            this.splitPreview = new System.Windows.Forms.SplitContainer();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.panelChooseSets = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.labelClassifierOrMethod = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxTestSet = new System.Windows.Forms.ComboBox();
            this.comboBoxClassifier = new System.Windows.Forms.ComboBox();
            this.comboBoxTrainingSet = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelPreview = new System.Windows.Forms.Panel();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonStop = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDownRefreshCount = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.labelMessage = new System.Windows.Forms.Label();
            this.buttonTest = new System.Windows.Forms.Button();
            this.buttonTraining = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitPreview)).BeginInit();
            this.splitPreview.Panel1.SuspendLayout();
            this.splitPreview.Panel2.SuspendLayout();
            this.splitPreview.SuspendLayout();
            this.panelChooseSets.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRefreshCount)).BeginInit();
            this.SuspendLayout();
            // 
            // splitPreview
            // 
            this.splitPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitPreview.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitPreview.Location = new System.Drawing.Point(0, 0);
            this.splitPreview.Name = "splitPreview";
            // 
            // splitPreview.Panel1
            // 
            this.splitPreview.Panel1.Controls.Add(this.groupBoxSettings);
            this.splitPreview.Panel1.Controls.Add(this.panelChooseSets);
            this.splitPreview.Panel1MinSize = 200;
            // 
            // splitPreview.Panel2
            // 
            this.splitPreview.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitPreview.Panel2.Controls.Add(this.buttonStop);
            this.splitPreview.Panel2.Controls.Add(this.label4);
            this.splitPreview.Panel2.Controls.Add(this.numericUpDownRefreshCount);
            this.splitPreview.Panel2.Controls.Add(this.label3);
            this.splitPreview.Panel2.Controls.Add(this.labelMessage);
            this.splitPreview.Panel2.Controls.Add(this.buttonTest);
            this.splitPreview.Panel2.Controls.Add(this.buttonTraining);
            this.splitPreview.Panel2.Controls.Add(this.buttonClose);
            this.splitPreview.Size = new System.Drawing.Size(1127, 485);
            this.splitPreview.SplitterDistance = 210;
            this.splitPreview.TabIndex = 0;
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSettings.Location = new System.Drawing.Point(4, 99);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(199, 383);
            this.groupBoxSettings.TabIndex = 1;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Parametri";
            // 
            // panelChooseSets
            // 
            this.panelChooseSets.Controls.Add(this.label2);
            this.panelChooseSets.Controls.Add(this.labelClassifierOrMethod);
            this.panelChooseSets.Controls.Add(this.label1);
            this.panelChooseSets.Controls.Add(this.comboBoxTestSet);
            this.panelChooseSets.Controls.Add(this.comboBoxClassifier);
            this.panelChooseSets.Controls.Add(this.comboBoxTrainingSet);
            this.panelChooseSets.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChooseSets.Location = new System.Drawing.Point(0, 0);
            this.panelChooseSets.Name = "panelChooseSets";
            this.panelChooseSets.Size = new System.Drawing.Size(210, 93);
            this.panelChooseSets.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Test set";
            // 
            // labelClassifierOrMethod
            // 
            this.labelClassifierOrMethod.AutoSize = true;
            this.labelClassifierOrMethod.Location = new System.Drawing.Point(7, 8);
            this.labelClassifierOrMethod.Name = "labelClassifierOrMethod";
            this.labelClassifierOrMethod.Size = new System.Drawing.Size(69, 13);
            this.labelClassifierOrMethod.TabIndex = 2;
            this.labelClassifierOrMethod.Text = "Classificatore";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Training set";
            // 
            // comboBoxTestSet
            // 
            this.comboBoxTestSet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTestSet.FormattingEnabled = true;
            this.comboBoxTestSet.Location = new System.Drawing.Point(82, 59);
            this.comboBoxTestSet.Name = "comboBoxTestSet";
            this.comboBoxTestSet.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTestSet.TabIndex = 7;
            this.comboBoxTestSet.SelectedIndexChanged += new System.EventHandler(this.comboBoxTestSet_SelectedIndexChanged);
            // 
            // comboBoxClassifier
            // 
            this.comboBoxClassifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxClassifier.FormattingEnabled = true;
            this.comboBoxClassifier.Location = new System.Drawing.Point(82, 5);
            this.comboBoxClassifier.Name = "comboBoxClassifier";
            this.comboBoxClassifier.Size = new System.Drawing.Size(121, 21);
            this.comboBoxClassifier.TabIndex = 3;
            this.comboBoxClassifier.SelectedIndexChanged += new System.EventHandler(this.comboBoxClassifier_SelectedIndexChanged);
            // 
            // comboBoxTrainingSet
            // 
            this.comboBoxTrainingSet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTrainingSet.FormattingEnabled = true;
            this.comboBoxTrainingSet.Location = new System.Drawing.Point(82, 32);
            this.comboBoxTrainingSet.Name = "comboBoxTrainingSet";
            this.comboBoxTrainingSet.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTrainingSet.TabIndex = 5;
            this.comboBoxTrainingSet.SelectedIndexChanged += new System.EventHandler(this.comboBoxTrainingSet_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panelPreview, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chart, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(908, 433);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // panelPreview
            // 
            this.panelPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPreview.Location = new System.Drawing.Point(3, 3);
            this.panelPreview.Name = "panelPreview";
            this.panelPreview.Size = new System.Drawing.Size(448, 427);
            this.panelPreview.TabIndex = 0;
            // 
            // chart
            // 
            chartArea1.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea1);
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            legend1.Position.Auto = false;
            legend1.Position.Height = 8.857809F;
            legend1.Position.Width = 18.41004F;
            legend1.Position.X = 78.58996F;
            legend1.Position.Y = 5F;
            this.chart.Legends.Add(legend1);
            this.chart.Location = new System.Drawing.Point(457, 3);
            this.chart.Name = "chart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Training set";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Test set";
            this.chart.Series.Add(series1);
            this.chart.Series.Add(series2);
            this.chart.Size = new System.Drawing.Size(448, 427);
            this.chart.TabIndex = 1;
            this.chart.Text = "chart1";
            // 
            // buttonStop
            // 
            this.buttonStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonStop.Location = new System.Drawing.Point(165, 459);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 8;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(364, 464);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "iterazioni";
            // 
            // numericUpDownRefreshCount
            // 
            this.numericUpDownRefreshCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownRefreshCount.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownRefreshCount.Location = new System.Drawing.Point(313, 458);
            this.numericUpDownRefreshCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownRefreshCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRefreshCount.Name = "numericUpDownRefreshCount";
            this.numericUpDownRefreshCount.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownRefreshCount.TabIndex = 6;
            this.numericUpDownRefreshCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownRefreshCount.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(246, 464);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Refresh ogni";
            // 
            // labelMessage
            // 
            this.labelMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMessage.Location = new System.Drawing.Point(3, 436);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(907, 20);
            this.labelMessage.TabIndex = 4;
            this.labelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonTest
            // 
            this.buttonTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTest.Enabled = false;
            this.buttonTest.Location = new System.Drawing.Point(84, 459);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(75, 23);
            this.buttonTest.TabIndex = 2;
            this.buttonTest.Text = "Test";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // buttonTraining
            // 
            this.buttonTraining.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTraining.Location = new System.Drawing.Point(3, 459);
            this.buttonTraining.Name = "buttonTraining";
            this.buttonTraining.Size = new System.Drawing.Size(75, 23);
            this.buttonTraining.TabIndex = 1;
            this.buttonTraining.Text = "Training";
            this.buttonTraining.UseVisualStyleBackColor = true;
            this.buttonTraining.Click += new System.EventHandler(this.buttonTraining_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(835, 459);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Chiudi";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // TryNeuralNetworks
            // 
            this.AcceptButton = this.buttonTraining;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(1127, 485);
            this.ControlBox = false;
            this.Controls.Add(this.splitPreview);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(750, 450);
            this.Name = "TryNeuralNetworks";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Prova reti neurali";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UseClassifier_FormClosed);
            this.Load += new System.EventHandler(this.UseClassifier_Load);
            this.splitPreview.Panel1.ResumeLayout(false);
            this.splitPreview.Panel2.ResumeLayout(false);
            this.splitPreview.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPreview)).EndInit();
            this.splitPreview.ResumeLayout(false);
            this.panelChooseSets.ResumeLayout(false);
            this.panelChooseSets.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRefreshCount)).EndInit();
            this.ResumeLayout(false);

}

        #endregion

        private System.Windows.Forms.SplitContainer splitPreview;
        private System.Windows.Forms.Panel panelChooseSets;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTestSet;
        private System.Windows.Forms.ComboBox comboBoxTrainingSet;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel panelPreview;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.Button buttonTraining;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Label labelClassifierOrMethod;
        private System.Windows.Forms.ComboBox comboBoxClassifier;
        private System.Windows.Forms.NumericUpDown numericUpDownRefreshCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
    }
}