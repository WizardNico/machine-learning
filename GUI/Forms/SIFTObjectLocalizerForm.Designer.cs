﻿namespace PRLab.GUI.Forms
{
  partial class SIFTObjectLocalizerForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
      this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
      this.buttonLoadTestImage = new System.Windows.Forms.Button();
      this.imageViewerTestImage = new BioLab.GUI.DataViewers.ImageViewer();
      this.panel1 = new System.Windows.Forms.Panel();
      this.numericUpDownDistanceThr = new System.Windows.Forms.NumericUpDown();
      this.numericUpDownStepPercentage = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.numericUpDownScaleIncrementStep = new System.Windows.Forms.NumericUpDown();
      this.numericUpDownMaxScale = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.numericUpDownMinSCale = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.buttonLocalize = new System.Windows.Forms.Button();
      this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
      this.buttonLoadTrainingSet = new System.Windows.Forms.Button();
      this.panelTrainingSet = new System.Windows.Forms.Panel();
      this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
      this.tableLayoutPanel1.SuspendLayout();
      this.tableLayoutPanel2.SuspendLayout();
      this.tableLayoutPanel3.SuspendLayout();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDistanceThr)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStepPercentage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScaleIncrementStep)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxScale)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinSCale)).BeginInit();
      this.tableLayoutPanel4.SuspendLayout();
      this.SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
      this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(942, 706);
      this.tableLayoutPanel1.TabIndex = 0;
      // 
      // tableLayoutPanel2
      // 
      this.tableLayoutPanel2.ColumnCount = 1;
      this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
      this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 1);
      this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel2.Location = new System.Drawing.Point(285, 3);
      this.tableLayoutPanel2.Name = "tableLayoutPanel2";
      this.tableLayoutPanel2.RowCount = 2;
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
      this.tableLayoutPanel2.Size = new System.Drawing.Size(654, 700);
      this.tableLayoutPanel2.TabIndex = 0;
      // 
      // tableLayoutPanel3
      // 
      this.tableLayoutPanel3.ColumnCount = 1;
      this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel3.Controls.Add(this.buttonLoadTestImage, 0, 0);
      this.tableLayoutPanel3.Controls.Add(this.imageViewerTestImage, 0, 1);
      this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
      this.tableLayoutPanel3.Name = "tableLayoutPanel3";
      this.tableLayoutPanel3.RowCount = 2;
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel3.Size = new System.Drawing.Size(648, 594);
      this.tableLayoutPanel3.TabIndex = 0;
      // 
      // buttonLoadTestImage
      // 
      this.buttonLoadTestImage.Location = new System.Drawing.Point(3, 3);
      this.buttonLoadTestImage.Name = "buttonLoadTestImage";
      this.buttonLoadTestImage.Size = new System.Drawing.Size(110, 23);
      this.buttonLoadTestImage.TabIndex = 0;
      this.buttonLoadTestImage.Text = "Load test image ...";
      this.buttonLoadTestImage.UseVisualStyleBackColor = true;
      this.buttonLoadTestImage.Click += new System.EventHandler(this.buttonLoadTestImage_Click);
      // 
      // imageViewerTestImage
      // 
      this.imageViewerTestImage.BackColor = System.Drawing.SystemColors.Control;
      this.imageViewerTestImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.imageViewerTestImage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.imageViewerTestImage.Location = new System.Drawing.Point(3, 33);
      this.imageViewerTestImage.Name = "imageViewerTestImage";
      this.imageViewerTestImage.Size = new System.Drawing.Size(642, 558);
      this.imageViewerTestImage.TabIndex = 1;
      this.imageViewerTestImage.Paint += new System.Windows.Forms.PaintEventHandler(this.imageViewerTestImage_Paint);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.numericUpDownDistanceThr);
      this.panel1.Controls.Add(this.numericUpDownStepPercentage);
      this.panel1.Controls.Add(this.label5);
      this.panel1.Controls.Add(this.label4);
      this.panel1.Controls.Add(this.label3);
      this.panel1.Controls.Add(this.numericUpDownScaleIncrementStep);
      this.panel1.Controls.Add(this.numericUpDownMaxScale);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.numericUpDownMinSCale);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.buttonLocalize);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(3, 603);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(648, 94);
      this.panel1.TabIndex = 1;
      // 
      // numericUpDownDistanceThr
      // 
      this.numericUpDownDistanceThr.DecimalPlaces = 2;
      this.numericUpDownDistanceThr.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
      this.numericUpDownDistanceThr.Location = new System.Drawing.Point(273, 3);
      this.numericUpDownDistanceThr.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numericUpDownDistanceThr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownDistanceThr.Name = "numericUpDownDistanceThr";
      this.numericUpDownDistanceThr.Size = new System.Drawing.Size(45, 20);
      this.numericUpDownDistanceThr.TabIndex = 10;
      this.numericUpDownDistanceThr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.numericUpDownDistanceThr.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownDistanceThr.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
      // 
      // numericUpDownStepPercentage
      // 
      this.numericUpDownStepPercentage.DecimalPlaces = 2;
      this.numericUpDownStepPercentage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownStepPercentage.Location = new System.Drawing.Point(273, 30);
      this.numericUpDownStepPercentage.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numericUpDownStepPercentage.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownStepPercentage.Name = "numericUpDownStepPercentage";
      this.numericUpDownStepPercentage.Size = new System.Drawing.Size(45, 20);
      this.numericUpDownStepPercentage.TabIndex = 9;
      this.numericUpDownStepPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.numericUpDownStepPercentage.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownStepPercentage.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(180, 32);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(87, 13);
      this.label5.TabIndex = 8;
      this.label5.Text = "StepPercentage:";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(180, 5);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(68, 13);
      this.label4.TabIndex = 7;
      this.label4.Text = "DistanceThr:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(3, 58);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(106, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "ScaleIncrementStep:";
      // 
      // numericUpDownScaleIncrementStep
      // 
      this.numericUpDownScaleIncrementStep.DecimalPlaces = 2;
      this.numericUpDownScaleIncrementStep.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownScaleIncrementStep.Location = new System.Drawing.Point(115, 56);
      this.numericUpDownScaleIncrementStep.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numericUpDownScaleIncrementStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownScaleIncrementStep.Name = "numericUpDownScaleIncrementStep";
      this.numericUpDownScaleIncrementStep.Size = new System.Drawing.Size(45, 20);
      this.numericUpDownScaleIncrementStep.TabIndex = 5;
      this.numericUpDownScaleIncrementStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.numericUpDownScaleIncrementStep.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownScaleIncrementStep.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
      // 
      // numericUpDownMaxScale
      // 
      this.numericUpDownMaxScale.DecimalPlaces = 2;
      this.numericUpDownMaxScale.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownMaxScale.Location = new System.Drawing.Point(115, 30);
      this.numericUpDownMaxScale.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numericUpDownMaxScale.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownMaxScale.Name = "numericUpDownMaxScale";
      this.numericUpDownMaxScale.Size = new System.Drawing.Size(45, 20);
      this.numericUpDownMaxScale.TabIndex = 4;
      this.numericUpDownMaxScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.numericUpDownMaxScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownMaxScale.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 32);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(57, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "MaxScale:";
      // 
      // numericUpDownMinSCale
      // 
      this.numericUpDownMinSCale.DecimalPlaces = 2;
      this.numericUpDownMinSCale.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownMinSCale.Location = new System.Drawing.Point(115, 3);
      this.numericUpDownMinSCale.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numericUpDownMinSCale.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownMinSCale.Name = "numericUpDownMinSCale";
      this.numericUpDownMinSCale.Size = new System.Drawing.Size(45, 20);
      this.numericUpDownMinSCale.TabIndex = 2;
      this.numericUpDownMinSCale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.numericUpDownMinSCale.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.numericUpDownMinSCale.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(54, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "MinScale:";
      // 
      // buttonLocalize
      // 
      this.buttonLocalize.Location = new System.Drawing.Point(535, 68);
      this.buttonLocalize.Name = "buttonLocalize";
      this.buttonLocalize.Size = new System.Drawing.Size(110, 23);
      this.buttonLocalize.TabIndex = 0;
      this.buttonLocalize.Text = "Localize";
      this.buttonLocalize.UseVisualStyleBackColor = true;
      this.buttonLocalize.Click += new System.EventHandler(this.buttonLocalize_Click);
      // 
      // tableLayoutPanel4
      // 
      this.tableLayoutPanel4.ColumnCount = 1;
      this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.Controls.Add(this.buttonLoadTrainingSet, 0, 0);
      this.tableLayoutPanel4.Controls.Add(this.panelTrainingSet, 0, 1);
      this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
      this.tableLayoutPanel4.Name = "tableLayoutPanel4";
      this.tableLayoutPanel4.RowCount = 2;
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
      this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel4.Size = new System.Drawing.Size(276, 700);
      this.tableLayoutPanel4.TabIndex = 1;
      // 
      // buttonLoadTrainingSet
      // 
      this.buttonLoadTrainingSet.Location = new System.Drawing.Point(3, 3);
      this.buttonLoadTrainingSet.Name = "buttonLoadTrainingSet";
      this.buttonLoadTrainingSet.Size = new System.Drawing.Size(110, 23);
      this.buttonLoadTrainingSet.TabIndex = 0;
      this.buttonLoadTrainingSet.Text = "Load training set ...";
      this.buttonLoadTrainingSet.UseVisualStyleBackColor = true;
      this.buttonLoadTrainingSet.Click += new System.EventHandler(this.buttonLoadTrainingSet_Click);
      // 
      // panelTrainingSet
      // 
      this.panelTrainingSet.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelTrainingSet.Location = new System.Drawing.Point(3, 33);
      this.panelTrainingSet.Name = "panelTrainingSet";
      this.panelTrainingSet.Size = new System.Drawing.Size(270, 664);
      this.panelTrainingSet.TabIndex = 1;
      // 
      // backgroundWorker
      // 
      this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
      this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
      // 
      // SIFTObjectLocalizerForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(942, 706);
      this.Controls.Add(this.tableLayoutPanel1);
      this.Name = "SIFTObjectLocalizerForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "SIFTObjectLocalizerForm";
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel2.ResumeLayout(false);
      this.tableLayoutPanel3.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDistanceThr)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStepPercentage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownScaleIncrementStep)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxScale)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinSCale)).EndInit();
      this.tableLayoutPanel4.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    private System.Windows.Forms.Button buttonLoadTestImage;
    private BioLab.GUI.DataViewers.ImageViewer imageViewerTestImage;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    private System.Windows.Forms.Button buttonLoadTrainingSet;
    private System.Windows.Forms.Panel panelTrainingSet;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button buttonLocalize;
    private System.Windows.Forms.NumericUpDown numericUpDownMaxScale;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown numericUpDownMinSCale;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown numericUpDownScaleIncrementStep;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown numericUpDownDistanceThr;
    private System.Windows.Forms.NumericUpDown numericUpDownStepPercentage;
    private System.Windows.Forms.Label label5;
    private System.ComponentModel.BackgroundWorker backgroundWorker;
  }
}