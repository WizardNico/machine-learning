
namespace PRLab
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Forms;
    using BioLab.Classification.DensityEstimation;
    using BioLab.Classification.Supervised;
    using BioLab.Classification.Unsupervised;
    using BioLab.Common;
    using BioLab.GUI.Common;
    using BioLab.GUI.DataViewers;
    using BioLab.GUI.Forms;
    using BioLab.ImageProcessing;
    using BioLab.ImageProcessing.ImageFormats;
    using PRLab.EsercitazioniVR;
    using PRLab.Properties;
    using PRLab.GUI.Forms;
    using PRLab.GUI.DataViewers;
    using PRLab.EsercitazioniML;
    using BioLab.DimensionalityReduction;


    public partial class MainForm : Form, IAlgorithmPreviewFormDataProvider
    {
        int newObjectCounter = 1;

        public MainForm()
        {
            InitializeComponent();
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IClipboardAware viewer = GetActiveMdiChildClipboardAwareViewer();
            if (viewer != null)
            {
                try
                {
                    viewer.Copy();
                }
                catch (Exception ex)
                {
                    ExceptionMessageBox.Show(ex, "Error", "Cannot copy to the clipboard");
                }
            }
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IClipboardAware viewer = GetActiveMdiChildClipboardAwareViewer();
            if (viewer != null)
            {
                try
                {
                    viewer.Paste();
                }
                catch (Exception ex)
                {
                    ExceptionMessageBox.Show(ex, "Error", "Cannot paste from the clipboard");
                }
            }
            UpdateStatusLabel();
        }

        private void menuPasteNew_Click(object sender, EventArgs e)
        {
            ImageBase image;
            try
            {
                image = FormatConverter.ConvertImageFromClipboard();
            }
            catch (Exception ex)
            {
                ExceptionMessageBox.Show(ex, "Error", "Cannot paste from the clipboard");
                return;
            }
            if (image != null)
            {
                CreateChildWindowWithDataViewer("Clipboard image", image);
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Application.Idle += new EventHandler(Application_Idle);
            UpdateStatusLabel();

            // Creazione dinamica menu con algoritmi
            Type[] types = GetTypesWithAlgorithmAttribute();
            AppendOperationsToMenu("Basic operations", basicOperationsToolStripMenuItem.DropDown, types);
            AppendOperationsToMenu("Filtering", filteringToolStripMenuItem.DropDown, types);
            AppendOperationsToMenu("Digital topology", digitalTopologyToolStripMenuItem.DropDown, types);
            AppendOperationsToMenu("Binary morphology", binaryMorphologyToolStripMenuItem.DropDown, types);
            AppendOperationsToMenu("Grayscale morphology", grayscaleMorphologyToolStripMenuItem.DropDown, types);
            AppendOperationsToMenu("Localization", detectionToolStripMenuItem.DropDown, types);
        }

        private void AppendOperationsToMenu(string category, ToolStripDropDown menu, Type[] types)
        {
            var itemsToAdd = new List<KeyValuePair<Type, AlgorithmInfoAttribute>>();
            foreach (Type t in types)
            {
                object[] attributes = t.GetCustomAttributes(typeof(AlgorithmInfoAttribute), false);
                if (attributes != null && attributes.Length == 1)
                {
                    AlgorithmInfoAttribute attribute = (AlgorithmInfoAttribute)attributes[0];
                    if (attribute.Category == category)
                    {
                        itemsToAdd.Add(new KeyValuePair<Type, AlgorithmInfoAttribute>(t, attribute));
                    }
                }
            }

            itemsToAdd.Sort((i1, i2) => Comparer<string>.Default.Compare(i1.Value.Name, i2.Value.Name));
            foreach (var item in itemsToAdd)
            {
                ToolStripMenuItem menuItem = new ToolStripMenuItem(item.Value.Name);
                menuItem.Tag = item.Key;
                menuItem.Click += new EventHandler(automaticMenuItem_Click);
                menu.Items.Add(menuItem);
            }
        }

        private Type[] GetTypesWithAlgorithmAttribute()
        {
            List<Type> typeList = new List<Type>(1000);
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var a in assemblies)
            {
                Type[] types = a.GetExportedTypes();
                foreach (var t in types)
                {
                    object[] attributes = t.GetCustomAttributes(typeof(AlgorithmInfoAttribute), false);
                    if (attributes != null && attributes.Length == 1)
                    {
                        typeList.Add(t);
                    }
                }
            }
            return typeList.ToArray();
        }

        void automaticMenuItem_Click(object sender, EventArgs e)
        {
            OpenAlgorithmPreview((Type)((ToolStripMenuItem)sender).Tag);
        }

        void Application_Idle(object sender, EventArgs e)
        {
            UpdateToolStrips();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            using (var openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Multiselect = true;
                openFileDialog.Filter = "Images (*.jpg;*.png;*.bmp;*.gif;*.tif)|*.jpg;*.png;*.bmp;*.gif;*.tif|Feature Vector Set Files (*.fvsbin)|*.fvsbin|Feature Vector Set Text Files (*.fvstxt)|*.fvstxt|Image With Regions Text Files (*.mkrtxt)|*.mkrtxt|All Files (*.*)|*.*";
                openFileDialog.FilterIndex = Settings.Default.OpenDialogFilterIndex;
                if (openFileDialog.ShowDialog(this) == DialogResult.OK)
                {
                    Settings.Default.OpenDialogFilterIndex = openFileDialog.FilterIndex;
                    foreach (string path in openFileDialog.FileNames)
                    {
                        OpenFile(path);
                    }
                }
            }
        }

        private void OpenFile(string fileName)
        {
            Update(); // cosmetic
            using (new WaitCursor())
            {
                Data data;
                try
                {
                    switch (Path.GetExtension(fileName))
                    {
                        case (".jpg"):
                        case (".png"):
                        case (".bmp"):
                        case (".gif"):
                        case (".tif"):
                            data = ImageBase.LoadFromFile(fileName);
                            break;
                        case (".fvsbin"):
                            data = FeatureVectorSet.LoadFromFile(fileName);
                            break;
                        case (".fvstxt"):
                            data = FeatureVectorSet.LoadFromTextFile(fileName);
                            break;
                        case (".mkrtxt"):
                            var mkr = new ImageWithMarkedRegions(ImageBase.LoadFromFile(Path.ChangeExtension(fileName, ".png")));
                            mkr.LoadRegionsFromTextFile(fileName);
                            data = mkr;
                            break;
                        default:
                            throw new Exception("File format not supported");
                    }


                }
                catch (Exception ex)
                {
                    ExceptionMessageBox.Show(ex, "Error", ex.Message);
                    return;
                }
                CreateChildWindowWithDataViewer(fileName, data);
                newObjectCounter++;
                return;
            }
        }

        private MdiChildForm CreateChildWindowWithDataViewer(string title, object data)
        {
            try
            {
                DataViewer viewer = null;

                if (data is FeatureVectorSet)
                {
                    viewer = new PRFeatureVectorSetViewer((FeatureVectorSet) data);
                    //viewer.Data = data;
                }
                else
                {
                    viewer = DataViewerBuilder.CreateDataViewer(data);
                }

                MdiChildForm child = new MdiChildForm(this, viewer, Path.GetFileNameWithoutExtension(title));
                child.Show();

                return child;
            }
            catch (ArgumentException ex)
            {
                ExceptionMessageBox.Show(ex, "Error", "Cannot create data viewer");
                return null;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoSaveAs();
        }

        private void DoSaveAs()
        {
            using (var saveFileDialog = new SaveFileDialog())
            {
                var child = (MdiChildForm)ActiveMdiChild;

                if (child.Viewer.Data is FeatureVectorSet)
                {
                    saveFileDialog.Filter = "Feature Vector Set Binary Files |*.fvsbin|Feature Vector Set Text Files (*.fvstxt)|*.fvstxt";
                }
                else if (child.Viewer.Data is ImageWithMarkedRegions)
                {
                    saveFileDialog.Filter = "Image With Regions Text Files (*.mkrtxt)|*.mkrtxt";
                }
                else if (child.Viewer.Data is ImageBase)
                {
                    saveFileDialog.Filter = "PNG (*.png)|*.png|BMP (*.bmp)|*.bmp|JPEG (*.jpg)|*.jpg|TIFF (*.tif)|*.tif|GIF (*.gif)|*.gif";
                }

                saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
                {
                    child.LastSavedPath = saveFileDialog.FileName;
                    DoSave();
                }
            }
        }

        private void DoSave()
        {
            try
            {
                var child = (MdiChildForm)ActiveMdiChild;
                var data = child.Viewer.Data as Data;
                data.Description = Path.GetFileNameWithoutExtension(child.LastSavedPath);

                if (data is FeatureVectorSet &&
                    Path.GetExtension(child.LastSavedPath) == ".fvstxt")
                {
                    ((FeatureVectorSet)data).SaveToTextFile(child.LastSavedPath);
                }
                else if (data is ImageWithMarkedRegions)
                {
                    ((ImageWithMarkedRegions)data).SaveRegionsToTextFile(child.LastSavedPath);
                    var imgPath = Path.ChangeExtension(child.LastSavedPath, ".png");
                    if (!File.Exists(imgPath))
                    {
                        ((RgbImage<byte>)data).SaveToFile(imgPath);
                    }
                }
                else
                {
                    data.SaveToFile(child.LastSavedPath);
                }
            }
            catch (Exception ex)
            {
                ExceptionMessageBox.Show(ex, "Cannot save file.");
            }
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        //private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //  toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        //}


        private void EnableToolStripItems(bool enable, params ToolStripItem[] items)
        {
            foreach (ToolStripItem item in items)
                item.Enabled = enable;
        }

        private void MainForm_MdiChildActivate(object sender, EventArgs e)
        {
            UpdateStatusLabel();
        }

        private void UpdateStatusLabel()
        {
            var child = ActiveMdiChild as MdiChildForm;
            if (child != null)
            {
                toolStripStatusLabel.Text = child.Viewer.Data.ToString();
            }
            else
            {
                toolStripStatusLabel.Text = string.Empty;
            }
        }

        private IZoomable GetActiveMdiChildZoomableViewer()
        {
            var child = ActiveMdiChild as MdiChildForm;
            return child != null ? child.ViewerAsZoomable : null;
        }

        private IClipboardAware GetActiveMdiChildClipboardAwareViewer()
        {
            var child = ActiveMdiChild as MdiChildForm;
            return child != null ? child.Viewer as IClipboardAware : null;
        }

        private Image<byte> GetActiveMdiChildByteImage()
        {
            var child = ActiveMdiChild as MdiChildForm;
            if (child == null)
                return null;
            var image = child.Viewer.Data as ImageBase;
            if (image == null)
                return null;
            return image.ToByteImage();
        }

        private bool ActiveMdiChildSupportSave()
        {
            var child = ActiveMdiChild as MdiChildForm;
            if (child == null)
                return false;

            return (child.Viewer.Data is ImageBase) || (child.Viewer.Data is FeatureVectorSet);
        }

        private void UpdateToolStrips()
        {
            // Zoom
            IZoomable zoomableViewer = GetActiveMdiChildZoomableViewer();
            bool zoomable = zoomableViewer != null;

            EnableToolStripItems(zoomable,
                menuFitToWindow,
                menuZoomIn, menuZoomOut, menuZoomCombo
            );

            if (zoomable)
            {
                menuFitToWindow.Checked = zoomableViewer.FitToScreenSize;
                if (!viewMenu.Pressed)
                    menuZoomCombo.Text = string.Format(CultureInfo.InvariantCulture, "{0:p}", zoomableViewer.ZoomLevel);
            }
            else
            {
                menuFitToWindow.Checked = false;
            }

            // Clipboard
            IClipboardAware clipboardAwareViewer = GetActiveMdiChildClipboardAwareViewer();
            menuCopy.Enabled = clipboardAwareViewer != null && clipboardAwareViewer.CanCopy;
            menuPaste.Enabled = clipboardAwareViewer != null && clipboardAwareViewer.CanPaste;
            menuPasteNew.Enabled = Clipboard.ContainsImage();

            // Save
            saveToolStripMenuItem.Enabled = saveAsToolStripMenuItem.Enabled = ActiveMdiChildSupportSave();

            menuMarkRegions.Enabled = ActiveMdiChild != null &&
                                      ((MdiChildForm)ActiveMdiChild).Viewer.Data is ImageBase &&
                                      !(((MdiChildForm)ActiveMdiChild).Viewer.Data is ImageWithMarkedRegions);
        }

        private void menuZoomIn_Click(object sender, EventArgs e)
        {
            IZoomable zoomableViewer = GetActiveMdiChildZoomableViewer();
            if (zoomableViewer != null)
            {
                zoomableViewer.ZoomIn();
            }
        }

        private void menuZoomOut_Click(object sender, EventArgs e)
        {
            IZoomable zoomableViewer = GetActiveMdiChildZoomableViewer();
            if (zoomableViewer != null)
            {
                zoomableViewer.ZoomOut();
            }
        }

        private void menuFitToWindow_Click(object sender, EventArgs e)
        {
            IZoomable zoomableViewer = GetActiveMdiChildZoomableViewer();
            if (zoomableViewer != null)
            {
                zoomableViewer.FitToScreenSize = !zoomableViewer.FitToScreenSize;
            }
        }

        private void menuZoomCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            IZoomable zoomableViewer = GetActiveMdiChildZoomableViewer();
            if (zoomableViewer != null)
            {
                string zoomText = (string)menuZoomCombo.SelectedItem;
                double zoomValue = double.Parse(zoomText.Remove(zoomText.Length - 1)) / 100;
                zoomValue = Math.Max(zoomableViewer.MinZoomLevel, zoomValue);
                zoomValue = Math.Min(zoomableViewer.MaxZoomLevel, zoomValue);
                zoomableViewer.ZoomLevel = zoomValue;
            }
        }

        private void menuZoomCombo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                IZoomable zoomableViewer = GetActiveMdiChildZoomableViewer();
                if (zoomableViewer != null)
                {
                    string zoomText = (string)menuZoomCombo.Text;
                    zoomText.Replace("%", string.Empty);
                    double zoomValue;
                    try
                    {
                        zoomValue = double.Parse(zoomText) / 100;
                    }
                    catch
                    {
                        return;
                    }
                    zoomValue = Math.Max(zoomableViewer.MinZoomLevel, zoomValue);
                    zoomValue = Math.Min(zoomableViewer.MaxZoomLevel, zoomValue);
                    zoomableViewer.ZoomLevel = zoomValue;
                }
            }
        }


        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Idle -= Application_Idle;
        }


        private void OpenAlgorithmPreview(Type algorithmType)
        {
            if (algorithmType.IsGenericTypeDefinition)
            {
                // - Un supporto minimale per algoritmi con parametri generici -
                // Per ora tenta semplicemente di utilizzare byte o Image<byte> come tipo da sostituire
                Type[] genericArguments = algorithmType.GetGenericArguments();
                Type[] types = new Type[genericArguments.Length];
                for (int i = 0; i < types.Length; i++)
                {
                    Type[] constraints = genericArguments[i].GetGenericParameterConstraints();
                    if (constraints.Contains(typeof(System.ValueType)))
                    {
                        // Prova a sostituirlo con byte
                        types[i] = typeof(byte);
                    }
                    else
                    {
                        // Prova a sostituirlo con Image<byte>
                        types[i] = typeof(Image<byte>);
                    }
                }
                try
                {
                    algorithmType = algorithmType.MakeGenericType(types);
                }
                catch (Exception ex)
                {
                    ExceptionMessageBox.Show(ex, "Error substituting the generic parameters");
                    return;
                }
            }

            try
            {
                using (AlgorithmPreviewForm form = new AlgorithmPreviewForm(algorithmType, this))
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        foreach (var property in algorithmType.GetProperties())
                        {
                            if (property.GetCustomAttributes(typeof(AlgorithmOutputAttribute), true).Length > 0)
                            {
                                // It is an "output" property: get the result if available
                                MethodInfo getMethod = property.GetGetMethod();
                                if (getMethod != null)
                                {
                                    object data = getMethod.Invoke(form.Algorithm, null);
                                    if (data != null)
                                    {
                                        CreateChildWindowWithDataViewer(string.Format("{0}-{1}-{2}", algorithmType.Name, property.Name, newObjectCounter), data);
                                        newObjectCounter++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (TargetInvocationException ex)
            {
                ExceptionMessageBox.Show(ex.InnerException != null ? ex.InnerException : ex, "An error occurred during the algorithm executon");
            }
            catch (Exception ex)
            {
                ExceptionMessageBox.Show(ex, "An error occurred during the algorithm");
            }
        }

        private void menuBinarization_Click(object sender, EventArgs e)
        {
            OpenAlgorithmPreview(typeof(ImageBinarization));
        }


        private void menuBrightnessAdjustment_Click(object sender, EventArgs e)
        {
            OpenAlgorithmPreview(typeof(BrightnessAdjustment));
        }


        private void menuImageArithmetic_Click(object sender, EventArgs e)
        {
            OpenAlgorithmPreview(typeof(ImageArithmetic));
        }

        public IEnumerable<AlgorithmPreviewFormDataItem> GetAvailableData(Type dataType)
        {
            List<AlgorithmPreviewFormDataItem> data = new List<AlgorithmPreviewFormDataItem>();
            foreach (Form f in MdiChildren)
            {
                MdiChildForm child = f as MdiChildForm;
                if (child != null && child.Viewer.Data != null)
                {
                    object d = null;
                    string text = child.FileName;
                    if (dataType == typeof(Image<byte>) && !(child.Viewer.Data is Image<byte>))
                    {
                        // In this case it considers also images that can be converted into an Image<byte>
                        ImageBase image = child.Viewer.Data as ImageBase;
                        if (image != null)
                        {
                            try
                            {
                                d = image.ToByteImage();
                                text += " (Converted)";
                            }
                            catch (NotImplementedException)
                            {
                            }
                        }
                    }
                    else if (dataType == typeof(Image<int>) && !(child.Viewer.Data is Image<int>))
                    {
                        ImageBase image = child.Viewer.Data as ImageBase;
                        if (image != null)
                        {

                            Image<byte> source = child.Viewer.Data as Image<byte>;
                            if (source != null)
                            {
                                try
                                {
                                    source = image.ToByteImage();
                                    text += " (Converted)";
                                    Image<int> dest = new Image<int>(source.Width, source.Height);
                                    for (int i = 0; i < dest.PixelCount; i++)
                                    {
                                        dest[i] = source[i];
                                    }
                                    d = dest;
                                }
                                catch (NotImplementedException)
                                {
                                }
                            }
                        }
                    }
                    else
                    {
                        if (dataType.IsAssignableFrom(child.Viewer.Data.GetType()))
                            d = child.Viewer.Data;
                    }
                    if (d != null)
                    {
                        if (ActiveMdiChild == child) // mette la finestra attiva come primo elemento dell'elenco
                        {
                            data.Insert(0, new AlgorithmPreviewFormDataItem(d, text));
                        }
                        else
                        {
                            data.Add(new AlgorithmPreviewFormDataItem(d, text));
                        }
                    }
                }
            }
            return data;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowAboutBox();
        }

        private void ShowAboutBox()
        {
            using (var f = new AboutBox())
            {
                f.ShowDialog();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var child = ActiveMdiChild as MdiChildForm;
            if (child.NeverSaved)
            {
                DoSaveAs();
            }
            else
            {
                DoSave();
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new NewImageForm())
            {
                if (f.ShowDialog() == DialogResult.OK)
                {
                    CreateChildWindowWithDataViewer(string.Format("Image{0}", newObjectCounter), new Image<byte>((int)f.ImageWidth.Value, (int)f.ImageHeight.Value));
                    newObjectCounter++;
                }
            }
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }



        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string file in files)
                {
                    OpenFile(file);
                }
            }
            catch (Exception ex)
            {
                ExceptionMessageBox.Show(ex, "Error during drag and drop operation");
            }
        }

        private void newFeatureVectorSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var title = string.Format("FeatureVectorSet{0}", newObjectCounter++);
            var vs = new FeatureVectorSet() { Description = title };
            var childForm = CreateChildWindowWithDataViewer(title, vs);

            ((PRFeatureVectorSetViewer)childForm.Viewer).EditMode = true;
            ((PRFeatureVectorSetViewer)childForm.Viewer).ZoomLevel = 0.9;
        }

        private MdiChildForm[] GetAllMdiChildForm()
        {
            var mdiFormList = new List<MdiChildForm>(MdiChildren.Length);
            foreach (var f in MdiChildren)
            {
                var child = f as MdiChildForm;
                if (child != null &&
                    child.Viewer != null &&
                    child.Viewer.Data != null)
                {
                    mdiFormList.Add(child);
                }
            }

            return mdiFormList.ToArray();
        }

        private void esercitazioneBayesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[] {new KnnClassifierBuilder() {K=3},
                                                        new BayesClassifierBuilder(new NormalMLEstimator()),
                                                        new BayesClassifierBuilder(new ParzenEstimator(0.1,ParzenKernelType.Hypercube)),
                                                        new VR_BayesNormalClassifierBuilder(),
                                                        new SvmClassifierBuilder(){Probability=true}};
            var densityEstimators = new DensityEstimator[] { new NormalMLEstimator(), new VR_NormalMLEstimator(), new ParzenEstimator() { KernelType = ParzenKernelType.Hypercube, Bandwidth = 0.1 } };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryClassifiers(classifierBuilders, densityEstimators, sets.ToArray(),false))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void esercitazioneMultiClassificatoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[] {new KnnClassifierBuilder() {K=3},
                                                        new SvmClassifierBuilder(){Probability=true},
                                                        new BayesClassifierBuilder(new ParzenEstimator(0.1,ParzenKernelType.Hypercube)),
                                                        new VR_BayesNormalClassifierBuilder()};
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryMultiClassifiers(classifierBuilders, sets.ToArray()))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void esercitazioneRetiNeuraliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[] {new KnnClassifierBuilder() {K=3},
                                                        new SvmClassifierBuilder(){Probability=true},
                                                        new VR_BayesNormalClassifierBuilder(),
                                                        new BayesClassifierBuilder(new ParzenEstimator(10,ParzenKernelType.Hypercube)),
                                                        new NeuralNetworkClassifierBuilder(),
                                                        new VR_NeuralNetworkClassifierBuilder()
                                                        };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryClassifiers(classifierBuilders, null, sets.ToArray(),false))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void esercitazioneKMeansToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new Clusterizer[] {new KMeanClustering(),
                                                  new EMClustering(),
                                                  new VR_KMeanClustering(),
                                                  };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryClusterizers(classifierBuilders, sets.ToArray()))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void esercitazioneLocalizzazioneCornerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenAlgorithmPreview(typeof(VR_HarrisCornerDetector));
        }

        private void esercitazioneSegmentazioneColoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenAlgorithmPreview(typeof(VR_ColorSegmentation));
        }

        private void markRegionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var child = (MdiChildForm)ActiveMdiChild;
            CreateChildWindowWithDataViewer(child.FileName + "+Regions", new ImageWithMarkedRegions((ImageBase)child.Viewer.Data));
        }

        private void esercitazioneDescrittoriSIFTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenAlgorithmPreview(typeof(VR_DenseSift));
        }

        private void esercitazioneHoughToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenAlgorithmPreview(typeof(VR_HoughLinesDetector));
        }

        private void esercitazioneActiveShapeModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dlg = new TryActiveShapeModels())
            {
                dlg.ShowDialog();
            }
        }

        private void esercitazioneLocalizzazioneOggettiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dlg = new SIFTObjectLocalizerForm())
            {
                dlg.ShowDialog();
            }
        }

        private void esercitazioneIntroduttivaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var featureVectorSet = new MyEsercitazioneIntroduttiva().Generate();

            var title = string.Format("FeatureVectorSet{0}", newObjectCounter++);
            featureVectorSet.Description = title;
            var childForm = CreateChildWindowWithDataViewer(title, featureVectorSet);

            ((PRFeatureVectorSetViewer)childForm.Viewer).EditMode = false;
        }

        private void esercitazioneMLBayesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[]
                                    {
                                        new BayesClassifierBuilder(new NormalMLEstimator()),
                                        new BayesClassifierBuilder(new ParzenEstimator(0.1,ParzenKernelType.Gaussian)),
                                        new MyBayesClassifierBuilder(new MyNormalMLEstimator()),
                                        new MyBayesClassifierBuilder(new MyParzenEstimator(0.1,ParzenKernelType.Gaussian)),
                                    };
            var densityEstimators = new DensityEstimator[]
                                    {
                                        new NormalMLEstimator(),
                                        new ParzenEstimator() { KernelType = ParzenKernelType.Gaussian, Bandwidth = 0.1 },
                                        new MyNormalMLEstimator(),
                                        new MyParzenEstimator() { KernelType = ParzenKernelType.Gaussian, Bandwidth = 0.1 },
                                    };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryClassifiers(classifierBuilders, densityEstimators, sets.ToArray(),false))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void esercitazioneMLKMeansToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new Clusterizer[]
                                        {
                                            new KMeanClustering(),
                                            new MyKMeansClustering(),
                                        };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryClusterizers(classifierBuilders, sets.ToArray()))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void esercitazioneMLMulticlassificatoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[] {
                                                                new KnnClassifierBuilder() {K=3},
                                                                new SvmClassifierBuilder(){Probability=true},
                                                                new BayesClassifierBuilder(new NormalMLEstimator()),
                                                                new BayesClassifierBuilder(new ParzenEstimator(0.1,ParzenKernelType.Gaussian))
                                                            };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new MyTryMultiClassifiers(classifierBuilders, sets.ToArray()))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void selezioneManualeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[]
                                    {
                                        new SvmClassifierBuilder() {Probability=true,Gamma=0.5},
                                    };
            var densityEstimators = new DensityEstimator[]
                                    {
                                    };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryClassifiers(classifierBuilders, densityEstimators, sets.ToArray(), false))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void selezioneAutomaticaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double errValidation;
            double errTest;
            string optimizedParameters;

            using (var folderDlg = new FolderBrowserDialog())
            {
                folderDlg.ShowNewFolderButton = false;
                folderDlg.Description = "Selezionare la cartella in cui sono contenuti i dataset";

                if (folderDlg.ShowDialog() == DialogResult.OK)
                {
                    using (var dbDlg = new SelectFromListForm())
                    {
                        dbDlg.listBox.Items.Add("sepal_l vs petal_l");
                        dbDlg.listBox.Items.Add("sepal_w vs petal_l");
                        dbDlg.listBox.Items.Add("petal_l vs petal_w");
                        dbDlg.listBox.Items.Add("KL (2 dim)");
                        dbDlg.listBox.Items.Add("All feat");
                        
                        dbDlg.Text = "Selezionare dataset";
                        
                        if (dbDlg.ShowDialog() == DialogResult.OK &&
                            dbDlg.listBox.SelectedItem != null)
                        {
                            using (new WaitCursor())
                            {
                                MySvmParameterOptimizer.Execute(folderDlg.SelectedPath, dbDlg.listBox.SelectedItem.ToString(), out errValidation, out errTest, out optimizedParameters);
                            }

                            MessageBox.Show(string.Format(CultureInfo.InvariantCulture, "{0}\nErrValidation: {1:p2}\nErrTest: {2:p2}", optimizedParameters, errValidation, errTest), "Parametri selezionati");
                        }
                    }
                }
            }
        }

        private void esercitazioneMLRetiNeuraliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[]
                                        {
                                            new NeuralNetworkClassifierBuilder(),
                                            new MyNeuralNetworkClassifierBuilder(),
                                        };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryNeuralNetworks(classifierBuilders, null, sets.ToArray(), false))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void classificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[]
                                    {
                                        new KnnClassifierBuilder() {K=3},
                                        new BayesClassifierBuilder(new NormalMLEstimator()),
                                        new BayesClassifierBuilder(new ParzenEstimator(0.1,ParzenKernelType.Gaussian)),
                                        new SvmClassifierBuilder(){Probability=true},
                                    };
            //var densityEstimators = new DensityEstimator[]
            //                        {
            //                            new NormalMLEstimator(),
            //                            new ParzenEstimator() { KernelType = ParzenKernelType.Gaussian, Bandwidth = 0.1 },
            //                        };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                //using (var dlg = new TryClassifiers(classifierBuilders, densityEstimators, sets.ToArray(), false))
                using (var dlg = new TryMultiClassifiers(classifierBuilders, sets.ToArray()))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void clusteringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new Clusterizer[]
                                        {
                                            new KMeanClustering(),
                                            new EMClustering() {ComputeLogarithmicLikelihood=true},
                                        };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryClusterizers(classifierBuilders, sets.ToArray()))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void dimensionalityReductionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dimReductionBuilders = new DimensionalityReductionBuilder[]
                                        {
                                            new PcaTransformBuilder(),
                                            new LdaTransformBuilder(),
                                        };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }
            
            if (sets.Count > 0)
            {
                using (var dlg = new TryDimReduction(dimReductionBuilders, sets.ToArray()))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void neuralNetworksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var classifierBuilders = new ClassifierBuilder[]
                                        {
                                            new NeuralNetworkClassifierBuilder(),
                                        };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryNeuralNetworks(classifierBuilders, null, sets.ToArray(), false))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }

        private void densityEstimationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var densityEstimators = new DensityEstimator[]
                                    {
                                        new NormalMLEstimator(),
                                        new ParzenEstimator() { KernelType = ParzenKernelType.Gaussian, Bandwidth = 0.1 },
                                    };
            var sets = new List<FeatureVectorSet>();
            var mdiChildForms = GetAllMdiChildForm();
            foreach (var child in mdiChildForms)
            {
                if (child.Viewer is FeatureVectorSetViewer)
                {
                    sets.Add((FeatureVectorSet)child.Viewer.Data);
                }
            }

            if (sets.Count > 0)
            {
                using (var dlg = new TryDensityEstimators(densityEstimators, sets.ToArray()))
                {
                    dlg.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Deve essere presente almeno un insieme di vettori", "Errore");
            }
        }
    }
}


