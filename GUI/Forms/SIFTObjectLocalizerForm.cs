﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BioLab.ImageProcessing;
using System.IO;
using BioLab.GUI.DataViewers;
using BioLab.GUI.Common;
using PRLab.EsercitazioniVR;
using System.Globalization;
using System.ComponentModel;

namespace PRLab.GUI.Forms
{
  public partial class SIFTObjectLocalizerForm : Form
  {
    private VR_SIFTObjectLocalizer localizer;
    
    private RgbImage<byte>[][] trainingSet;

    public SIFTObjectLocalizerForm()
    {
      InitializeComponent();

      localizer= new VR_SIFTObjectLocalizer();

      numericUpDownMinSCale.Value=(decimal) localizer.MinScale;
      numericUpDownMaxScale.Value=(decimal) localizer.MaxScale;
      numericUpDownScaleIncrementStep.Value=(decimal) localizer.ScaleIncrementStep;
      numericUpDownDistanceThr.Value=(decimal) localizer.DistanceThr;
      numericUpDownStepPercentage.Value=(decimal) localizer.StepPercentage;
    }

    private void buttonLoadTestImage_Click(object sender, EventArgs e)
    {
      using (var dlg = new OpenFileDialog())
      {
        dlg.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG";
        dlg.Multiselect=false;

        if (dlg.ShowDialog() == DialogResult.OK)
        {
          imageViewerTestImage.Tag = null;
          imageViewerTestImage.Image=ImageBase.LoadFromFile(dlg.FileName);
        }
      }
    }

    private void buttonLoadTrainingSet_Click(object sender, EventArgs e)
    {
      using (var dlg = new OpenFileDialog())
      {
        dlg.Filter = "Training Index Files(*.IDX)|*.IDX";
        dlg.Multiselect = false;

        if (dlg.ShowDialog() == DialogResult.OK)
        {
          using (new WaitCursor())
          {
            trainingSet = LoadTrainingSet(dlg.FileName);

            UpdateTrainingSetView();

            ClearLocalizationView();
          }
        }
      }
    }

    private void UpdateTrainingSetView()
    {
      panelTrainingSet.Controls.Clear();

      var trainingSetTable = new TableLayoutPanel();
      trainingSetTable.Dock = DockStyle.Fill;
      trainingSetTable.ColumnCount = 1;
      trainingSetTable.RowCount = trainingSet.Length;
      trainingSetTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
      var rowPerc=(float) (100.0/trainingSet.Length);

      for (int i = 0; i < trainingSet.Length; i++)
			{
        trainingSetTable.RowStyles.Add(new RowStyle(SizeType.Percent, rowPerc));

        var classTable = new TableLayoutPanel();
        classTable.Dock = DockStyle.Fill;
        classTable.ColumnCount = trainingSet[i].Length;
        classTable.RowCount = 1;
        classTable.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
        classTable.Tag=LabelColors.Colors[i];
        classTable.Paint += new PaintEventHandler(classTable_Paint);
        var colPerc = (float) (100.0 / trainingSet[i].Length);

        for (int j = 0; j < trainingSet[i].Length; j++)
        {
          classTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, colPerc));

          var viewer = new ImageViewer(trainingSet[i][j]);
          viewer.Dock = DockStyle.Fill;
          
          classTable.Controls.Add(viewer, j, 0);
        }

        trainingSetTable.Controls.Add(classTable, 0, i);
			}

      panelTrainingSet.Controls.Add(trainingSetTable);
    }

    void classTable_Paint(object sender, PaintEventArgs e)
    {
      var control=(Control) sender;
      ControlPaint.DrawBorder(e.Graphics, control.ClientRectangle,(Color) control.Tag, ButtonBorderStyle.Solid);
    }

    private static RgbImage<byte>[][] LoadTrainingSet(string indexFilePath)
    {
      var lines = File.ReadAllLines(indexFilePath);

      var classCount = int.Parse(lines[0]);

      var baseFolder = Path.GetDirectoryName(indexFilePath);
      var ts = new RgbImage<byte>[classCount][];
      for (int i = 0; i < classCount; i++)
      {
        var splittedLine = lines[i+1].Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);

        var elemCount = int.Parse(splittedLine[1]);

        ts[i]=new RgbImage<byte>[elemCount];
        for (int j = 0; j < elemCount; j++)
        {
          ts[i][j] =(RgbImage<byte>) ImageBase.LoadFromFile(Path.Combine(baseFolder,splittedLine[j+2]));
        }
      }

      return ts;
    }

    private void buttonLocalize_Click(object sender, EventArgs e)
    {
      Cursor = Cursors.WaitCursor;

      UpdateControlState(false);

      backgroundWorker.RunWorkerAsync();
    }

    private void UpdateControlState(bool state)
    {
      buttonLoadTestImage.Enabled = state;
      buttonLoadTrainingSet.Enabled = state;
      buttonLocalize.Enabled = state;

      numericUpDownMinSCale.Enabled = state;
      numericUpDownMaxScale.Enabled = state;
      numericUpDownScaleIncrementStep.Enabled = state;
      numericUpDownDistanceThr.Enabled = state;
      numericUpDownStepPercentage.Enabled = state;
    }

    private static Font stringFont=new Font("Arial", 16);
    private void imageViewerTestImage_Paint(object sender, PaintEventArgs e)
    {
      if (imageViewerTestImage.Tag != null)
      {
        imageViewerTestImage.AdjustGraphicsToWorldUnits(e.Graphics);

        var detectedObjects = (ObjectCandidate[])imageViewerTestImage.Tag;

        for (int i = 0; i < detectedObjects.Length; i++)
        {
          e.Graphics.DrawRectangle(LabelColors.Pens[detectedObjects[i].Class],detectedObjects[i].Rectangle.ToRectangle());
          e.Graphics.DrawString(detectedObjects[i].Distance.ToString("0.00",CultureInfo.InvariantCulture),
                                stringFont,
                                LabelColors.Brushes[detectedObjects[i].Class],
                                detectedObjects[i].Rectangle.TopLeft.ToPointF());
        }
      }
    }

    private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      var ts = new Image<byte>[trainingSet.Length][];
      for (int i = 0; i < trainingSet.Length; i++)
      {
        ts[i] = new Image<byte>[trainingSet[i].Length];
        for (int j = 0; j < trainingSet[i].Length; j++)
        {
          ts[i][j] = trainingSet[i][j].ToByteImage();
        }
      }

      localizer.TrainingSet = ts;
      localizer.TestImage = imageViewerTestImage.Image.ToByteImage();
      localizer.MinScale = (double)numericUpDownMinSCale.Value;
      localizer.MaxScale = (double)numericUpDownMaxScale.Value;
      localizer.ScaleIncrementStep = (double)numericUpDownScaleIncrementStep.Value;
      localizer.DistanceThr = (double)numericUpDownDistanceThr.Value;
      localizer.StepPercentage = (double)numericUpDownStepPercentage.Value;
      localizer.Run();

      e.Result = localizer.DetectedObjects;
    }

    private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      imageViewerTestImage.Tag = e.Result;
      imageViewerTestImage.Invalidate();

      UpdateControlState(true);

      Cursor = Cursors.Default;
    }

    private void numericUpDown_ValueChanged(object sender, EventArgs e)
    {
      ClearLocalizationView();
    }

    private void ClearLocalizationView()
    {
      if (imageViewerTestImage.Tag != null)
      {
        imageViewerTestImage.Tag = null;
        imageViewerTestImage.Invalidate();
      }
    }
  }
}
