using System;
using System.Windows.Forms;
using BioLab.Classification.Supervised;
using BioLab.Classification.DensityEstimation;
using BioLab.Common;
using PRLab.EsercitazioniVR;

namespace PRLab
{
  partial class VR_CreateMulticlassifierForm : Form
  {
    public VR_CreateMulticlassifierForm()
    {
      InitializeComponent();
    }

    private void buttonAdd_Click(object sender, EventArgs e)
    {
      using (var dlg = new SelectFromListForm())
      {
        ClassifierBuilder[] clList = { new KnnClassifierBuilder() { K = 3 },
                                       new SvmClassifierBuilder(),
                                       new BayesClassifierBuilder(new ParzenEstimator(0.1,ParzenKernelType.Hypercube)),
                                      new VR_BayesNormalClassifierBuilder()};
        dlg.listBox.Items.AddRange(clList);
        dlg.Text = "Selezionare classificatore";
        if (dlg.ShowDialog() == DialogResult.OK &&
            dlg.listBox.SelectedItem != null)
        {
          var cl = (ClassifierBuilder)dlg.listBox.SelectedItem;
          listBoxClassifiers.SelectedIndex = listBoxClassifiers.Items.Add(cl);
        }
      }
    }

    private void listBoxClassifiers_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (groupBoxParams.Controls.Count > 0)
      {
        groupBoxParams.Controls.RemoveAt(0);
      }
      var uc = ParametersControl.CreateClassifierParametersControl((ClassifierBuilder)listBoxClassifiers.SelectedItem);
      uc.Dock = DockStyle.Fill;
      groupBoxParams.Controls.Add(uc);
      buttonRemove.Enabled = listBoxClassifiers.SelectedItem != null;
    }

    public ClassifierBuilder MCSBuilder = null;

    private void buttonCreate_Click(object sender, EventArgs e)
    {
      var clBuilderArray = new ClassifierBuilder[listBoxClassifiers.Items.Count];
      if (clBuilderArray.Length < 2)
      {
        MessageBox.Show("� necessario selezionare almeno due classificatori", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        return;
      }
      for (int i = 0; i < clBuilderArray.Length; i++)
        clBuilderArray[i] = (ClassifierBuilder)listBoxClassifiers.Items[i];
      try
      {
        if (radioButtonConfidence.Checked)
        {
          MCSBuilder = new VR_McsConfidenceBuilder(clBuilderArray, ClassifierFusionMethod.Sum);
        }
        else
        {
          MCSBuilder = new VR_McsMayorVoteBuilder(clBuilderArray);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }
      DialogResult = DialogResult.OK;
      Close();
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.Cancel;
      Close();
    }

    private void buttonRemove_Click(object sender, EventArgs e)
    {
      listBoxClassifiers.Items.Remove(listBoxClassifiers.SelectedItem);
    }
  }
}