namespace PRLab
{
    partial class MainForm
    {

		#region Fields (53) 

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.ToolTip toolTip;

		#endregion Fields 

		#region Methods (1) 


		// Protected Methods (1) 

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


		#endregion Methods 


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newFeatureVectorSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPasteNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMarkRegions = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.menuZoomIn = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoomOut = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFitToWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.menuZoomCombo = new System.Windows.Forms.ToolStripComboBox();
            this.imageProcessingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.basicOperationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filteringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.digitalTopologyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.binaryMorphologyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayscaleMorphologyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patternRecognitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classifiersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clusteringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dimensionalityReductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuralNetworksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazione01ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneSegmentazioneColoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazione02ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneLocalizzazioneCornerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneDescrittoriSIFTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazione04ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneLocalizzazioneOggettiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazione03ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneHoughToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneActiveShapeModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioniMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneIntroduttivaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneBayesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneSVMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selezioneManualeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selezioneAutomaticaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneMulticlassificatoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioneKMeansToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esercitazioniRetiNeuraliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrangeIconsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.densityEstimationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 727);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1146, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.viewMenu,
            this.imageProcessingToolStripMenuItem,
            this.patternRecognitionToolStripMenuItem,
            this.esercitazioniToolStripMenuItem,
            this.esercitazioniMLToolStripMenuItem,
            this.toolsMenu,
            this.windowsMenu,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.MdiWindowListItem = this.windowsMenu;
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1146, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newImageToolStripMenuItem,
            this.newFeatureVectorSetToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator3,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator4,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.printSetupToolStripMenuItem,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // newImageToolStripMenuItem
            // 
            this.newImageToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newImageToolStripMenuItem.Image")));
            this.newImageToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.newImageToolStripMenuItem.Name = "newImageToolStripMenuItem";
            this.newImageToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newImageToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.newImageToolStripMenuItem.Text = "&New Image...";
            this.newImageToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // newFeatureVectorSetToolStripMenuItem
            // 
            this.newFeatureVectorSetToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newFeatureVectorSetToolStripMenuItem.Image")));
            this.newFeatureVectorSetToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.newFeatureVectorSetToolStripMenuItem.Name = "newFeatureVectorSetToolStripMenuItem";
            this.newFeatureVectorSetToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.newFeatureVectorSetToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.newFeatureVectorSetToolStripMenuItem.Text = "New &Feature Vector Set";
            this.newFeatureVectorSetToolStripMenuItem.Click += new System.EventHandler(this.newFeatureVectorSetToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.openToolStripMenuItem.Text = "&Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenFile);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(232, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.SaveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(232, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Enabled = false;
            this.printToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem.Image")));
            this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.printToolStripMenuItem.Text = "&Print";
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Enabled = false;
            this.printPreviewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripMenuItem.Image")));
            this.printPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.printPreviewToolStripMenuItem.Text = "Print Pre&view";
            // 
            // printSetupToolStripMenuItem
            // 
            this.printSetupToolStripMenuItem.Enabled = false;
            this.printSetupToolStripMenuItem.Name = "printSetupToolStripMenuItem";
            this.printSetupToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.printSetupToolStripMenuItem.Text = "Print Setup";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(232, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCopy,
            this.menuPaste,
            this.menuPasteNew,
            this.menuMarkRegions});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(39, 20);
            this.editMenu.Text = "&Edit";
            // 
            // menuCopy
            // 
            this.menuCopy.Image = ((System.Drawing.Image)(resources.GetObject("menuCopy.Image")));
            this.menuCopy.ImageTransparentColor = System.Drawing.Color.Black;
            this.menuCopy.Name = "menuCopy";
            this.menuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.menuCopy.Size = new System.Drawing.Size(241, 22);
            this.menuCopy.Text = "&Copy";
            this.menuCopy.Click += new System.EventHandler(this.CopyToolStripMenuItem_Click);
            // 
            // menuPaste
            // 
            this.menuPaste.Image = ((System.Drawing.Image)(resources.GetObject("menuPaste.Image")));
            this.menuPaste.ImageTransparentColor = System.Drawing.Color.Black;
            this.menuPaste.Name = "menuPaste";
            this.menuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.V)));
            this.menuPaste.Size = new System.Drawing.Size(241, 22);
            this.menuPaste.Text = "&Paste";
            this.menuPaste.Click += new System.EventHandler(this.PasteToolStripMenuItem_Click);
            // 
            // menuPasteNew
            // 
            this.menuPasteNew.Image = ((System.Drawing.Image)(resources.GetObject("menuPasteNew.Image")));
            this.menuPasteNew.Name = "menuPasteNew";
            this.menuPasteNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.menuPasteNew.Size = new System.Drawing.Size(241, 22);
            this.menuPasteNew.Text = "Paste In A New Window";
            this.menuPasteNew.Click += new System.EventHandler(this.menuPasteNew_Click);
            // 
            // menuMarkRegions
            // 
            this.menuMarkRegions.Name = "menuMarkRegions";
            this.menuMarkRegions.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.menuMarkRegions.Size = new System.Drawing.Size(241, 22);
            this.menuMarkRegions.Text = "&Mark Regions";
            this.menuMarkRegions.Click += new System.EventHandler(this.markRegionsToolStripMenuItem_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBarToolStripMenuItem,
            this.statusBarToolStripMenuItem,
            this.toolStripSeparator9,
            this.menuZoomIn,
            this.menuZoomOut,
            this.menuFitToWindow,
            this.menuZoomCombo});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            // 
            // toolBarToolStripMenuItem
            // 
            this.toolBarToolStripMenuItem.Checked = true;
            this.toolBarToolStripMenuItem.CheckOnClick = true;
            this.toolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolBarToolStripMenuItem.Name = "toolBarToolStripMenuItem";
            this.toolBarToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.toolBarToolStripMenuItem.Text = "&Toolbar";
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckOnClick = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.statusBarToolStripMenuItem.Text = "&Status Bar";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.StatusBarToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(178, 6);
            // 
            // menuZoomIn
            // 
            this.menuZoomIn.Name = "menuZoomIn";
            this.menuZoomIn.Size = new System.Drawing.Size(181, 22);
            this.menuZoomIn.Text = "Zoom In";
            this.menuZoomIn.Click += new System.EventHandler(this.menuZoomIn_Click);
            // 
            // menuZoomOut
            // 
            this.menuZoomOut.Name = "menuZoomOut";
            this.menuZoomOut.Size = new System.Drawing.Size(181, 22);
            this.menuZoomOut.Text = "Zoom Out";
            this.menuZoomOut.Click += new System.EventHandler(this.menuZoomOut_Click);
            // 
            // menuFitToWindow
            // 
            this.menuFitToWindow.Name = "menuFitToWindow";
            this.menuFitToWindow.Size = new System.Drawing.Size(181, 22);
            this.menuFitToWindow.Text = "Fit To Window";
            this.menuFitToWindow.Click += new System.EventHandler(this.menuFitToWindow_Click);
            // 
            // menuZoomCombo
            // 
            this.menuZoomCombo.DropDownWidth = 100;
            this.menuZoomCombo.Items.AddRange(new object[] {
            "400%",
            "300%",
            "200%",
            "100%",
            "50%",
            "33%",
            "25%",
            "10%"});
            this.menuZoomCombo.Name = "menuZoomCombo";
            this.menuZoomCombo.Size = new System.Drawing.Size(121, 23);
            this.menuZoomCombo.SelectedIndexChanged += new System.EventHandler(this.menuZoomCombo_SelectedIndexChanged);
            this.menuZoomCombo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.menuZoomCombo_KeyPress);
            // 
            // imageProcessingToolStripMenuItem
            // 
            this.imageProcessingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.basicOperationsToolStripMenuItem,
            this.filteringToolStripMenuItem,
            this.digitalTopologyToolStripMenuItem,
            this.binaryMorphologyToolStripMenuItem,
            this.grayscaleMorphologyToolStripMenuItem});
            this.imageProcessingToolStripMenuItem.Name = "imageProcessingToolStripMenuItem";
            this.imageProcessingToolStripMenuItem.Size = new System.Drawing.Size(112, 20);
            this.imageProcessingToolStripMenuItem.Text = "&Image Processing";
            // 
            // basicOperationsToolStripMenuItem
            // 
            this.basicOperationsToolStripMenuItem.Name = "basicOperationsToolStripMenuItem";
            this.basicOperationsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.basicOperationsToolStripMenuItem.Text = "Basic operations";
            // 
            // filteringToolStripMenuItem
            // 
            this.filteringToolStripMenuItem.Name = "filteringToolStripMenuItem";
            this.filteringToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.filteringToolStripMenuItem.Text = "Filtering";
            // 
            // digitalTopologyToolStripMenuItem
            // 
            this.digitalTopologyToolStripMenuItem.Name = "digitalTopologyToolStripMenuItem";
            this.digitalTopologyToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.digitalTopologyToolStripMenuItem.Text = "Digital topology";
            // 
            // binaryMorphologyToolStripMenuItem
            // 
            this.binaryMorphologyToolStripMenuItem.Name = "binaryMorphologyToolStripMenuItem";
            this.binaryMorphologyToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.binaryMorphologyToolStripMenuItem.Text = "Binary morphology";
            // 
            // grayscaleMorphologyToolStripMenuItem
            // 
            this.grayscaleMorphologyToolStripMenuItem.Name = "grayscaleMorphologyToolStripMenuItem";
            this.grayscaleMorphologyToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.grayscaleMorphologyToolStripMenuItem.Text = "Grayscale morphology";
            // 
            // patternRecognitionToolStripMenuItem
            // 
            this.patternRecognitionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.detectionToolStripMenuItem,
            this.densityEstimationToolStripMenuItem,
            this.classifiersToolStripMenuItem,
            this.clusteringToolStripMenuItem,
            this.dimensionalityReductionToolStripMenuItem,
            this.neuralNetworksToolStripMenuItem});
            this.patternRecognitionToolStripMenuItem.Name = "patternRecognitionToolStripMenuItem";
            this.patternRecognitionToolStripMenuItem.Size = new System.Drawing.Size(124, 20);
            this.patternRecognitionToolStripMenuItem.Text = "&Pattern Recognition";
            // 
            // detectionToolStripMenuItem
            // 
            this.detectionToolStripMenuItem.Name = "detectionToolStripMenuItem";
            this.detectionToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.detectionToolStripMenuItem.Text = "&Detection";
            // 
            // classifiersToolStripMenuItem
            // 
            this.classifiersToolStripMenuItem.Name = "classifiersToolStripMenuItem";
            this.classifiersToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.classifiersToolStripMenuItem.Text = "Classification";
            this.classifiersToolStripMenuItem.Click += new System.EventHandler(this.classificationToolStripMenuItem_Click);
            // 
            // clusteringToolStripMenuItem
            // 
            this.clusteringToolStripMenuItem.Name = "clusteringToolStripMenuItem";
            this.clusteringToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.clusteringToolStripMenuItem.Text = "Clustering";
            this.clusteringToolStripMenuItem.Click += new System.EventHandler(this.clusteringToolStripMenuItem_Click);
            // 
            // dimensionalityReductionToolStripMenuItem
            // 
            this.dimensionalityReductionToolStripMenuItem.Name = "dimensionalityReductionToolStripMenuItem";
            this.dimensionalityReductionToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.dimensionalityReductionToolStripMenuItem.Text = "Dimensionality reduction";
            this.dimensionalityReductionToolStripMenuItem.Click += new System.EventHandler(this.dimensionalityReductionToolStripMenuItem_Click);
            // 
            // neuralNetworksToolStripMenuItem
            // 
            this.neuralNetworksToolStripMenuItem.Name = "neuralNetworksToolStripMenuItem";
            this.neuralNetworksToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.neuralNetworksToolStripMenuItem.Text = "Neural networks";
            this.neuralNetworksToolStripMenuItem.Click += new System.EventHandler(this.neuralNetworksToolStripMenuItem_Click);
            // 
            // esercitazioniToolStripMenuItem
            // 
            this.esercitazioniToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.esercitazione01ToolStripMenuItem,
            this.esercitazioneSegmentazioneColoreToolStripMenuItem,
            this.esercitazione02ToolStripMenuItem,
            this.esercitazioneLocalizzazioneCornerToolStripMenuItem,
            this.esercitazioneDescrittoriSIFTToolStripMenuItem,
            this.esercitazione04ToolStripMenuItem,
            this.esercitazioneLocalizzazioneOggettiToolStripMenuItem,
            this.esercitazione03ToolStripMenuItem,
            this.esercitazioneHoughToolStripMenuItem,
            this.esercitazioneActiveShapeModelToolStripMenuItem});
            this.esercitazioniToolStripMenuItem.Name = "esercitazioniToolStripMenuItem";
            this.esercitazioniToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.esercitazioniToolStripMenuItem.Text = "E&sercitazioni V&&R";
            // 
            // esercitazione01ToolStripMenuItem
            // 
            this.esercitazione01ToolStripMenuItem.Name = "esercitazione01ToolStripMenuItem";
            this.esercitazione01ToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazione01ToolStripMenuItem.Text = "Esercitazione Bayes";
            this.esercitazione01ToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneBayesToolStripMenuItem_Click);
            // 
            // esercitazioneSegmentazioneColoreToolStripMenuItem
            // 
            this.esercitazioneSegmentazioneColoreToolStripMenuItem.Name = "esercitazioneSegmentazioneColoreToolStripMenuItem";
            this.esercitazioneSegmentazioneColoreToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazioneSegmentazioneColoreToolStripMenuItem.Text = "Esercitazione Segmentazione Colore";
            this.esercitazioneSegmentazioneColoreToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneSegmentazioneColoreToolStripMenuItem_Click);
            // 
            // esercitazione02ToolStripMenuItem
            // 
            this.esercitazione02ToolStripMenuItem.Name = "esercitazione02ToolStripMenuItem";
            this.esercitazione02ToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazione02ToolStripMenuItem.Text = "Esercitazione Multi Classificatori";
            this.esercitazione02ToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneMultiClassificatoriToolStripMenuItem_Click);
            // 
            // esercitazioneLocalizzazioneCornerToolStripMenuItem
            // 
            this.esercitazioneLocalizzazioneCornerToolStripMenuItem.Name = "esercitazioneLocalizzazioneCornerToolStripMenuItem";
            this.esercitazioneLocalizzazioneCornerToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazioneLocalizzazioneCornerToolStripMenuItem.Text = "Esercitazione Localizzazione Corner";
            this.esercitazioneLocalizzazioneCornerToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneLocalizzazioneCornerToolStripMenuItem_Click);
            // 
            // esercitazioneDescrittoriSIFTToolStripMenuItem
            // 
            this.esercitazioneDescrittoriSIFTToolStripMenuItem.Name = "esercitazioneDescrittoriSIFTToolStripMenuItem";
            this.esercitazioneDescrittoriSIFTToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazioneDescrittoriSIFTToolStripMenuItem.Text = "Esercitazione Descrittori SIFT";
            this.esercitazioneDescrittoriSIFTToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneDescrittoriSIFTToolStripMenuItem_Click);
            // 
            // esercitazione04ToolStripMenuItem
            // 
            this.esercitazione04ToolStripMenuItem.Name = "esercitazione04ToolStripMenuItem";
            this.esercitazione04ToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazione04ToolStripMenuItem.Text = "Esercitazione K-Means";
            this.esercitazione04ToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneKMeansToolStripMenuItem_Click);
            // 
            // esercitazioneLocalizzazioneOggettiToolStripMenuItem
            // 
            this.esercitazioneLocalizzazioneOggettiToolStripMenuItem.Name = "esercitazioneLocalizzazioneOggettiToolStripMenuItem";
            this.esercitazioneLocalizzazioneOggettiToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazioneLocalizzazioneOggettiToolStripMenuItem.Text = "Esercitazione Localizzazione Oggetti";
            this.esercitazioneLocalizzazioneOggettiToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneLocalizzazioneOggettiToolStripMenuItem_Click);
            // 
            // esercitazione03ToolStripMenuItem
            // 
            this.esercitazione03ToolStripMenuItem.Name = "esercitazione03ToolStripMenuItem";
            this.esercitazione03ToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazione03ToolStripMenuItem.Text = "Esercitazione Reti Neurali";
            this.esercitazione03ToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneRetiNeuraliToolStripMenuItem_Click);
            // 
            // esercitazioneHoughToolStripMenuItem
            // 
            this.esercitazioneHoughToolStripMenuItem.Name = "esercitazioneHoughToolStripMenuItem";
            this.esercitazioneHoughToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazioneHoughToolStripMenuItem.Text = "Esercitazione Hough";
            this.esercitazioneHoughToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneHoughToolStripMenuItem_Click);
            // 
            // esercitazioneActiveShapeModelToolStripMenuItem
            // 
            this.esercitazioneActiveShapeModelToolStripMenuItem.Name = "esercitazioneActiveShapeModelToolStripMenuItem";
            this.esercitazioneActiveShapeModelToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.esercitazioneActiveShapeModelToolStripMenuItem.Text = "Esercitazione Active Shape Model";
            this.esercitazioneActiveShapeModelToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneActiveShapeModelToolStripMenuItem_Click);
            // 
            // esercitazioniMLToolStripMenuItem
            // 
            this.esercitazioniMLToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.esercitazioneIntroduttivaToolStripMenuItem,
            this.esercitazioneBayesToolStripMenuItem,
            this.esercitazioneSVMToolStripMenuItem,
            this.esercitazioneMulticlassificatoriToolStripMenuItem,
            this.esercitazioneKMeansToolStripMenuItem,
            this.esercitazioniRetiNeuraliToolStripMenuItem});
            this.esercitazioniMLToolStripMenuItem.Name = "esercitazioniMLToolStripMenuItem";
            this.esercitazioniMLToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.esercitazioniMLToolStripMenuItem.Text = "Esercitazioni ML";
            // 
            // esercitazioneIntroduttivaToolStripMenuItem
            // 
            this.esercitazioneIntroduttivaToolStripMenuItem.Name = "esercitazioneIntroduttivaToolStripMenuItem";
            this.esercitazioneIntroduttivaToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.esercitazioneIntroduttivaToolStripMenuItem.Text = "1 - Esercitazione introduttiva";
            this.esercitazioneIntroduttivaToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneIntroduttivaToolStripMenuItem_Click);
            // 
            // esercitazioneBayesToolStripMenuItem
            // 
            this.esercitazioneBayesToolStripMenuItem.Name = "esercitazioneBayesToolStripMenuItem";
            this.esercitazioneBayesToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.esercitazioneBayesToolStripMenuItem.Text = "2 - Esercitazione Bayes";
            this.esercitazioneBayesToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneMLBayesToolStripMenuItem_Click);
            // 
            // esercitazioneSVMToolStripMenuItem
            // 
            this.esercitazioneSVMToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selezioneManualeToolStripMenuItem,
            this.selezioneAutomaticaToolStripMenuItem});
            this.esercitazioneSVMToolStripMenuItem.Name = "esercitazioneSVMToolStripMenuItem";
            this.esercitazioneSVMToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.esercitazioneSVMToolStripMenuItem.Text = "3 - Esercitazione SVM";
            // 
            // selezioneManualeToolStripMenuItem
            // 
            this.selezioneManualeToolStripMenuItem.Name = "selezioneManualeToolStripMenuItem";
            this.selezioneManualeToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.selezioneManualeToolStripMenuItem.Text = "Selezione manuale";
            this.selezioneManualeToolStripMenuItem.Click += new System.EventHandler(this.selezioneManualeToolStripMenuItem_Click);
            // 
            // selezioneAutomaticaToolStripMenuItem
            // 
            this.selezioneAutomaticaToolStripMenuItem.Name = "selezioneAutomaticaToolStripMenuItem";
            this.selezioneAutomaticaToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.selezioneAutomaticaToolStripMenuItem.Text = "Selezione automatica";
            this.selezioneAutomaticaToolStripMenuItem.Click += new System.EventHandler(this.selezioneAutomaticaToolStripMenuItem_Click);
            // 
            // esercitazioneMulticlassificatoriToolStripMenuItem
            // 
            this.esercitazioneMulticlassificatoriToolStripMenuItem.Name = "esercitazioneMulticlassificatoriToolStripMenuItem";
            this.esercitazioneMulticlassificatoriToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.esercitazioneMulticlassificatoriToolStripMenuItem.Text = "4 - Esercitazione multiclassificatori";
            this.esercitazioneMulticlassificatoriToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneMLMulticlassificatoriToolStripMenuItem_Click);
            // 
            // esercitazioneKMeansToolStripMenuItem
            // 
            this.esercitazioneKMeansToolStripMenuItem.Name = "esercitazioneKMeansToolStripMenuItem";
            this.esercitazioneKMeansToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.esercitazioneKMeansToolStripMenuItem.Text = "5 - Esercitazione K-Means";
            this.esercitazioneKMeansToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneMLKMeansToolStripMenuItem_Click);
            // 
            // esercitazioniRetiNeuraliToolStripMenuItem
            // 
            this.esercitazioniRetiNeuraliToolStripMenuItem.Name = "esercitazioniRetiNeuraliToolStripMenuItem";
            this.esercitazioniRetiNeuraliToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.esercitazioniRetiNeuraliToolStripMenuItem.Text = "6 - Esercitazione reti neurali";
            this.esercitazioniRetiNeuraliToolStripMenuItem.Click += new System.EventHandler(this.esercitazioneMLRetiNeuraliToolStripMenuItem_Click);
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(47, 20);
            this.toolsMenu.Text = "&Tools";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Enabled = false;
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.optionsToolStripMenuItem.Text = "&Options...";
            // 
            // windowsMenu
            // 
            this.windowsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newWindowToolStripMenuItem,
            this.cascadeToolStripMenuItem,
            this.tileVerticalToolStripMenuItem,
            this.tileHorizontalToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.arrangeIconsToolStripMenuItem});
            this.windowsMenu.Name = "windowsMenu";
            this.windowsMenu.Size = new System.Drawing.Size(68, 20);
            this.windowsMenu.Text = "&Windows";
            // 
            // newWindowToolStripMenuItem
            // 
            this.newWindowToolStripMenuItem.Name = "newWindowToolStripMenuItem";
            this.newWindowToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.newWindowToolStripMenuItem.Text = "&New Window";
            // 
            // cascadeToolStripMenuItem
            // 
            this.cascadeToolStripMenuItem.Name = "cascadeToolStripMenuItem";
            this.cascadeToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.cascadeToolStripMenuItem.Text = "&Cascade";
            this.cascadeToolStripMenuItem.Click += new System.EventHandler(this.CascadeToolStripMenuItem_Click);
            // 
            // tileVerticalToolStripMenuItem
            // 
            this.tileVerticalToolStripMenuItem.Name = "tileVerticalToolStripMenuItem";
            this.tileVerticalToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.tileVerticalToolStripMenuItem.Text = "Tile &Vertical";
            this.tileVerticalToolStripMenuItem.Click += new System.EventHandler(this.TileVerticalToolStripMenuItem_Click);
            // 
            // tileHorizontalToolStripMenuItem
            // 
            this.tileHorizontalToolStripMenuItem.Name = "tileHorizontalToolStripMenuItem";
            this.tileHorizontalToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.tileHorizontalToolStripMenuItem.Text = "Tile &Horizontal";
            this.tileHorizontalToolStripMenuItem.Click += new System.EventHandler(this.TileHorizontalToolStripMenuItem_Click);
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.closeAllToolStripMenuItem.Text = "C&lose All";
            this.closeAllToolStripMenuItem.Click += new System.EventHandler(this.CloseAllToolStripMenuItem_Click);
            // 
            // arrangeIconsToolStripMenuItem
            // 
            this.arrangeIconsToolStripMenuItem.Name = "arrangeIconsToolStripMenuItem";
            this.arrangeIconsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.arrangeIconsToolStripMenuItem.Text = "&Arrange Icons";
            this.arrangeIconsToolStripMenuItem.Click += new System.EventHandler(this.ArrangeIconsToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Enabled = false;
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.contentsToolStripMenuItem.Text = "&Contents...";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.AutoScroll = true;
            this.ContentPanel.Size = new System.Drawing.Size(787, 439);
            // 
            // densityEstimationToolStripMenuItem
            // 
            this.densityEstimationToolStripMenuItem.Name = "densityEstimationToolStripMenuItem";
            this.densityEstimationToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.densityEstimationToolStripMenuItem.Text = "Density estimation";
            this.densityEstimationToolStripMenuItem.Click += new System.EventHandler(this.densityEstimationToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 749);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "PR Lab";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MdiChildActivate += new System.EventHandler(this.MainForm_MdiChildActivate);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem newImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem menuCopy;
        private System.Windows.Forms.ToolStripMenuItem menuPaste;
        private System.Windows.Forms.ToolStripMenuItem menuPasteNew;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem menuZoomIn;
        private System.Windows.Forms.ToolStripMenuItem menuZoomOut;
        private System.Windows.Forms.ToolStripMenuItem menuFitToWindow;
        private System.Windows.Forms.ToolStripComboBox menuZoomCombo;
        private System.Windows.Forms.ToolStripMenuItem imageProcessingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem basicOperationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filteringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem digitalTopologyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem binaryMorphologyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayscaleMorphologyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patternRecognitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsMenu;
        private System.Windows.Forms.ToolStripMenuItem newWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cascadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrangeIconsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newFeatureVectorSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazione01ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazione02ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazione03ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazione04ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneLocalizzazioneCornerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneSegmentazioneColoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuMarkRegions;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneDescrittoriSIFTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneHoughToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneActiveShapeModelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneLocalizzazioneOggettiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioniMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneIntroduttivaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneBayesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneKMeansToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneSVMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioneMulticlassificatoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selezioneManualeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selezioneAutomaticaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esercitazioniRetiNeuraliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classifiersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clusteringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dimensionalityReductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuralNetworksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem densityEstimationToolStripMenuItem;

    }
}



