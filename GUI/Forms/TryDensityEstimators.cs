using System;
using System.Windows.Forms;
using BioLab.Common;
using BioLab.Classification.Supervised;
using BioLab.Classification.DensityEstimation;

namespace PRLab
{
    partial class TryDensityEstimators : Form
    {
        #region Membri privati
        
        private DensityEstimator theDensityEstimator  // il density est. corrente
        {
            get { return (DensityEstimator)comboBoxDensityEst.SelectedItem; }
        }
        FeatureVectorSet[] availableSets;
        FeatureVectorSet dataSet;
        FeatureVectorSetWithClassSurfaces2DViewer preview;
        ParametersControl userControlParameters;
        #endregion


        public TryDensityEstimators(DensityEstimator[] densityEstimators, FeatureVectorSet[] sets)
        {
            InitializeComponent();
            availableSets = sets;
            // riempie i combo
            if (availableSets.Length == 0)
                throw new Exception("Nessun set di vettori definito");
            foreach (var vs in availableSets)
            {
                string desc = vs.Description == null ? "Nessuna descrizione" : vs.Description;
                comboBoxTrainingSet.Items.Add(desc);
            }
            comboBoxTrainingSet.SelectedIndex = 0;
            
            if (densityEstimators != null &&
                densityEstimators.Length > 0)
            {
                comboBoxDensityEst.Items.AddRange(densityEstimators);
                comboBoxDensityEst.SelectedIndex = 0;
            }
        }

        private void UseClassifier_Load(object sender, EventArgs e)
        {
            // Imposta ora il Panel2MinSize: non pu� essere fatto a design time per un bug del VS2005...
            splitPreview.Panel2MinSize = 250;
        }

        void userControlParameters_ParametersChanged(object sender, EventArgs e)
        {
            // annulla il training
            ClearTrainingState();
            UpdatePreview(dataSet, false);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdatePreview(FeatureVectorSet vs, bool showSurface)
        {
            if (dataSet.Dim != 2)   // solo in 2D si puo' disegnare la superficie
                showSurface = false;
            if (preview == null)
            {
                preview = new FeatureVectorSetWithClassSurfaces2DViewer(vs, null, showSurface ? theDensityEstimator : null);
                preview.Dock = DockStyle.Fill;
                preview.EnableEditModeInDefaultContextMenu = false;
                panelPreview.Controls.Add(preview);
            }
            else
            {
                preview.UpdateViewEx(vs, null, showSurface ? theDensityEstimator : null);
            }
        }

        private void comboBoxTrainingSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearTrainingState();
            dataSet = availableSets[comboBoxTrainingSet.SelectedIndex];
            UpdatePreview(dataSet, false);
        }

        private void ClearTrainingState()
        {
            labelMessage.Text = "";
        }

        private void buttonTraining_Click(object sender, EventArgs e)
        {
            // nel caso sia stato usato Invio (� l'AcceptButton), sposta il fuoco per fare eventuali aggiornamenti
            buttonTraining.Focus();
            ClearTrainingState();
            Cursor = Cursors.WaitCursor;
            try
            {
                theDensityEstimator.TrainingSet = dataSet;
                UpdatePreview(dataSet, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Si � verificato un errore imprevisto durante il training.\n{0}", ex.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void UseClassifier_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (preview != null)
            //    preview.Cleanup();
        }

        private void CreateParametersControl()
        {
            if (userControlParameters != null)
            {
                groupBoxSettings.Controls.Remove(userControlParameters);
            }
            userControlParameters = ParametersControl.CreateDensityEstParametersControl((DensityEstimator)comboBoxDensityEst.SelectedItem);
            userControlParameters.Dock = DockStyle.Fill;
            userControlParameters.ParametersChanged += new EventHandler(userControlParameters_ParametersChanged);
            groupBoxSettings.Controls.Add(userControlParameters);
        }

        private void comboBoxProbEst_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateParametersControl();
        }
    }
}