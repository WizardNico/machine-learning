namespace PRLab
{
    partial class TryClusterizers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TryClusterizers));
            this.splitPreview = new System.Windows.Forms.SplitContainer();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.panelChooseSets = new System.Windows.Forms.Panel();
            this.numericUpDownNumClsuters = new System.Windows.Forms.NumericUpDown();
            this.labelAlg = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxAlg = new System.Windows.Forms.ComboBox();
            this.comboBoxDataSets = new System.Windows.Forms.ComboBox();
            this.labelScore = new System.Windows.Forms.Label();
            this.trackBarTimerSpeed = new System.Windows.Forms.TrackBar();
            this.checkBoxAutoIteration = new System.Windows.Forms.CheckBox();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.panelPreview = new System.Windows.Forms.Panel();
            this.timerIteration = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitPreview)).BeginInit();
            this.splitPreview.Panel1.SuspendLayout();
            this.splitPreview.Panel2.SuspendLayout();
            this.splitPreview.SuspendLayout();
            this.panelChooseSets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumClsuters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTimerSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // splitPreview
            // 
            this.splitPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitPreview.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitPreview.Location = new System.Drawing.Point(0, 0);
            this.splitPreview.Name = "splitPreview";
            // 
            // splitPreview.Panel1
            // 
            this.splitPreview.Panel1.Controls.Add(this.groupBoxSettings);
            this.splitPreview.Panel1.Controls.Add(this.panelChooseSets);
            this.splitPreview.Panel1MinSize = 200;
            // 
            // splitPreview.Panel2
            // 
            this.splitPreview.Panel2.Controls.Add(this.labelScore);
            this.splitPreview.Panel2.Controls.Add(this.trackBarTimerSpeed);
            this.splitPreview.Panel2.Controls.Add(this.checkBoxAutoIteration);
            this.splitPreview.Panel2.Controls.Add(this.buttonStop);
            this.splitPreview.Panel2.Controls.Add(this.buttonStart);
            this.splitPreview.Panel2.Controls.Add(this.buttonClose);
            this.splitPreview.Panel2.Controls.Add(this.panelPreview);
            this.splitPreview.Size = new System.Drawing.Size(949, 525);
            this.splitPreview.SplitterDistance = 210;
            this.splitPreview.TabIndex = 0;
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSettings.Location = new System.Drawing.Point(4, 115);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(199, 404);
            this.groupBoxSettings.TabIndex = 1;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Parametri";
            // 
            // panelChooseSets
            // 
            this.panelChooseSets.Controls.Add(this.numericUpDownNumClsuters);
            this.panelChooseSets.Controls.Add(this.labelAlg);
            this.panelChooseSets.Controls.Add(this.label2);
            this.panelChooseSets.Controls.Add(this.label1);
            this.panelChooseSets.Controls.Add(this.comboBoxAlg);
            this.panelChooseSets.Controls.Add(this.comboBoxDataSets);
            this.panelChooseSets.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelChooseSets.Location = new System.Drawing.Point(0, 0);
            this.panelChooseSets.Name = "panelChooseSets";
            this.panelChooseSets.Size = new System.Drawing.Size(210, 109);
            this.panelChooseSets.TabIndex = 0;
            // 
            // numericUpDownNumClsuters
            // 
            this.numericUpDownNumClsuters.Location = new System.Drawing.Point(99, 67);
            this.numericUpDownNumClsuters.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownNumClsuters.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownNumClsuters.Name = "numericUpDownNumClsuters";
            this.numericUpDownNumClsuters.Size = new System.Drawing.Size(100, 20);
            this.numericUpDownNumClsuters.TabIndex = 5;
            this.numericUpDownNumClsuters.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // labelAlg
            // 
            this.labelAlg.AutoSize = true;
            this.labelAlg.Location = new System.Drawing.Point(3, 9);
            this.labelAlg.Name = "labelAlg";
            this.labelAlg.Size = new System.Drawing.Size(50, 13);
            this.labelAlg.TabIndex = 0;
            this.labelAlg.Text = "Algoritmo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Numero di cluster";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Dataset";
            // 
            // comboBoxAlg
            // 
            this.comboBoxAlg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAlg.FormattingEnabled = true;
            this.comboBoxAlg.Location = new System.Drawing.Point(59, 6);
            this.comboBoxAlg.Name = "comboBoxAlg";
            this.comboBoxAlg.Size = new System.Drawing.Size(140, 21);
            this.comboBoxAlg.TabIndex = 1;
            this.comboBoxAlg.SelectedIndexChanged += new System.EventHandler(this.comboBoxAlg_SelectedIndexChanged);
            // 
            // comboBoxDataSets
            // 
            this.comboBoxDataSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDataSets.FormattingEnabled = true;
            this.comboBoxDataSets.Location = new System.Drawing.Point(59, 33);
            this.comboBoxDataSets.Name = "comboBoxDataSets";
            this.comboBoxDataSets.Size = new System.Drawing.Size(140, 21);
            this.comboBoxDataSets.TabIndex = 3;
            this.comboBoxDataSets.SelectedIndexChanged += new System.EventHandler(this.comboBoxTrainingSet_SelectedIndexChanged);
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Location = new System.Drawing.Point(252, 501);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(10, 13);
            this.labelScore.TabIndex = 7;
            this.labelScore.Text = "-";
            // 
            // trackBarTimerSpeed
            // 
            this.trackBarTimerSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBarTimerSpeed.AutoSize = false;
            this.trackBarTimerSpeed.LargeChange = 200;
            this.trackBarTimerSpeed.Location = new System.Drawing.Point(3, 483);
            this.trackBarTimerSpeed.Maximum = 999;
            this.trackBarTimerSpeed.Name = "trackBarTimerSpeed";
            this.trackBarTimerSpeed.Size = new System.Drawing.Size(83, 39);
            this.trackBarTimerSpeed.SmallChange = 100;
            this.trackBarTimerSpeed.TabIndex = 5;
            this.trackBarTimerSpeed.TickFrequency = 100;
            this.trackBarTimerSpeed.Value = 500;
            this.trackBarTimerSpeed.ValueChanged += new System.EventHandler(this.trackBarTimerSpeed_ValueChanged);
            // 
            // checkBoxAutoIteration
            // 
            this.checkBoxAutoIteration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBoxAutoIteration.AutoSize = true;
            this.checkBoxAutoIteration.Enabled = false;
            this.checkBoxAutoIteration.Location = new System.Drawing.Point(89, 500);
            this.checkBoxAutoIteration.Name = "checkBoxAutoIteration";
            this.checkBoxAutoIteration.Size = new System.Drawing.Size(146, 17);
            this.checkBoxAutoIteration.TabIndex = 2;
            this.checkBoxAutoIteration.Text = "Avanzamento automatico";
            this.checkBoxAutoIteration.UseVisualStyleBackColor = true;
            this.checkBoxAutoIteration.CheckedChanged += new System.EventHandler(this.checkBoxAutoIteration_CheckedChanged);
            // 
            // buttonStop
            // 
            this.buttonStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonStop.Enabled = false;
            this.buttonStop.Location = new System.Drawing.Point(570, 496);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 3;
            this.buttonStop.Text = "Interrompi";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonStart.Location = new System.Drawing.Point(8, 496);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "Avvia";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(651, 496);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 4;
            this.buttonClose.Text = "Chiudi";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // panelPreview
            // 
            this.panelPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelPreview.Location = new System.Drawing.Point(0, 0);
            this.panelPreview.Name = "panelPreview";
            this.panelPreview.Size = new System.Drawing.Size(735, 477);
            this.panelPreview.TabIndex = 0;
            // 
            // timerIteration
            // 
            this.timerIteration.Interval = 1000;
            this.timerIteration.Tick += new System.EventHandler(this.timerIteration_Tick);
            // 
            // TryClusterizers
            // 
            this.AcceptButton = this.buttonStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(949, 525);
            this.Controls.Add(this.splitPreview);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(656, 452);
            this.Name = "TryClusterizers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Prova algoritmi di clustering";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UseClusterizer_FormClosed);
            this.Load += new System.EventHandler(this.TestClusterizers_Load);
            this.splitPreview.Panel1.ResumeLayout(false);
            this.splitPreview.Panel2.ResumeLayout(false);
            this.splitPreview.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPreview)).EndInit();
            this.splitPreview.ResumeLayout(false);
            this.panelChooseSets.ResumeLayout(false);
            this.panelChooseSets.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumClsuters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTimerSpeed)).EndInit();
            this.ResumeLayout(false);

}

        #endregion

        private System.Windows.Forms.SplitContainer splitPreview;
        private System.Windows.Forms.Panel panelChooseSets;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxDataSets;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel panelPreview;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelAlg;
        private System.Windows.Forms.ComboBox comboBoxAlg;
        private System.Windows.Forms.NumericUpDown numericUpDownNumClsuters;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxAutoIteration;
        private System.Windows.Forms.Timer timerIteration;
        private System.Windows.Forms.TrackBar trackBarTimerSpeed;
        private System.Windows.Forms.Label labelScore;
    }
}