using System;
using System.Windows.Forms;
using BioLab.Common;
using BioLab.Classification.Supervised;
using BioLab.Classification.DensityEstimation;
using PRLab.GUI.Forms;
using System.ComponentModel;
using System.Threading;
using System.Collections.Generic;
using BioLab.Math.Geometry;
using System.Windows.Forms.DataVisualization.Charting;

namespace PRLab
{
    partial class TryNeuralNetworks : Form
    {
        #region Membri privati

        private Thread trainingThread=null;
        private int trainingIterationCount = 0;

        private ClassifierBuilder TheClassifierBuilder  // il classifier builder corrente
        {
            get { return (ClassifierBuilder)comboBoxClassifier.SelectedItem; }
        }
        private Classifier TheClassifier  // il classifier corrente
        {
            get { return (Classifier)theClassifier; }
        }

        FeatureVectorSet[] availableSets;
        FeatureVectorSet trainingSet, testSet;
        FeatureVectorSetWithClassSurfaces2DViewer preview;
        ParametersControl userControlParameters;
        private Classifier theClassifier;
        #endregion

        public TryNeuralNetworks(ClassifierBuilder[] classifierBuilders, DensityEstimator[] densityEstimators, FeatureVectorSet[] sets, bool createMCs)
        {
            InitializeComponent();
            availableSets = sets;
            // riempie i combo
            if (availableSets.Length == 0)
                throw new Exception("Nessun set di vettori definito");

            foreach (var vs in availableSets)
            {
                string desc = vs.Description == null ? "Nessuna descrizione" : vs.Description;
                comboBoxTrainingSet.Items.Add(desc);
                comboBoxTestSet.Items.Add(desc);
            }
            comboBoxTrainingSet.SelectedIndex = 0;
            comboBoxTestSet.SelectedIndex = comboBoxTestSet.Items.Count > 1 ? 1 : 0;
            comboBoxClassifier.Items.AddRange(classifierBuilders);

            if (createMCs)
            {
                comboBoxClassifier.Items.Add("Crea MCS...");
            }

            comboBoxClassifier.SelectedIndex = 0;

            buttonStop.Enabled = false;
        }

        private void UseClassifier_Load(object sender, EventArgs e)
        {
            // Imposta ora il Panel2MinSize: non pu� essere fatto a design time per un bug del VS2005...
            splitPreview.Panel2MinSize = 250;
        }

        void userControlParameters_ParametersChanged(object sender, EventArgs e)
        {
            // annulla il training
            ClearTrainingState();
            UpdatePreview(trainingSet, false);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdatePreview(FeatureVectorSet vs, bool showSurface)
        {
            if (trainingSet.Dim != 2)   // solo in 2D si puo' disegnare la superficie
            {
                showSurface = false;
            }

            if (preview == null)
            {
                preview = new FeatureVectorSetWithClassSurfaces2DViewer();
                preview.Dock = DockStyle.Fill;
                preview.EnableEditModeInDefaultContextMenu = false;
                preview.UpdateViewFinished += new EventHandler<EventArgs>(preview_UpdateViewFinished);
                panelPreview.Controls.Add(preview);
            }

            preview.UpdateViewEx(vs, showSurface ? theClassifier : null, null);
        }

        void preview_UpdateViewFinished(object sender, EventArgs e)
        {
            pool.Set();
        }

        private void comboBoxTrainingSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearTrainingState();
            trainingSet = availableSets[comboBoxTrainingSet.SelectedIndex];
            UpdatePreview(trainingSet, false);
        }

        private void ClearTrainingState()
        {
            buttonTest.Enabled = false;
            labelMessage.Text = "";

            chart.Series.Clear();
        }

        private void comboBoxTestSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ClearTrainingState();
            testSet = availableSets[comboBoxTestSet.SelectedIndex];
            UpdatePreview(testSet, false);
        }

        private void buttonTraining_Click(object sender, EventArgs e)
        {
            // nel caso sia stato usato Invio (� l'AcceptButton), sposta il fuoco per fare eventuali aggiornamenti
            buttonTraining.Focus();
            ClearTrainingState();

            buttonTraining.Enabled = false;
            buttonStop.Enabled = true;
            buttonClose.Enabled = false;
            numericUpDownRefreshCount.Enabled = false;

            Cursor = Cursors.WaitCursor;

            var builder = TheClassifierBuilder;

            builder.IntermediateResult -= builder_IntermediateResult;
            builder.IntermediateResult += new EventHandler<AlgorithmIntermediateResultEventArgs>(builder_IntermediateResult);

            for (int i = 0; i < graphValues.Length; i++)
            {
                if (graphValues[i] == null)
                {
                    graphValues[i] = new List<Point2D>();
                }
                else
                {
                    graphValues[i].Clear();
                }
            }

            chart.Series.Clear();

            //chart.ChartAreas[0].AxisX.Minimum = 0;
            //chart.ChartAreas[0].AxisX.Maximum = ((NeuralNetworkClassifierBuilder)builder).MaxEpochs;

            //chart.ChartAreas[0].AxisY.Minimum = 0;
            //chart.ChartAreas[0].AxisY.Maximum = 1;

            trainingIterationCount = 0;

            trainingThread = new Thread(ExecuteTraining);
            trainingThread.Start(new object[]
                                    {
                                        builder,
                                        trainingSet,
                                    });
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            //ClearTrainingState();
            Cursor = Cursors.WaitCursor;
            try
            {
                if (testSet.Count > 0)
                {
                    var et = new ElapsedTime();
                    et.Start();
                    var nErrors = Classify(testSet);
                    et.Stop();
                    labelMessage.Text = string.Format("Errore di classificazione: {0:0.0%} [Tempo: {1}]", (double)nErrors / testSet.Count, et);
                }
                UpdatePreview(testSet, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Si � verificato un errore imprevisto durante il test.\n{0}", ex.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private int Classify(FeatureVectorSet dataSet)
        {
            int nErrors = 0;
            foreach (var v in dataSet)
            {
                int c = TheClassifier.Classify(v);
                if (c != v.Class)
                {
                    nErrors++;
                }
            }
            return nErrors;
        }

        private void UseClassifier_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (preview != null)
            //    preview.Cleanup();
        }

        private void CreateParametersControl()
        {
            if (userControlParameters != null)
            {
                groupBoxSettings.Controls.Remove(userControlParameters);
            }

            userControlParameters = ParametersControl.CreateClassifierParametersControl((ClassifierBuilder)comboBoxClassifier.SelectedItem);

            userControlParameters.Dock = DockStyle.Fill;
            userControlParameters.ParametersChanged += new EventHandler(userControlParameters_ParametersChanged);
            groupBoxSettings.Controls.Add(userControlParameters);
        }


        private void comboBoxClassifier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxClassifier.SelectedItem == null)
                return;
            if (comboBoxClassifier.SelectedItem is ClassifierBuilder)
            {
                CreateParametersControl();
            }
        }

        private void ExecuteTraining(object data)//(ClassifierBuilder builder, FeatureVectorSet trainingSet)
        {
            try
            {
                var args = (object[])data;
                var builder = (ClassifierBuilder)args[0];
                var trainingSet = (FeatureVectorSet)args[1];

                theClassifier = builder.Train(trainingSet);
            }
            catch (Exception ex)
            {
                if (!(ex is ThreadAbortException))
                {
                    MessageBox.Show(string.Format("Si � verificato un errore imprevisto durante il training.\n{0}", ex.Message), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                this.Invoke((MethodInvoker)TrainingCompleted);
            }
        }

        private List<Point2D>[] graphValues = new List<Point2D>[2];
        private AutoResetEvent pool = new AutoResetEvent(true);
        void builder_IntermediateResult(object sender, AlgorithmIntermediateResultEventArgs e)
        {
            if (e.Iteration % numericUpDownRefreshCount.Value == 0)
            {
                theClassifier = (Classifier)e.IntermediateResult;
                trainingIterationCount = e.Iteration;

                this.Invoke((MethodInvoker) UpdateIntermediateResults);

                pool.WaitOne();
                Thread.Sleep(100);
            }
        }

        private void UpdateIntermediateResults()
        {
            graphValues[0].Add(new Point2D(trainingIterationCount, (double)100.0*Classify(trainingSet) / trainingSet.Count));
            graphValues[1].Add(new Point2D(trainingIterationCount, (double)100.0 * Classify(testSet) / testSet.Count));
            
            UpdatePreview(trainingSet, true);
            labelMessage.Text = string.Format("N� iterazioni: {0}", trainingIterationCount);

            UpdateGraph(chart,graphValues,new string[] {"Training set","Test set"});
        }

        public static void UpdateGraph(Chart chart,List<Point2D>[] data,string[] dataNames)
        {
            chart.Series.Clear();

            for (int i = 0; i < data.Length; i++)
            {
                var line = new Series();
                line.Name = dataNames[i];
                line.ChartType = SeriesChartType.Line;
                for (int j = 0; j < data[i].Count; j++)
                {
                    line.Points.Add(new DataPoint(data[i][j].X, data[i][j].Y));
                }

                chart.Series.Add(line);
            }
        }

        //private void backgroundWorkerNeuralNetworkTraining_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    //var worker = (BackgroundWorker) sender;

        //    var args = (object[])e.Argument;
        //    var builder = (ClassifierBuilder)args[0];
        //    var trainingSet = (FeatureVectorSet)args[1];

        //    builder.IntermediateResult += new EventHandler<AlgorithmIntermediateResultEventArgs>(builder_IntermediateResult);

        //    var nn = builder.Train(trainingSet);

        //    e.Result = nn;
        //}

        //private void backgroundWorkerNeuralNetworkTraining_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    theClassifier = (Classifier)e.Result;

        //    TrainingCompleted();
        //}

        //private void backgroundWorkerNeuralNetworkTraining_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //{
        //    theClassifier = (Classifier)e.UserState;

        //    UpdatePreview(trainingSet, true);
        //}

        private void TrainingCompleted()
        {
            Cursor = Cursors.Default;

            buttonTraining.Enabled = true;
            buttonTest.Enabled = true;
            buttonClose.Enabled = true;
            buttonStop.Enabled = false;
            numericUpDownRefreshCount.Enabled = true;

            if (theClassifier != null)
            {
                UpdatePreview(trainingSet, true);
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (trainingThread != null)
            {
                trainingThread.Abort();
                trainingThread = null;

                TrainingCompleted();
            }
        }
    }
}