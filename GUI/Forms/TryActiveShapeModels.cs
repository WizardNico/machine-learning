using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PRLab.EsercitazioniVR;
using BioLab.ImageProcessing;
using System.IO;
using BioLab.GUI.Common;
using BioLab.Math.LinearAlgebra;

namespace PRLab
{
  public partial class TryActiveShapeModels : Form
  {
    #region Campi privati
    ImageWithMarkedRegions[] arImr = null;
    ImageWithMarkedRegions currentShape = null;
    Image<byte> emptyImg = null;
    TrackBar[] trackBarModes;
    VR_ActiveShapeModel Asm;
    #endregion


    public TryActiveShapeModels()
    {
      InitializeComponent();

      imageWithMarkedRegionsViewerTraining.Modifying = false;
      imageWithMarkedRegionsViewerCurrentShape.Modifying = false;
    }

    private void buttonLoadTrainingData_Click(object sender, EventArgs e)
    {
      try
      {
        using (var dlg = new OpenFileDialog())
        {
          dlg.Multiselect = true;
          dlg.Filter = "Image With Regions Text Files (*.mkrtxt)|*.mkrtxt|All Files (*.*)|*.*";

          if (dlg.ShowDialog() == DialogResult.OK)
          {
            using (var wc = new WaitCursor())
            {
              arImr = new ImageWithMarkedRegions[dlg.FileNames.Length];
              for (int i = 0; i < dlg.FileNames.Length; i++)
              {
                var mkr = new ImageWithMarkedRegions(ImageBase.LoadFromFile(Path.ChangeExtension(dlg.FileNames[i], ".png")));
                mkr.LoadRegionsFromTextFile(dlg.FileNames[i]);

                arImr[i] = mkr;
              }

              if (arImr.Length > 0)
              {
                imageWithMarkedRegionsViewerTraining.SetImageWithMarkedRegions(arImr[0]);
                emptyImg = new Image<byte>(arImr[0].Width, arImr[1].Height);
              }

              numericUpDownPreviewTrainImg.Minimum = 0;
              numericUpDownPreviewTrainImg.Maximum = arImr.Length - 1;
              numericUpDownPreviewTrainImg.Value = 0;

              ResetControl();
            }
          }
        }
      }
      catch (InvalidCastException)
      {
        MessageBox.Show("� necessario selezionare immagini con regioni", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
    }

    private void numericUpDownPreviewTrainImg_ValueChanged(object sender, EventArgs e)
    {
      int index = (int)numericUpDownPreviewTrainImg.Value;
      if (arImr[index] != imageWithMarkedRegionsViewerTraining.Image)
      {
        imageWithMarkedRegionsViewerTraining.SetImageWithMarkedRegions(arImr[index]);
      }
    }

    private void TryActiveShapeModels_Load(object sender, EventArgs e)
    {
      UpdateTrackBarModes();
    }

    private void UpdateTrackBarModes()
    {
      tableLayoutPanel.Controls.Clear();
                 
      trackBarModes = new TrackBar[(int) numericUpDownModeCount.Value];
      for (int i = 0; i < trackBarModes.Length; i++)
      {
        trackBarModes[i] = new TrackBar();
        trackBarModes[i].Minimum = -100;
        trackBarModes[i].Maximum = +100;
        trackBarModes[i].Value = 0;
        trackBarModes[i].TickFrequency = 10;
        trackBarModes[i].Dock = DockStyle.Fill;
        trackBarModes[i].ValueChanged += new EventHandler(TryActiveShapeModels_ValueChanged);
        tableLayoutPanel.Controls.Add(trackBarModes[i], 0, i);
      }
    }

    void TryActiveShapeModels_ValueChanged(object sender, EventArgs e)
    {
      if (currentShape != null)
        UpdateCurrentShape();
    }

    private void UpdateCurrentShape()
    {
      if (currentShape != null)
      {
        var modeValues = new Vector(trackBarModes.Length);
        for (int i = 0; i < trackBarModes.Length; i++)
        {
          modeValues[i] = ((double)trackBarModes[i].Value / 100) * Asm.GetModeRange(i);
        }
        Asm.AdjustModel(currentShape, modeValues);
        imageWithMarkedRegionsViewerCurrentShape.SetImageWithMarkedRegions(currentShape);
      }
      else
      {
        imageWithMarkedRegionsViewerCurrentShape.SetImageWithMarkedRegions(null);
      }
      imageWithMarkedRegionsViewerCurrentShape.Invalidate();
    }

    private void buttonCalculate_Click(object sender, EventArgs e)
    {
      try
      {
        using (var wc = new WaitCursor())
        {
          Asm = new VR_ActiveShapeModel();
          Asm.Calculate(arImr, checkBoxCenter.Checked, trackBarModes.Length);
        }
      }
      catch
      {
        MessageBox.Show("Impossibile calcolare il modello");
        return;
      }
      currentShape = new ImageWithMarkedRegions(emptyImg);
      UpdateCurrentShape();
    }

    private void buttonResetModes_Click(object sender, EventArgs e)
    {
      ResetModes();
    }

    private void ResetModes()
    {
      for (int i = 0; i < trackBarModes.Length; i++)
      {
        trackBarModes[i].Value = 0;
      }
    }

    private void numericUpDownModeCount_ValueChanged(object sender, EventArgs e)
    {
      UpdateTrackBarModes();

      ResetControl();
    }

    private void ResetControl()
    {
      Asm = null;
      currentShape = null;
      ResetModes();
      UpdateCurrentShape();
    }
  }
}