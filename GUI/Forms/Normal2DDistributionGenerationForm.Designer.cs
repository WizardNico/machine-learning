﻿namespace PRLab.GUI.Forms
{
    partial class Normal2DDistributionGenerationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Normal2DDistributionGenerationForm));
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownDistributionCount = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMaxPatternCountPerDistribution = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownMinDistanceBetweenMeanVectors = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDownMaxDistanceOnFirstAxis = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMaxDistanceOnSecondAxis = new System.Windows.Forms.NumericUpDown();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.radioButtonPredefinedSeed = new System.Windows.Forms.RadioButton();
            this.radioButtonRandomSeed = new System.Windows.Forms.RadioButton();
            this.numericUpDownPredefinedSeed = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDownMinPatternCountPerDistribution = new System.Windows.Forms.NumericUpDown();
            this.buttonSetDefaultParams = new System.Windows.Forms.Button();
            this.numericUpDownMinDistanceOnFirstAxis = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMinDistanceOnSecondAxis = new System.Windows.Forms.NumericUpDown();
            this.checkBoxAddClassId = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDistributionCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxPatternCountPerDistribution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinDistanceBetweenMeanVectors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxDistanceOnFirstAxis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxDistanceOnSecondAxis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPredefinedSeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinPatternCountPerDistribution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinDistanceOnFirstAxis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinDistanceOnSecondAxis)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Numero distribuzioni:";
            // 
            // numericUpDownDistributionCount
            // 
            this.numericUpDownDistributionCount.Location = new System.Drawing.Point(307, 7);
            this.numericUpDownDistributionCount.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownDistributionCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownDistributionCount.Name = "numericUpDownDistributionCount";
            this.numericUpDownDistributionCount.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownDistributionCount.TabIndex = 1;
            this.numericUpDownDistributionCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownDistributionCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDownMaxPatternCountPerDistribution
            // 
            this.numericUpDownMaxPatternCountPerDistribution.Location = new System.Drawing.Point(364, 33);
            this.numericUpDownMaxPatternCountPerDistribution.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numericUpDownMaxPatternCountPerDistribution.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMaxPatternCountPerDistribution.Name = "numericUpDownMaxPatternCountPerDistribution";
            this.numericUpDownMaxPatternCountPerDistribution.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownMaxPatternCountPerDistribution.TabIndex = 3;
            this.numericUpDownMaxPatternCountPerDistribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownMaxPatternCountPerDistribution.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Distanza minima tra i centri delle classi:";
            // 
            // numericUpDownMinDistanceBetweenMeanVectors
            // 
            this.numericUpDownMinDistanceBetweenMeanVectors.DecimalPlaces = 1;
            this.numericUpDownMinDistanceBetweenMeanVectors.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMinDistanceBetweenMeanVectors.Location = new System.Drawing.Point(307, 59);
            this.numericUpDownMinDistanceBetweenMeanVectors.Maximum = new decimal(new int[] {
            14,
            0,
            0,
            65536});
            this.numericUpDownMinDistanceBetweenMeanVectors.Name = "numericUpDownMinDistanceBetweenMeanVectors";
            this.numericUpDownMinDistanceBetweenMeanVectors.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownMinDistanceBetweenMeanVectors.TabIndex = 4;
            this.numericUpDownMinDistanceBetweenMeanVectors.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownMinDistanceBetweenMeanVectors.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(289, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Distanza minima e massima dalla media sulla 1a dimensione:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(289, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Distanza minima e massima dalla media sulla 2a dimensione:";
            // 
            // numericUpDownMaxDistanceOnFirstAxis
            // 
            this.numericUpDownMaxDistanceOnFirstAxis.DecimalPlaces = 1;
            this.numericUpDownMaxDistanceOnFirstAxis.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMaxDistanceOnFirstAxis.Location = new System.Drawing.Point(364, 85);
            this.numericUpDownMaxDistanceOnFirstAxis.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMaxDistanceOnFirstAxis.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMaxDistanceOnFirstAxis.Name = "numericUpDownMaxDistanceOnFirstAxis";
            this.numericUpDownMaxDistanceOnFirstAxis.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownMaxDistanceOnFirstAxis.TabIndex = 6;
            this.numericUpDownMaxDistanceOnFirstAxis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownMaxDistanceOnFirstAxis.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // numericUpDownMaxDistanceOnSecondAxis
            // 
            this.numericUpDownMaxDistanceOnSecondAxis.DecimalPlaces = 1;
            this.numericUpDownMaxDistanceOnSecondAxis.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMaxDistanceOnSecondAxis.Location = new System.Drawing.Point(364, 111);
            this.numericUpDownMaxDistanceOnSecondAxis.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMaxDistanceOnSecondAxis.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMaxDistanceOnSecondAxis.Name = "numericUpDownMaxDistanceOnSecondAxis";
            this.numericUpDownMaxDistanceOnSecondAxis.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownMaxDistanceOnSecondAxis.TabIndex = 8;
            this.numericUpDownMaxDistanceOnSecondAxis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownMaxDistanceOnSecondAxis.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(340, 209);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(75, 23);
            this.buttonGenerate.TabIndex = 14;
            this.buttonGenerate.Text = "Generate";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Seme generatore:";
            // 
            // radioButtonPredefinedSeed
            // 
            this.radioButtonPredefinedSeed.AutoSize = true;
            this.radioButtonPredefinedSeed.Checked = true;
            this.radioButtonPredefinedSeed.Location = new System.Drawing.Point(113, 137);
            this.radioButtonPredefinedSeed.Name = "radioButtonPredefinedSeed";
            this.radioButtonPredefinedSeed.Size = new System.Drawing.Size(75, 17);
            this.radioButtonPredefinedSeed.TabIndex = 9;
            this.radioButtonPredefinedSeed.TabStop = true;
            this.radioButtonPredefinedSeed.Text = "Predefinito";
            this.radioButtonPredefinedSeed.UseVisualStyleBackColor = true;
            this.radioButtonPredefinedSeed.CheckedChanged += new System.EventHandler(this.radioButtonSeed_CheckedChanged);
            // 
            // radioButtonRandomSeed
            // 
            this.radioButtonRandomSeed.AutoSize = true;
            this.radioButtonRandomSeed.Location = new System.Drawing.Point(113, 160);
            this.radioButtonRandomSeed.Name = "radioButtonRandomSeed";
            this.radioButtonRandomSeed.Size = new System.Drawing.Size(63, 17);
            this.radioButtonRandomSeed.TabIndex = 11;
            this.radioButtonRandomSeed.Text = "Casuale";
            this.radioButtonRandomSeed.UseVisualStyleBackColor = true;
            this.radioButtonRandomSeed.CheckedChanged += new System.EventHandler(this.radioButtonSeed_CheckedChanged);
            // 
            // numericUpDownPredefinedSeed
            // 
            this.numericUpDownPredefinedSeed.Location = new System.Drawing.Point(307, 137);
            this.numericUpDownPredefinedSeed.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numericUpDownPredefinedSeed.Name = "numericUpDownPredefinedSeed";
            this.numericUpDownPredefinedSeed.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownPredefinedSeed.TabIndex = 10;
            this.numericUpDownPredefinedSeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownPredefinedSeed.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(232, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Numero minimo e massimo di pattern per classe:";
            // 
            // numericUpDownMinPatternCountPerDistribution
            // 
            this.numericUpDownMinPatternCountPerDistribution.Location = new System.Drawing.Point(307, 33);
            this.numericUpDownMinPatternCountPerDistribution.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numericUpDownMinPatternCountPerDistribution.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMinPatternCountPerDistribution.Name = "numericUpDownMinPatternCountPerDistribution";
            this.numericUpDownMinPatternCountPerDistribution.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownMinPatternCountPerDistribution.TabIndex = 2;
            this.numericUpDownMinPatternCountPerDistribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownMinPatternCountPerDistribution.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonSetDefaultParams
            // 
            this.buttonSetDefaultParams.Location = new System.Drawing.Point(12, 209);
            this.buttonSetDefaultParams.Name = "buttonSetDefaultParams";
            this.buttonSetDefaultParams.Size = new System.Drawing.Size(75, 23);
            this.buttonSetDefaultParams.TabIndex = 13;
            this.buttonSetDefaultParams.Text = "Default";
            this.buttonSetDefaultParams.UseVisualStyleBackColor = true;
            this.buttonSetDefaultParams.Click += new System.EventHandler(this.buttonSetDefaultParams_Click);
            // 
            // numericUpDownMinDistanceOnFirstAxis
            // 
            this.numericUpDownMinDistanceOnFirstAxis.DecimalPlaces = 1;
            this.numericUpDownMinDistanceOnFirstAxis.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMinDistanceOnFirstAxis.Location = new System.Drawing.Point(307, 85);
            this.numericUpDownMinDistanceOnFirstAxis.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMinDistanceOnFirstAxis.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMinDistanceOnFirstAxis.Name = "numericUpDownMinDistanceOnFirstAxis";
            this.numericUpDownMinDistanceOnFirstAxis.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownMinDistanceOnFirstAxis.TabIndex = 5;
            this.numericUpDownMinDistanceOnFirstAxis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownMinDistanceOnFirstAxis.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // numericUpDownMinDistanceOnSecondAxis
            // 
            this.numericUpDownMinDistanceOnSecondAxis.DecimalPlaces = 1;
            this.numericUpDownMinDistanceOnSecondAxis.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMinDistanceOnSecondAxis.Location = new System.Drawing.Point(307, 111);
            this.numericUpDownMinDistanceOnSecondAxis.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMinDistanceOnSecondAxis.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownMinDistanceOnSecondAxis.Name = "numericUpDownMinDistanceOnSecondAxis";
            this.numericUpDownMinDistanceOnSecondAxis.Size = new System.Drawing.Size(51, 20);
            this.numericUpDownMinDistanceOnSecondAxis.TabIndex = 7;
            this.numericUpDownMinDistanceOnSecondAxis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownMinDistanceOnSecondAxis.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // checkBoxAddClassId
            // 
            this.checkBoxAddClassId.AutoSize = true;
            this.checkBoxAddClassId.Checked = true;
            this.checkBoxAddClassId.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAddClassId.Location = new System.Drawing.Point(15, 186);
            this.checkBoxAddClassId.Name = "checkBoxAddClassId";
            this.checkBoxAddClassId.Size = new System.Drawing.Size(124, 17);
            this.checkBoxAddClassId.TabIndex = 12;
            this.checkBoxAddClassId.Text = "Aggiungere etichette";
            this.checkBoxAddClassId.UseVisualStyleBackColor = true;
            // 
            // Normal2DDistributionGenerationForm
            // 
            this.AcceptButton = this.buttonGenerate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 240);
            this.Controls.Add(this.checkBoxAddClassId);
            this.Controls.Add(this.numericUpDownMinDistanceOnSecondAxis);
            this.Controls.Add(this.numericUpDownMinDistanceOnFirstAxis);
            this.Controls.Add(this.buttonSetDefaultParams);
            this.Controls.Add(this.numericUpDownMinPatternCountPerDistribution);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericUpDownPredefinedSeed);
            this.Controls.Add(this.radioButtonRandomSeed);
            this.Controls.Add(this.radioButtonPredefinedSeed);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.numericUpDownMaxDistanceOnSecondAxis);
            this.Controls.Add(this.numericUpDownMaxDistanceOnFirstAxis);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericUpDownMinDistanceBetweenMeanVectors);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownMaxPatternCountPerDistribution);
            this.Controls.Add(this.numericUpDownDistributionCount);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Normal2DDistributionGenerationForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Generazione distribuzioni MultiNormali";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDistributionCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxPatternCountPerDistribution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinDistanceBetweenMeanVectors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxDistanceOnFirstAxis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxDistanceOnSecondAxis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPredefinedSeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinPatternCountPerDistribution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinDistanceOnFirstAxis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinDistanceOnSecondAxis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownDistributionCount;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxPatternCountPerDistribution;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownMinDistanceBetweenMeanVectors;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxDistanceOnFirstAxis;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxDistanceOnSecondAxis;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton radioButtonPredefinedSeed;
        private System.Windows.Forms.RadioButton radioButtonRandomSeed;
        private System.Windows.Forms.NumericUpDown numericUpDownPredefinedSeed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDownMinPatternCountPerDistribution;
        private System.Windows.Forms.Button buttonSetDefaultParams;
        private System.Windows.Forms.NumericUpDown numericUpDownMinDistanceOnFirstAxis;
        private System.Windows.Forms.NumericUpDown numericUpDownMinDistanceOnSecondAxis;
        private System.Windows.Forms.CheckBox checkBoxAddClassId;
    }
}