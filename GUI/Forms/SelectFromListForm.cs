using System;
using System.Windows.Forms;

namespace PRLab
{
    public partial class SelectFromListForm : Form
    {
        public SelectFromListForm()
        {
            InitializeComponent();
        }

        private void SelectFromListForm_Load(object sender, EventArgs e)
        {
            if (listBox.Items.Count > 0)
                listBox.SelectedIndex = 0;
            else ButtonOK.Enabled = false;
        }

        private void listBox_DoubleClick(object sender, EventArgs e)
        {
            if (listBox.SelectedItem != null)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}