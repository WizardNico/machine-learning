﻿
using BioLab.Classification.Supervised;
using BioLab.Math.LinearAlgebra;
using BioLab.Common;
using System.IO;
using System;

namespace PRLab.EsercitazioniVR
{
  /// <summary>
  /// Classificatore Neural Network
  /// </summary>
  public class VR_NeuralNetworkClassifier : Classifier
  {
    #region PrivateVariables
    private Vector minFeatureValues;
    private Vector maxFeatureValues;
    InputLayer inputLayer;
    HiddenLayer hiddenLayer;
    OutputLayer outputLayer;
    #endregion

    #region Properties

    // Network Architecture
    public bool Biased { get; private set; }

    // Classification confidence
    public override bool HasConfidence
    {
      get { return true; }
    }

    #endregion

    public VR_NeuralNetworkClassifier(InputLayer inputLayer, HiddenLayer hiddenLayer, OutputLayer outputLayer,
                                    Vector minFeatureValues, Vector maxFeatureValues, bool biased)
      : base(outputLayer.Count)
    {
      // TODO: Complete member initialization
      this.inputLayer = inputLayer;
      this.hiddenLayer = hiddenLayer;
      this.outputLayer = outputLayer;
      this.minFeatureValues = minFeatureValues;
      this.maxFeatureValues = maxFeatureValues;
      Biased = biased;
    }

    public override int Classify(FeatureVector v)
    {
      double[] unused;
      return Classify(v, out unused);
    }

    public override int Classify(FeatureVector v, out double[] confidence)
    {
      var normalizedVector = VR_NeuralNetworkClassifierBuilder.ScaleFeatureVector(v, minFeatureValues, maxFeatureValues);

      // Propagate Pattern
      // lanciare il metodo forward propagation sul pattern
      // normalizzato
      throw new NotImplementedException();

      // Compute the confidence
      confidence = new double[ClassCount];
      double sumProb = 0;
      for (int i = 0; i < outputLayer.Count; i++)
        sumProb += outputLayer.Activation[i];

      int prediction = -1;
      double maxConfidence = -1.0;
      for (int i = 0; i < ClassCount; i++)
      {
        if (sumProb == 0)
        {
          confidence[i] = 0;
        }
        else
        {
          confidence[i] = outputLayer.Activation[i] / sumProb;
        }

        if (confidence[i] > maxConfidence)
        {
          maxConfidence = confidence[i];
          prediction = i;
        }
      }
      return prediction;
    }

    public override void SaveToStream(Stream stream)
    {
      throw new NotImplementedException();
    }

    protected override Data InternalClone()
    {
      throw new NotImplementedException();
    }
  }

  public class VR_NeuralNetworkClassifierBuilder : ClassifierBuilder
  {
    [AlgorithmParameter]
    public double MaxError { get; set; }

    [AlgorithmParameter]
    public int MaxEpochs { get; set; }

    [AlgorithmParameter]
    public int HiddenUnitCount { get; set; }

    [AlgorithmParameter]
    public bool Biased { get; set; }

    [AlgorithmParameter]
    public double LearningRate { get; set; }

    [AlgorithmParameter]
    public int RandomSeed { get; set; }

    public VR_NeuralNetworkClassifierBuilder()
    {
      // Default Parameters
      HiddenUnitCount = 10;
      MaxEpochs = 50000;
      MaxError = 0.1;
      LearningRate = 0.7;
      Biased = true;
      RandomSeed = 17;
    }

    public override string ToString()
    {
      return "VR Neural Network Classifier";
    }

    public override void Run()
    {
      InputLayer inputLayer;
      HiddenLayer hiddenLayer;
      OutputLayer outputLayer;
      Vector minFeatureValues;
      Vector maxFeatureValues;
      Training(TrainingSet,
                MaxError,
                MaxEpochs,
                HiddenUnitCount,
                Biased,
                LearningRate,
                RandomSeed,
                out inputLayer,
                out hiddenLayer,
                out outputLayer,
                out minFeatureValues,
                out maxFeatureValues);

      Classifier = new NeuralNetworkClassifier(inputLayer, hiddenLayer, outputLayer, minFeatureValues, maxFeatureValues, Biased);
    }

    private static void Training(FeatureVectorSet trainingSet, double maxError, int maxEpochs, int hiddenUnitCount, bool biased,
                                  double learningRate, int randomSeed, out InputLayer inputLayer, out HiddenLayer hiddenLayer,
                                  out OutputLayer outputLayer, out Vector minFeatureValues, out Vector maxFeatureValues)
    {
      double error = 0;
      int epoch = 0;

      // Normalize dataset between 0 and 1
      var normalizedDataset = DataNormalization(trainingSet, out minFeatureValues, out maxFeatureValues);

      // Check the number of classes
      var numClasses = normalizedDataset.FindMaxClassIndex() + 1;

      // Network Creation
      var rnd = new Random(randomSeed);
      inputLayer = new InputLayer(trainingSet.Dim, hiddenUnitCount, biased, rnd);
      hiddenLayer = new HiddenLayer(hiddenUnitCount, numClasses, biased, rnd);
      outputLayer = new OutputLayer(numClasses);

      // Network Training
      //var errorsList = new List<string>();
      do
      {
        // Error in the current iteration
        error = 0;

        foreach (var v in normalizedDataset)
        {
          FeedForwardPropagation(v, inputLayer, hiddenLayer, outputLayer, biased);
          error += BackPropagation(inputLayer, hiddenLayer, outputLayer, biased, learningRate);
        }

        // Add Temp Result to the List
        //errorsList.Add(string.Format("It {0}: {1:0.000000}", epoch, Math.Sqrt(error / trainingSet.Count)));

        epoch++;

        // Check RMSE
      } while (Math.Sqrt(error / trainingSet.Count) > maxError && epoch < maxEpochs);
    }

    // Dataset Normalization
    private static FeatureVectorSet DataNormalization(FeatureVectorSet vs, out Vector minFeatureValues, out Vector maxFeatureValues)
    {
      var normalized = new FeatureVectorSet();
      int nFeatures = vs.Dim;

      // prepara la normalizzazione
      minFeatureValues = new Vector(nFeatures);
      maxFeatureValues = new Vector(nFeatures);
      for (int i = 0; i < nFeatures; i++)
      {
        minFeatureValues[i] = double.MaxValue;
        maxFeatureValues[i] = double.MinValue;
      }

      foreach (var v in vs)
      {
        for (int k = 0; k < vs.Dim; k++)
        {
          if (v[k] < minFeatureValues[k])
          {
            minFeatureValues[k] = v[k];
          }
          if (v[k] > maxFeatureValues[k])
          {
            maxFeatureValues[k] = v[k];
          }
        }
      }

      for (int i = 0; i < vs.Count; i++)
      {
        normalized.Add(ScaleFeatureVector(vs[i], minFeatureValues, maxFeatureValues), false);
      }

      return normalized;
    }

    internal static FeatureVector ScaleFeatureVector(FeatureVector vector, Vector minFeatureValues, Vector maxFeatureValues)
    {
      var fv = new FeatureVector(vector.Count);
      fv.Class = vector.Class;
      for (int i = 0; i < vector.Count; i++)
      {
        var diff = maxFeatureValues[i] - minFeatureValues[i];
        if (diff == 0)
        {
          fv[i] = 0;
        }
        else
        {
          fv[i] = (vector[i] - minFeatureValues[i]) / diff;
        }
      }

      return fv;
    }

    // Network Propagation
    internal static void FeedForwardPropagation(FeatureVector pattern, InputLayer inputLayer, HiddenLayer hiddenLayer,
                                                OutputLayer outputLayer, bool biased)
    {
      // Set Current Pattern
      for (int i = 0; i < inputLayer.Count; i++)
      {
        // ESERCITAZIONE VA&R (punto 1)
        // memorizzare il pattern nell'input layer
        throw new NotImplementedException();
      }

      for (int i = 0; i < outputLayer.Count; i++)
      {
        if (pattern.Class == i)
        {
          outputLayer.Target[i] = 1.0;
        }
        else
        {
          outputLayer.Target[i] = 0.0;
        }
      }

      // Compute Hidden Layer activations
      for (int i = 0; i < hiddenLayer.Count; i++)
      {
        // implementare la somma pesata e l'attivazione per
        // l'hidden layer
        throw new NotImplementedException();
      }


      // Compute Output Layer Activations
      for (int i = 0; i < outputLayer.Count; i++)
      {
        // implementare la somma pesata e l'attivazione per
        // l'output layer
        throw new NotImplementedException();
      }
    }

    // Activation Function (Sigmoid)
    private static double ActivationFunction(double net)
    {
      throw new NotImplementedException();
    }

    private static double BackPropagation(InputLayer inputLayer, HiddenLayer hiddenLayer, OutputLayer outputLayer, bool biased,
                                          double learningRate)
    {
      // Compute Output Layer Errors
      double sumOutputErrors = 0;
      for (int i = 0; i < outputLayer.Count; i++)
      {
        // calcolo dell'errrore nell'output layer
        throw new NotImplementedException();

        sumOutputErrors += (outputLayer.Target[i] - outputLayer.Activation[i]) * (outputLayer.Target[i] - outputLayer.Activation[i]) / 2.0;
      }

      // Compute Hidden Layer Errors
      for (int i = 0; i < hiddenLayer.Count; i++)
      {
        // calcolo dell'errrore nell'hidden layer
        throw new NotImplementedException();
      }

      // Update Hidden-Output Weights
      for (int i = 0; i < outputLayer.Count; i++)
      {
        // aggiornamento pesi tra output ed hidden layer
        // se la rete è biased anche i pesi del bias devono essere aggiornati
        throw new NotImplementedException();
      }

      // Update Input-Hidden Weights
      for (int i = 0; i < hiddenLayer.Count; i++)
      {
        // aggiornamento pesi tra hidden ed input layer
        // se la rete è biased anche i pesi del bias devono essere aggiornati
        throw new NotImplementedException();
      }

      return sumOutputErrors;
    }
  }
}