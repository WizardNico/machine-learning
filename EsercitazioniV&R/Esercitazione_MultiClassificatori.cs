﻿
using BioLab.Classification.Supervised;
using System;
using BioLab.Common;
using System.Text;
using System.IO;

namespace PRLab.EsercitazioniVR
{
  /// <summary>
  /// Implementation of multi-classifier system with decision-level fusion
  /// and majority-vote rule.
  /// </summary>
  public class VR_McsMayorVote : MultiClassifier
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="VR_McsMayorVote"/> class.
    /// </summary>
    /// <param name="classifiers">The classifiers to merge.</param>
    public VR_McsMayorVote(Classifier[] classifiers)
      : base(classifiers)
    {
    }

    /// <summary>
    /// Classifies a feature vector with respect to the most voted class. 
    /// </summary>
    /// <param name="vector">The vector to be classified.</param>
    /// <returns>
    /// [0..classesCount-1] or -1 (unknown class)
    /// </returns>
    public override int Classify(FeatureVector vector)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Saves the current <see cref="BioLab.Common.Data"/> to a specified <see cref="System.IO.Stream"/>.
    /// </summary>
    /// <param name="stream">The <see cref="System.IO.Stream"/> where the current <see cref="BioLab.Common.Data"/> will be saved.</param>
    public override void SaveToStream(System.IO.Stream stream)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Creates an exact copy of this instance.
    /// </summary>
    /// <returns>
    /// The <see cref="BioLab.Common.Data"/> this method creates.
    /// </returns>
    protected override Data InternalClone()
    {
      throw new NotImplementedException();
    }
  }

  /// <summary>
  /// Implementation of a multi-classifier system with confidence-level fusion.
  /// </summary>
  /// <remarks>Assumes no normalization in classifiers confidences.</remarks>
  public class VR_McsConfidence : MultiClassifier
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="VR_McsConfidence"/> class.
    /// </summary>
    /// <param name="classifiers">The classifiers to merge.</param>
    /// <param name="fusionRule">The fusion rule.</param>
    public VR_McsConfidence(Classifier[] classifiers, ClassifierFusionMethod fusionRule)
      : base(classifiers)
    {
      // Verify that every classifier support confidence.
      foreach (Classifier c in Classifiers)
      {
        if (!c.HasConfidence)
        {
          throw new ArgumentException("All the classifiers must support confidence.");
        }
      }

      FusionRule = fusionRule;
    }

    /// <summary>
    /// Gets or sets the fusion rule.
    /// </summary>
    /// <value>The fusion rule.</value>
    public ClassifierFusionMethod FusionRule { get; set; }

    /// <summary>
    /// Classifies a feature vector.
    /// </summary>
    /// <param name="vector">The vector to be classified.</param>
    /// <returns>
    /// [0..classesCount-1] or -1 (unknown class)
    /// </returns>
    public override int Classify(FeatureVector vector)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Saves the current <see cref="BioLab.Common.Data"/> to a specified <see cref="System.IO.Stream"/>.
    /// </summary>
    /// <param name="stream">The <see cref="System.IO.Stream"/> where the current <see cref="BioLab.Common.Data"/> will be saved.</param>
    public override void SaveToStream(Stream stream)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Creates an exact copy of this instance.
    /// </summary>
    /// <returns>
    /// The <see cref="BioLab.Common.Data"/> this method creates.
    /// </returns>
    protected override Data InternalClone()
    {
      throw new NotImplementedException();
    }
  }

  /// <summary>
  /// Builder for a multi-classifier system with decision-level fusion and majority-vote rule.
  /// </summary>
  public class VR_McsMayorVoteBuilder : ClassifierBuilder
  {
    /// <summary>
    /// Gets or sets the classifier builders used to create the multi-classifier.
    /// </summary>
    [AlgorithmInput]
    public ClassifierBuilder[] ClassifierBuilders { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="VR_McsMayorVoteBuilder"/> class.
    /// </summary>
    /// <param name="classifierBuilders">The classifier builders to use in the creation of the <see cref="VR_McsMayorVoteBuilder"/>.</param>
    public VR_McsMayorVoteBuilder(ClassifierBuilder[] classifierBuilders)
    {
      ClassifierBuilders = (ClassifierBuilder[])classifierBuilders.Clone(); // copia i riferimenti dei builder in un nuovo array
    }

    /// <summary>
    /// Executes the Training of the different classifiers and return the multi-classifier.
    /// </summary> 
    public override void Run()
    {
      var classifiers = new Classifier[ClassifierBuilders.Length];
      for (int i = 0; i < ClassifierBuilders.Length; i++)
      {
        classifiers[i] = ClassifierBuilders[i].Train(TrainingSet);
      }

      Classifier = new VR_McsMayorVote(classifiers);
    }

    /// <summary>
    /// Returns a string that represents the current <see cref="McsMajorVoteBuilder"/>.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      var strBuilder = new StringBuilder();
      strBuilder.Append("VR Mcs Mayor Classifier (");
      for (int i = 0; i < ClassifierBuilders.Length; i++)
      {
        strBuilder.Append((i > 0 ? "," : string.Empty) + ClassifierBuilders[i].ToString());
      }
      strBuilder.Append(")");

      return strBuilder.ToString();
    }
  }

  /// <summary>
  /// Builder for a multi-classifier system with confidence-level fusion.
  /// </summary>
  public class VR_McsConfidenceBuilder : ClassifierBuilder
  {
    /// <summary>
    /// Gets or sets the classifier builders used to create the multi-classifier.
    /// </summary>
    [AlgorithmInput]
    public ClassifierBuilder[] ClassifierBuilders { get; set; }

    /// <summary>
    /// Gets or sets the fusion rule.
    /// </summary>
    /// <value>The fusion rule.</value>
    [AlgorithmInput]
    public ClassifierFusionMethod FusionRule { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="VR_McsConfidenceBuilder"/> class.
    /// </summary>
    /// <param name="classifierBuilders">The classifier builders to use in the creation of the <see cref="VR_McsConfidenceBuilder"/>.</param>
    /// <param name="fusionRule">The fusion rule.</param>
    public VR_McsConfidenceBuilder(ClassifierBuilder[] classifierBuilders, ClassifierFusionMethod fusionRule)
    {
      ClassifierBuilders = (ClassifierBuilder[])classifierBuilders.Clone(); // copia i riferimenti dei builder in un nuovo array

      FusionRule = fusionRule;
    }

    /// <summary>
    /// Executes the Training of the different classifiers and return the multi-classifier.
    /// </summary> 
    public override void Run()
    {
      var classifiers = new Classifier[ClassifierBuilders.Length];
      for (int i = 0; i < ClassifierBuilders.Length; i++)
      {
        classifiers[i] = ClassifierBuilders[i].Train(TrainingSet);
      }

      Classifier = new VR_McsConfidence(classifiers, FusionRule);
    }

    /// <summary>
    /// Returns a string that represents the current <see cref="McsConfidenceBuilder"/>.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      var strBuilder = new StringBuilder();
      strBuilder.AppendFormat("VR Mcs Confidence Classifier[{0}] (", FusionRule);
      for (int i = 0; i < ClassifierBuilders.Length; i++)
      {
        strBuilder.Append((i > 0 ? "," : string.Empty) + ClassifierBuilders[i].ToString());
      }
      strBuilder.Append(")");

      return strBuilder.ToString();
    }
  }
}