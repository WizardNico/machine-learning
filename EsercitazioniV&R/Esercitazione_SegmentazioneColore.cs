﻿
using BioLab.Common;
using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using BioLab.GUI.DataViewers;
using BioLab.Math.Geometry;
using BioLab.GUI.Forms;
using PRLab.GUI.DataViewers;
using System.Text;
using BioLab.Classification.Supervised;
using BioLab.Classification.DensityEstimation;

namespace PRLab.EsercitazioniVR
{
  [CustomAlgorithmPreviewOutputAttribute(typeof(ColorSegmentationOutputViewer))]
  public class VR_ColorSegmentation : Algorithm
  {
    [AlgorithmInput]
    public ImageWithMarkedRegions ForegroundImage { get; set; }

    [AlgorithmInput]
    public ImageWithMarkedRegions BackgroundImage { get; set; }

    [AlgorithmInput]
    public RgbImage<byte> ImageToSegment { get; set; }

    [AlgorithmParameter]
    public ColorSpace FeatureColorSpace { get; set; }

    [AlgorithmOutput]
    public Image<byte> SegmentationMask { get; private set; }

    public override void Run()
    {
      // Crea dati di training
      var trainingSet = new FeatureVectorSet();
      throw new NotImplementedException();
      
      // Addestra il classificatore
      var classifier = new VR_BayesNormalClassifierBuilder().Train(trainingSet);
            
      // Segmenta
      SegmentationMask = new Image<byte>(ImageToSegment.Width, ImageToSegment.Height);
      throw new NotImplementedException();
    }

    private static MultipleChannelImage<byte> FeatureImage(RgbImage<byte> rgbImage, ColorSpace colorSpace)
    {
      MultipleChannelImage<byte> featureImage;

      switch (colorSpace)
      {
        case ColorSpace.RGB:
          featureImage=rgbImage;
          break;
        case ColorSpace.HSL:
          featureImage=rgbImage.ToByteHslImage();
          break;
        case ColorSpace.YUV:
          featureImage=rgbImage.ToByteYuvImage();
          break;
        case ColorSpace.YCbCr:
          featureImage=rgbImage.ToByteYCbCrImage();
          break;
        default:
          throw new Exception("Color space not supported.");
      }

      return featureImage;
    }

    private static FeatureVector ExtractFeatures(MultipleChannelImage<byte> image, int pixelIdx)
    {
      throw new NotImplementedException();
    }
  }

  public enum ColorSpace { RGB, HSL, YUV,YCbCr };

  /// <summary>
  /// Un'immagine con una serie di regioni poligonali
  /// </summary>
  [DefaultDataViewer(typeof(ImageWithMarkedRegionsViewer))]
  public class ImageWithMarkedRegions : RgbImage<byte>
  {
    /// <summary>
    /// Regioni nell'immagine
    /// </summary>
    public MarkedRegionList Regions = new MarkedRegionList();

    public ImageWithMarkedRegions(ImageBase img)
      :base(img.ToByteRgbImage())
    {
    }

    /// <summary>
    /// Calcola il numero totale di punti nelle varie regioni
    /// </summary>
    public int CountTotalPoints()
    {
      int n = 0;
      foreach (var r in Regions)
        n += r.Points.Count;
      return n;
    }

    /// <summary>
    /// Carica le regioni da file
    /// </summary>
    public void LoadRegionsFromTextFile(string pathname)
    {
      var lines = File.ReadAllLines(pathname);
      Regions = new MarkedRegionList();
      var count = int.Parse(lines[0]);
      for (int i = 1; i <= count; i++)
      {
        var region = new MarkedRegion();

        var splittedLine = lines[i].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
        var pointCount = int.Parse(splittedLine[0]);
        region.Closed = bool.Parse(splittedLine[1]);
        var k = 2;
        for (int j = 0; j < pointCount; j++)
        {
          var x = int.Parse(splittedLine[k++]);
          var y = int.Parse(splittedLine[k++]);
          region.Points.Add(new IntPoint2D(x, y));
        }

        Regions.Add(region);
      }
    }

    /// <summary>
    /// Salva le regioni su file
    /// </summary>
    public void SaveRegionsToTextFile(string filePath)
    {
      var strBuilder = new StringBuilder();
      strBuilder.AppendLine(Regions.Count.ToString());
      foreach (var r in Regions)
      {
        strBuilder.AppendFormat("{0} {1}", r.Points.Count, r.Closed);
        foreach (var p in r.Points)
        {
          strBuilder.AppendFormat(" {0} {1}", p.X, p.Y);
        }
        strBuilder.AppendLine();
      }

      File.WriteAllText(filePath, strBuilder.ToString());
    }

    public override void SaveToStream(Stream stream)
    {
      throw new NotImplementedException();
    }

    protected override Data InternalClone()
    {
      throw new NotImplementedException();
    }
  }

  public class MarkedRegionList : IEnumerable<MarkedRegion>
  {
    #region Membri privati
    List<MarkedRegion> regions = new List<MarkedRegion>();
    #endregion

    public void Add(MarkedRegion markedRegion)
    {
      regions.Add(markedRegion);
    }

    public bool Remove(MarkedRegion r)
    {
      return regions.Remove(r);
    }

    public int Count { get { return regions.Count; } }


    public MarkedRegion this[int index]
    {
      get { return regions[index]; }
      set { regions[index] = value; }
    }


    #region IEnumerable<MarkedRegion> Members

    public IEnumerator<MarkedRegion> GetEnumerator()
    {
      return regions.GetEnumerator();
    }

    #endregion

    #region IEnumerable Members

    IEnumerator IEnumerable.GetEnumerator()
    {
      return regions.GetEnumerator();
    }

    #endregion

    #region ICloneable Members

    public object Clone()
    {
      var res = new MarkedRegionList();
      foreach (var r in regions)
      {
        res.Add((MarkedRegion) r.Clone());
      }

      return res;
    }

    #endregion
  }

  /// <summary>
  /// Una regione in un'immagine (in coordinate pixel dell'immagine)
  /// </summary>
  public class MarkedRegion : ICloneable
  {
    public List<IntPoint2D> Points = new List<IntPoint2D>();
    public bool Closed = false;

    public bool IsInside(IntPoint2D p)
    {
      return IsInside(p.X, p.Y);
    }

    public bool IsInside(int x, int y)
    {
      if (!Closed)
        return false;
      bool c = false;
      for (int i = 0, j = Points.Count - 1; i < Points.Count; j = i++)
      {
        if ((((Points[i].Y <= y) && (y < Points[j].Y)) ||
             ((Points[j].Y <= y) && (y < Points[i].Y))) &&
            (x < (Points[j].X - Points[i].X) * (y - Points[i].Y) / (Points[j].Y - Points[i].Y) + Points[i].X))
          c = !c;
      }
      return c;
    }

    #region ICloneable Members

    public object Clone()
    {
      MarkedRegion r = new MarkedRegion();
      r.Points = new List<IntPoint2D>(Points);
      r.Closed = Closed;
      return r;
    }

    #endregion
  }
}