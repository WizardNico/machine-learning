﻿using BioLab.Math.Geometry;
using System;
using BioLab.ImageProcessing;
using BioLab.Common;
using System.ComponentModel;
using System.Collections.Generic;
using BioLab.Math.LinearAlgebra;
using System.IO;
using System.Windows.Forms;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Linq;

namespace PRLab.EsercitazioniVR
{
    public class VR_SIFTObjectLocalizer : Algorithm
    {

        private const double defaultMinScale = 0.6;
        private const double defaultMaxScale = 0.8;
        private const double defaultScaleIncrementStep = 0.1;
        private const double defaultDistanceThr = 0.95;
        private const double defaultStepPercentage = 0.2;

        [AlgorithmInput]
        public Image<byte> TestImage { get; set; }

        [AlgorithmInput]
        public Image<byte>[][] TrainingSet { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultMinScale)]
        public double MinScale { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultMaxScale)]
        public double MaxScale { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultScaleIncrementStep)]
        public double ScaleIncrementStep { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultDistanceThr)]
        public double DistanceThr { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultStepPercentage)]
        public double StepPercentage { get; set; }

        [AlgorithmOutput]
        public ObjectCandidate[] DetectedObjects { get; private set; }

        public VR_SIFTObjectLocalizer()
        {
            MinScale = defaultMinScale;
            MaxScale = defaultMaxScale;
            ScaleIncrementStep = defaultScaleIncrementStep;
            StepPercentage = defaultStepPercentage;
            DistanceThr = defaultDistanceThr;
        }

        public VR_SIFTObjectLocalizer(double minScale, double maxScale, double scaleIncrementStep, double stepPercentage,
                                      double distanceThr)
        {
            MinScale = minScale;
            MaxScale = maxScale;
            ScaleIncrementStep = scaleIncrementStep;
            StepPercentage = stepPercentage;
            DistanceThr = DistanceThr;
        }

        public override void Run()
        {
            var scaleFactors = new List<double>();
            for (var scale = MinScale; scale <= MaxScale; scale += ScaleIncrementStep)
            {
                scaleFactors.Add(scale);
            }
            var testImageResized = ComputeTestImageResized(scaleFactors.ToArray());

            var classCount = TrainingSet.Length;
            var objectCandidatePerClass = new List<ObjectCandidate>[classCount];

            var classPartition = Partitioner.Create(0, classCount);
            Parallel.ForEach(classPartition, pClass =>
            {
                for (int c = pClass.Item1; c < pClass.Item2; c++)
                {
                    var imgPerClassCount = TrainingSet[c].Length;

                    var objectCandidatePerImg = new List<ObjectCandidate>[imgPerClassCount];
                    var imgPartition = Partitioner.Create(0, imgPerClassCount);
                    Parallel.ForEach(imgPartition, pImg =>
                    {
                        var featureExtractor = new VR_DenseSift();
                        featureExtractor.SamplingSpacing = 8;
                        featureExtractor.Scale = 1.5;

                        for (int i = pImg.Item1; i < pImg.Item2; i++)
                        {
                            throw new NotImplementedException();
                        }
                    }
                    );

                    objectCandidatePerClass[c] = new List<ObjectCandidate>(objectCandidatePerImg[0]);
                    for (int i = 1; i < imgPerClassCount; i++)
                    {
                        objectCandidatePerClass[c].AddRange(objectCandidatePerImg[i]);
                    }
                    objectCandidatePerClass[c] = PostProcessing(objectCandidatePerClass[c], 0.5);
                }
            }
            );

            var objectCandidates = new List<ObjectCandidate>(objectCandidatePerClass[0]);
            for (int c = 1; c < classCount; c++)
            {
                objectCandidates.AddRange(objectCandidatePerClass[c]);
            }

            DetectedObjects = objectCandidates.ToArray();
        }

        private Image<byte>[] ComputeTestImageResized(double[] scaleFactors)
        {
            var testImageResized = new Image<byte>[scaleFactors.Length];
            var partition = Partitioner.Create(0, scaleFactors.Length);
            Parallel.ForEach(partition, p =>
            {
                for (int i = p.Item1; i < p.Item2; i++)
                {
                    testImageResized[i] = TestImage.Resize(scaleFactors[i]);
                }
            }
            );

            return testImageResized;
        }

        private List<ObjectCandidate> PostProcessing(List<ObjectCandidate> candidates, double overlapThr)
        {
            throw new NotImplementedException();
        }
    }

    public class ObjectCandidate
    {
        public IntRectangle Rectangle;
        public int Class;
        public double Distance { get; set; }

        public ObjectCandidate(IntRectangle rect, int cl, double dist)
        {
            Rectangle = rect;
            Class = cl;
            Distance = dist;
        }
    }
}