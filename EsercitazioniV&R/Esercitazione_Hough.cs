﻿using BioLab.Common;
using System;
using BioLab.GUI.Forms;
using BioLab.ImageProcessing;
using BioLab.PatternRecognition.Localization;
using System.ComponentModel;

namespace PRLab.EsercitazioniVR
{
  /// <summary>
  /// Performs a Hough transform to detect lines in a grayscale image.
  /// </summary>
  [CustomAlgorithmPreviewOutput(typeof(VR_HoughLinesDetectorOutputViewer))]
  public class VR_HoughLinesDetector : ImageOperation<Image<byte>, HoughLineParameters[]>
  {
    #region Constructors (2)

    /// <summary>
    /// Initializes a new instance of the <see cref="HoughLinesDetector"/> class.
    /// </summary>
    public VR_HoughLinesDetector()
    {
      NumberOfLines = defaultNumberOfLines;
      GradientCalculationMethod = defaultGradientCalculationMethod;
      BinarizationThreshold = defaultBinarizationThreshold;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="HoughLinesDetector"/> class.
    /// </summary>
    /// <param name="inputImage">The input image.</param>
    public VR_HoughLinesDetector(Image<byte> inputImage)
      : base(inputImage)
    {
      NumberOfLines = defaultNumberOfLines;
      GradientCalculationMethod = defaultGradientCalculationMethod;
      BinarizationThreshold = defaultBinarizationThreshold;
    }

    #endregion Constructors

    #region Fields (5)

    private int thetaRange = 180 / 5;
    private int roMax;
    private int roRange;
    private int[,] A;
    private const int defaultNumberOfLines = 10;
    private const GradientCalculationMethod defaultGradientCalculationMethod = GradientCalculationMethod.Prewitt;
    private const byte defaultBinarizationThreshold = 128;

    #endregion Fields

    /// <summary>
    /// Indicates the number of line to detect.
    /// </summary>
    /// <value>The number of line to detect</value>
    [AlgorithmParameter]
    [DefaultValue(defaultNumberOfLines)]
    public int NumberOfLines { get; set; }

    /// <summary>
    /// Gets or sets the gradient calculation method.
    /// </summary>
    /// <value>The gradient calculation method.</value>
    [AlgorithmParameter]
    [DefaultValue(defaultGradientCalculationMethod)]
    public GradientCalculationMethod GradientCalculationMethod { get; set; }

    /// <summary>
    /// Gets or sets the binarization threshold.
    /// </summary>
    /// <value>The binarization threshold.</value>
    [AlgorithmParameter]
    [DefaultValue(defaultBinarizationThreshold)]
    public byte BinarizationThreshold { get; set; }

    /// <summary>
    /// Gets the modulus of the gradient for each pixel.
    /// </summary>
    /// <value>The gradient moduli.</value>
    [AlgorithmOutput]
    public Image<double> GradientModuli { get; private set; }

    /// <summary>
    /// Gets the binarized image of the modulus of the gradient.
    /// </summary>
    /// <value>The binarized gradient moduli image.</value>
    [AlgorithmOutput]
    public Image<byte> BinarizedGradientModuli { get; private set; }

    /// <summary>
    /// Executes the Hough transform.
    /// </summary>
    public override void Run()
    {
      //Calcolo del gradiente e memorizzazione del modulo all'interno del parametro GradientModuli
      throw new NotImplementedException();

      //Binarizzazione di GradientModuli e memorizzazione del risultato all'interno del parametro BinarizedGradientModuli
      throw new NotImplementedException();

      var squareWidth = BinarizedGradientModuli.Width * BinarizedGradientModuli.Width;
      var squareHeight=BinarizedGradientModuli.Height*BinarizedGradientModuli.Height;
      roMax = (int)Math.Ceiling(Math.Sqrt(squareWidth + squareHeight));
      roRange = roMax + roMax + 1;
      A = new int[thetaRange, roRange];

      //Calcolare ro per ogni pixel diverso da 0 in BinarizedGradientModuli e per ogni theta e incrementare l'elemento
      //corrispondente in A
      throw new NotImplementedException();

      //Memorizzare in Result i NumberOfLines parametri (HoughLineParameters) con il maggior numero di voti in A
      throw new NotImplementedException();
    }
  } 
}