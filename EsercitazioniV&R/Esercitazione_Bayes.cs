﻿
using BioLab.Classification.DensityEstimation;
using BioLab.Common;
using BioLab.Math.Probability;
using BioLab.Math.LinearAlgebra;
using System.Diagnostics;
using System;
using BioLab.Classification.Supervised;
using System.IO;

namespace PRLab.EsercitazioniVR
{
  /// <summary>
  /// Represents a Normal Distribution
  /// </summary>
  public class VR_NormalDistribution : ProbabilityDistribution
  {
    /// <summary>
    /// Mean vector
    /// </summary>
    public Vector Mean { get; set; }

    /// <summary>
    /// Covariance matrix
    /// </summary>
    public Matrix Covariance { get; set; }

    /// <summary>
    /// True if the covariance matrix became singular
    /// </summary>
    /// <value><c>true</c> if the distribution is invalid; otherwise, <c>false</c>.</value>
    public bool Invalid { get; set; }

    // Inverse of covariance matrix
    private Matrix invΣ;

    //Normalization factor
    private double normFactor;

    /// <summary>
    /// Compute the probability density
    /// </summary>
    public override double CalculateDensity(Vector x)
    {
      if (Invalid)
      {
        return 0;
      }

      throw new NotImplementedException();
    }

    /// <summary>
    /// To call if mean or covariance are modified
    /// </summary>
    public void PreCalculateData()
    {
      double determinant = Covariance.ComputeDeterminant();
      if (determinant <= double.Epsilon)
      {
        Invalid = true;
      }
      else
      {
        throw new NotImplementedException();

        Invalid = false;
      }
    }

    public override void SaveToStream(Stream stream)
    {
      throw new NotImplementedException();
    }

    protected override Data InternalClone()
    {
      throw new NotImplementedException();
    }
  }

  /// <summary>
  /// Parametric maximum likelihood estimation, assuming normal distributions.
  /// </summary>
  public class VR_NormalMLEstimator : DensityEstimator
  {
    /// <summary>
    /// Initializes a new instance of the VR_NormalMLEstimator class.
    /// </summary>
    public VR_NormalMLEstimator()
    {
    }

    /// <summary>
    /// Initializes a new instance of the VR_NormalMLEstimator class.
    /// </summary>
    /// <param name="trainingSet">The training set.</param>
    public VR_NormalMLEstimator(FeatureVectorSet trainingSet)
      : base(trainingSet)
    {
    }

    /// <summary>
    /// Executes the estimation on the training set.
    /// </summary>
    public override void Run()
    {
      int classCount = TrainingSet.FindMaxClassIndex() + 1;
      if (classCount == 0)
        classCount = 1;
      int[] numPerClass = new int[classCount];
      int n = TrainingSet.Dim;

      VR_NormalDistribution[] dist = new VR_NormalDistribution[classCount];

      // Stima le medie
      
      // Stima le matrici di covarianza

      throw new NotImplementedException();

      ClassConditionalDensities = dist;
    }

    public override string ToString()
    {
      return "VR_Normal";
    }
  }

  /// <summary>
  /// Represents a classifier based on the Bayes Classification Rule 
  /// </summary>
  public class VR_BayesClassifier : Classifier
  {
    private IProbabilityDensity[] classConditionalDensities;
    private double[] priorProbabilities;

    /// <summary>
    /// Initializes a new instance of the VR_BayesClassifier class.
    /// </summary>
    /// <param name="classConditionalDensities">The class conditional density for each class.</param>
    /// <param name="priorProbabilities">The prior probabilitie for each class.</param>
    public VR_BayesClassifier(IProbabilityDensity[] classConditionalDensities, double[] priorProbabilities)
      : base(classConditionalDensities.Length)
    {
      this.classConditionalDensities = new IProbabilityDensity[classConditionalDensities.Length];
      for (int i = 0; i < classConditionalDensities.Length; i++)
      {
        this.classConditionalDensities[i] = classConditionalDensities[i];
      }

      this.priorProbabilities = (double[])priorProbabilities.Clone();
    }

    /// <summary>
    /// Initializes a new instance of the VR_BayesClassifier class. The class conditional
    /// densities are given by the user. The priors are computed as fractions of examples of training 
    /// set belonging to a given class. 
    /// </summary>
    /// <param name="classConditionalDensities">The class conditional densities.</param>
    public VR_BayesClassifier(IProbabilityDensity[] classConditionalDensities)
      : base(classConditionalDensities.Length)
    {
      this.classConditionalDensities = classConditionalDensities;
      priorProbabilities = new double[classConditionalDensities.Length];
      for (int i = 0; i < classConditionalDensities.Length; i++)
      {
        priorProbabilities[i] = 1.0 / classConditionalDensities.Length;
      }
    }

    /// <summary>
    /// Classifies a feature vector.
    /// </summary>
    /// <param name="vector">The vector to be classified.</param>
    /// <returns>
    /// [0..classesCount-1] or -1 (unknown class)
    /// </returns>
    public override int Classify(FeatureVector vector)
    {
      double[] unused;
      return Classify(vector, out unused);
    }

    /// <summary>
    /// True if the classifier returns confidence information.
    /// </summary>
    public override bool HasConfidence { get { return true; } }

    /// <summary>
    /// Classifies the specified vector.
    /// </summary>
    /// <param name="vector">The vector.</param>
    /// <param name="confidence">The confidence of classification.</param>
    /// <returns></returns>
    public override int Classify(FeatureVector vector, out double[] confidence)
    {
      confidence = new double[priorProbabilities.Length];
      double maxPostProbability = 0;
      int max = -1;
      double probTot = 0; // Total probabilities (denominator in the bayes rule)

      //Compute the posterior proabilities and return the index of the greater.
      throw new NotImplementedException();

      return max;
    }

    public override void SaveToStream(Stream stream)
    {
      throw new NotImplementedException();
    }

    protected override Data InternalClone()
    {
      throw new NotImplementedException();
    }
  }

  /// <summary>
  /// Builder of Bayes Classifier. 
  /// </summary>
  public class VR_BayesNormalClassifierBuilder : ClassifierBuilder
  {
    /// <summary>
    /// Initializes a new instance of the VR_BayesNormalClassifierBuilder class.
    /// </summary>
    /// <param name="densityEstimator">The density estimator.</param>
    public VR_BayesNormalClassifierBuilder()
    {
    }

    /// <summary>
    /// Initializes a new instance of the VR_BayesNormalClassifierBuilder class.
    /// </summary>
    /// <param name="trainingSet">The training set.</param>
    /// <param name="densityEstimator">The density estimator.</param>
    public VR_BayesNormalClassifierBuilder(FeatureVectorSet trainingSet)
      : base(trainingSet)
    {
    }

    /// <summary>
    /// Get the Densitiy Estimator and build a Bayes Classifier ready for the classification
    /// computing the class conditional densities and the priors.
    /// </summary>
    public override void Run()
    {
      var densityEstimator = new NormalMLEstimator();
      densityEstimator.TrainingSet = TrainingSet;
      densityEstimator.Run();
      Classifier = new VR_BayesClassifier(densityEstimator.ClassConditionalDensities, CalculatePriorProbabilities());
    }

    /// <summary>
    /// Calculates the prior probabilities.
    /// </summary>
    /// <returns>An array of double containing the priors for each class.</returns>
    private double[] CalculatePriorProbabilities()
    {
      throw new NotImplementedException();
    }

    public override string ToString()
    {
      return "VR Bayes Normal Classifier";
    }
  }
}