﻿
using BioLab.Classification.Unsupervised;
using BioLab.Common;
using System;
using BioLab.Math.LinearAlgebra;

namespace PRLab.EsercitazioniVR
{
  /// <summary>
  /// Represents a k-Means clustering algorithm.
  /// </summary>
  [AlgorithmInfo("VR K-Means")]
  public class VR_KMeanClustering : IterativeClusterizer
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="KMeanClustering"/> class.
    /// </summary>
    /// <remarks>Note that the input parameters must be initialized before running the algorithm.</remarks>
    public VR_KMeanClustering()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="KMeanClustering"/> class and allows to specify the vector set and the number of clusters.
    /// </summary>
    /// <param name="vectorSet">The set of vectors to be clusterized.</param>
    /// <param name="clusterCount">The number of clusters to be created.</param>
    public VR_KMeanClustering(FeatureVectorSet vectorSet, int clusterCount)
      : base(vectorSet)
    {
      ClusterCount = clusterCount;
    }


    /// <summary>
    /// This method is called before the first iteration, to initialize the data structures and other internal parameters of the algorithm.
    /// </summary>
    protected override void Initialize()
    {
      centroids = new Vector[ClusterCount];
      numPerCluster = new int[ClusterCount];
      
      // inizializza i centri, per semplicità sceglie vettori random in vectorSet
      throw new NotImplementedException();

      AssignVectorsToCentroids(); // Esegue il primo assegnamento
    }

    /// <summary>
    /// Executes one iteration of the algorithm.
    /// </summary>
    /// <returns>
    /// true if the algorithm converged, false otherwise.
    /// </returns>
    protected override bool Iteration()
    {
      int n = VectorSet.Dim;
      
      // Ricalcola i centri
      throw new NotImplementedException();

      // Aggiorna i vettori
      return !AssignVectorsToCentroids();
    }

    /// <summary>
    /// Gets the current centroid of a given cluster.
    /// </summary>
    /// <param name="clusterIndex">Index of the cluster.</param>
    /// <returns>The <see cref="FeatureVector"/> corresponding to the centroid.</returns>
    public Vector GetCentroid(int clusterIndex)
    {
      return (Vector)centroids[clusterIndex].Clone();
    }


    #region Membri privati
    Vector[] centroids;
    int[] numPerCluster;

    /// <summary>
    /// Riassegna i vettori ai cluster in base alla minima distanza dai centri
    /// </summary>
    /// <returns>true se almeno un vettore è stato assegnato a un diverso cluster</returns>
    private bool AssignVectorsToCentroids()
    {
      throw new NotImplementedException();
    }
    #endregion


    /// <summary>
    /// Gets or sets the number of clusters to be created by the algorithm.
    /// </summary>
    /// <value>The number of clusters.</value>
    [AlgorithmParameter]
    public int ClusterCount { get; set; }
  }
}