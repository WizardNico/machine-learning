﻿
using BioLab.DimensionalityReduction;
using System;
using BioLab.Common;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;

namespace PRLab.EsercitazioniVR
{
  /// <summary>
  /// Una semplice implementazione della costruzione di un modello Active Shape
  /// </summary>
  class VR_ActiveShapeModel
  {
    #region Membri privati
    PcaTransform KL;
    MarkedRegionList templateRegions; // utilizzato per sapere come mappre i punti nelle regioni
    bool centerShape;
    #endregion


    /// <summary>
    /// Costruisce il modello a partire da un insieme di immagini etichettate
    /// </summary>
    /// <param name="trainData">Le immagini</param>
    /// <param name="center">True se si vuole che ogni immagine sia centrata rispetto al suo baricentro</param>
    /// <param name="numModes">Numero di modi di variazione (dimensione spazio ridotto)</param>
    public void Calculate(ImageWithMarkedRegions[] trainData, bool center, int numModes)
    {
      var vs = new FeatureVectorSet();

      throw new NotImplementedException();

      // Calcola la KL
      KL =(PcaTransform) new PcaTransformBuilder(vs, numModes).Calculate();
    }


    /// <summary>
    /// Restituisce un range di variazione appropriato per il modo di indice index
    /// </summary>
    public double GetModeRange(int index)
    {
      return 3 * Math.Sqrt(KL.Eigenvalues[index]); // 3 sigma
    }

    /// <summary>
    /// Crea la forma corrispondente ai valori in modeValues
    /// </summary>
    public void AdjustModel(ImageWithMarkedRegions imr, Vector modeValues)
    {
      throw new NotImplementedException();
    }
  }
}