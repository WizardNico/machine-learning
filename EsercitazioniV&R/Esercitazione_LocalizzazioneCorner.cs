﻿
using BioLab.Math.Geometry;
using BioLab.GUI.Forms;
using BioLab.GUI.DataViewers;
using BioLab.Common;
using System.ComponentModel;
using System;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System.Collections.Generic;

namespace PRLab.EsercitazioniVR
{
    [CustomAlgorithmPreviewOutputAttribute(typeof(VR_HarrisCornerDetectorOutputViewer))]
    public class VR_HarrisCornerDetector : ImageOperation<Image<byte>, IntPoint2D[]>
    {
        private const int defaultR = 3;
        private const double defaultAlfa = 0.04;
        private const int defaultCornernessThr = 300000;
        private const double defaultSigma = 1.4;
        private const int defaultFilterSize = 7;

        private int r;
        private double alfa;
        private int cornernessThr;
        private double sigma;
        private int filterSize;

        [AlgorithmParameter]
        [DefaultValue(defaultR)]
        public int R
        {
            get { return r; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("value", "The r parameter must be greater or equal to zero.");
                }
                else
                {
                    r = value;
                }
            }
        }

        [AlgorithmParameter]
        [DefaultValue(defaultAlfa)]
        public double Alfa { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultSigma)]
        public double Sigma { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultCornernessThr)]
        public int CornernessThreshold { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultFilterSize)]
        public int FilterSize { get; set; }

        public VR_HarrisCornerDetector()
        {
            R = defaultR;
            Alfa = defaultAlfa;
            CornernessThreshold = defaultCornernessThr;
            Sigma = defaultSigma;
            FilterSize = defaultFilterSize;
        }

        public VR_HarrisCornerDetector(int r, double alfa, int cornernessThr, double sigma, int filterSize)
        {
            R = r;
            Alfa = alfa;
            CornernessThreshold = cornernessThr;
            Sigma = sigma;
            FilterSize = filterSize;
        }

        public override void Run()
        {
          throw new NotImplementedException();
        }
    }
}