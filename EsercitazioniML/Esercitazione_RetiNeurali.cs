﻿
using BioLab.Classification.Supervised;
using BioLab.Math.LinearAlgebra;
using BioLab.Common;
using System.IO;
using System;

namespace PRLab.EsercitazioniML {
	/* Classe principare per la definizione del Classificatore */
	public class MyNeuralNetworkClassifier : Classifier {
		#region PrivateVariables
		private Vector minFeatureValues;
		private Vector maxFeatureValues;
		InputLayer inputLayer;
		HiddenLayer hiddenLayer;
		OutputLayer outputLayer;
		#endregion

		#region Properties

		/* Booleano che itentifica se la rete usa dei neuroni di bias */
		public bool Biased { get; private set; }

		/* Booleano che indica se il classificatore ritorna la confidenza 
			per ogni classe */
		public override bool HasConfidence
		{
			get { return true; }
		}

		#endregion

		/* Costruttore principale */
		public MyNeuralNetworkClassifier(InputLayer inputLayer, HiddenLayer hiddenLayer,
													OutputLayer outputLayer, Vector minFeatureValues,
													Vector maxFeatureValues, bool biased)
			 : base(outputLayer.Count) {
			/* Inizializzazione dei membri */
			this.inputLayer = inputLayer;
			this.hiddenLayer = hiddenLayer;
			this.outputLayer = outputLayer;
			this.minFeatureValues = minFeatureValues;
			this.maxFeatureValues = maxFeatureValues;
			Biased = biased;
		}

		/* Il metodo di classificazione senza confidenza */
		public override int Classify(FeatureVector v) {
			double[] unused;
			return Classify(v, out unused);
		}

		/* Il metodo di classificazione con confidenza */
		public override int Classify(FeatureVector v, out double[] confidence) {
			/* Normalizziamo il pattern da classificare */
			var normalizedVector = MyNeuralNetworkClassifierBuilder.ScaleFeatureVector(v, minFeatureValues, maxFeatureValues);

			/* Effettuiamo la propagazione in avanti del pattern */
			MyNeuralNetworkClassifierBuilder.ForwardPropagation(normalizedVector, inputLayer, hiddenLayer, outputLayer, Biased);

			/* Calcoliamo la confidenza */
			confidence = new double[ClassCount];
			double sumProb = 0;
			for (int i = 0; i < outputLayer.Count; i++)
				sumProb += outputLayer.Activation[i];

			/* Normalizziamo le probabilità perchè sommino ad 1 e prendiamo l'indice di 
			 * quella massima che corrisponde alla label predetta */
			int prediction = -1;
			double maxConfidence = -1.0;
			for (int i = 0; i < ClassCount; i++) {
				if (sumProb == 0)
					confidence[i] = 0;
				else
					confidence[i] = outputLayer.Activation[i] / sumProb;

				if (confidence[i] > maxConfidence) {
					maxConfidence = confidence[i];
					prediction = i;
				}
			}

			/* Ritornaimo la classe predizione */
			return prediction;
		}

		/* metodo per il salvataggio del modello */
		public override void SaveToStream(Stream stream) {
			throw new NotImplementedException();
		}

		/* Metodo per la clonazione */
		protected override Data InternalClone() {
			throw new NotImplementedException();
		}
	}

	/* Classe principale per la costruzione del classificatore */
	public class MyNeuralNetworkClassifierBuilder : ClassifierBuilder {
		[AlgorithmParameter]
		public double MaxError { get; set; }

		[AlgorithmParameter]
		public int MaxEpochs { get; set; }

		[AlgorithmParameter]
		public int HiddenUnitCount { get; set; }

		[AlgorithmParameter]
		public bool Biased { get; set; }

		[AlgorithmParameter]
		public double LearningRate { get; set; }

		[AlgorithmParameter]
		public int RandomSeed { get; set; }

		public MyNeuralNetworkClassifierBuilder() {
			/* Parametri di defaault */
			HiddenUnitCount = 10;
			MaxEpochs = 50000;
			MaxError = 0.1;
			LearningRate = 0.7;
			Biased = true;
			RandomSeed = 17;
		}

		public override string ToString() {
			return "My Neural Network Classifier";
		}

		/* Costruttore principale per la costruzione del classificatore */
		public override void Run() {
			InputLayer inputLayer;
			HiddenLayer hiddenLayer;
			OutputLayer outputLayer;
			Vector minFeatureValues;
			Vector maxFeatureValues;

			/* Addestriamo la rete */
			Training(
				 TrainingSet,
				 MaxError,
				 MaxEpochs,
				 HiddenUnitCount,
				 Biased,
				 LearningRate,
				 RandomSeed,
				 out inputLayer,
				 out hiddenLayer,
				 out outputLayer,
				 out minFeatureValues,
				 out maxFeatureValues
			);

			/* Creiamo il classificatore */
			Classifier = new MyNeuralNetworkClassifier(inputLayer, hiddenLayer, outputLayer, minFeatureValues, maxFeatureValues, Biased);
		}

		/* Metodo privato per l'addestramento della rete */
		private void Training(FeatureVectorSet trainingSet, double maxError, int maxEpochs, int hiddenUnitCount, bool biased, double learningRate, int randomSeed, out InputLayer inputLayer, out HiddenLayer hiddenLayer, out OutputLayer outputLayer, out Vector minFeatureValues, out Vector maxFeatureValues) {
			double error = 0;
			int epoch = 0;

			/* Normalizziamo i dati (scalati nell'intervallo [0, 1] */
			var normalizedDataset = DataNormalization(trainingSet, out minFeatureValues, out maxFeatureValues);

			/* Memorizziamo il numero di classi */
			var numClasses = normalizedDataset.FindMaxClassIndex() + 1;

			/* Creazione effettiva della rete */
			var rnd = new Random(randomSeed);
			inputLayer = new InputLayer(trainingSet.Dim, hiddenUnitCount, biased, rnd);
			hiddenLayer = new HiddenLayer(hiddenUnitCount, numClasses, biased, rnd);
			outputLayer = new OutputLayer(numClasses);

			/* Iterazioni di addestramento */
			do {
				/* Errori nell'iterazione corrente */
				error = 0;

				foreach (var v in normalizedDataset) {
					ForwardPropagation(v, inputLayer, hiddenLayer, outputLayer, biased);
					error += BackPropagation(inputLayer, hiddenLayer, outputLayer, biased, learningRate);
				}

				epoch++;

				/* Per la graficazione dei risultati intermedi */
				OnIntermediateResult(
					 new AlgorithmIntermediateResultEventArgs(
						  new MyNeuralNetworkClassifier(
								(InputLayer)inputLayer.Clone(),
								(HiddenLayer)hiddenLayer.Clone(),
								(OutputLayer)outputLayer.Clone(),
								(Vector)minFeatureValues.Clone(),
								(Vector)maxFeatureValues.Clone(),
								Biased
						  ),
						  string.Empty,
						  epoch
					)
				);

				/* Controllo della deviazione standard, se minore di una certa soglia fermo l'addestramento */
			} while (Math.Sqrt(error / trainingSet.Count) > maxError && epoch < maxEpochs);
		}

		/* Metodo statico per la normalizzazione dei dati */
		private static FeatureVectorSet DataNormalization(FeatureVectorSet vs, out Vector minFeatureValues, out Vector maxFeatureValues) {
			var normalized = new FeatureVectorSet();
			int nFeatures = vs.Dim;

			// prepara la normalizzazione
			minFeatureValues = new Vector(nFeatures);
			maxFeatureValues = new Vector(nFeatures);
			for (int i = 0; i < nFeatures; i++) {
				minFeatureValues[i] = double.MaxValue;
				maxFeatureValues[i] = double.MinValue;
			}

			foreach (var v in vs) {
				for (int k = 0; k < vs.Dim; k++) {
					if (v[k] < minFeatureValues[k]) {
						minFeatureValues[k] = v[k];
					}
					if (v[k] > maxFeatureValues[k]) {
						maxFeatureValues[k] = v[k];
					}
				}
			}

			for (int i = 0; i < vs.Count; i++) {
				normalized.Add(ScaleFeatureVector(vs[i], minFeatureValues, maxFeatureValues), false);
			}

			return normalized;
		}

		/* Metodo statico per la scalatura dei dati */
		internal static FeatureVector ScaleFeatureVector(FeatureVector vector, Vector minFeatureValues, Vector maxFeatureValues) {
			var fv = new FeatureVector(vector.Count);
			fv.Class = vector.Class;
			for (int i = 0; i < vector.Count; i++) {
				var diff = maxFeatureValues[i] - minFeatureValues[i];
				if (diff == 0) {
					fv[i] = 0;
				} else {
					fv[i] = (vector[i] - minFeatureValues[i]) / diff;
				}
			}
			return fv;
		}

		/* Propagazione in avanti */
		internal static void ForwardPropagation(FeatureVector pattern, InputLayer inputLayer, HiddenLayer hiddenLayer, OutputLayer outputLayer, bool biased) {

			/* Inizializziamo l'input layer con il FeatureVector pattern */
			for (int i = 0; i < inputLayer.Count; i++)
				inputLayer.Values[i] = pattern.Features[i];

			/* Settiamo il target, ossia il vettore con tutti 0 e 1 solo all'indice della label corretta*/
			for (int i = 0; i < outputLayer.Count; i++) {
				if (i == pattern.Class)
					outputLayer.Target[i] = 1.0;
				else
					outputLayer.Target[i] = 0.0;
			}

			/*-----Calcoliamo l'attivazione di ogni neurone nell'Hidden Layer-----*/
			for (int i = 0; i < hiddenLayer.Count; i++) {
				//Calcolo il net relativo al neurone i-esimo prima senza calcolare bias
				double weightedSum = 0.0;
				for (int j = 0; j < inputLayer.Count; j++)
					weightedSum += inputLayer.Values[j] * inputLayer.Weights[j, i];

				//Se abbiamo un neurone bias
				if (biased)
					weightedSum += inputLayer.BiasWeights[i] * inputLayer.Bias;

				//Agiungo il valore di net per questo neurone
				hiddenLayer.Net[i] = weightedSum;

				//Calcolo l'attivazione relativa al neurone i-esimo
				hiddenLayer.Activation[i] = ActivationFunction(weightedSum);
			}

			/*-----Calcoliamo infine il valore dei neuroni di Output-----*/
			for (int i = 0; i < outputLayer.Count; i++) {
				double weightedSum = 0.0;
				for (int j = 0; j < hiddenLayer.Count; j++)
					weightedSum += hiddenLayer.Activation[j] * hiddenLayer.Weights[j, i];

				if (biased)
					weightedSum += hiddenLayer.BiasWeights[i] * hiddenLayer.Bias;

				outputLayer.Net[i] = weightedSum;
				outputLayer.Activation[i] = ActivationFunction(weightedSum);
			}
		}

		/* Funzione di attivazione (Sigmoide) */
		private static double ActivationFunction(double net) {
			return 1 / (1 + Math.Exp(-net));
		}

		private static double BackPropagation(InputLayer inputLayer, HiddenLayer hiddenLayer, OutputLayer outputLayer, bool biased, double learningRate) {
			//Partiamo dall' Output layer per calcolare l'errore. Ricordiamo che le strutture dati, sono già state riempite in seguito alla chiamata della propagazione in avanti 
			double sumOutputErrors = 0;
			for (int i = 0; i < outputLayer.Count; i++) {
				//Calcolo l'errore
				outputLayer.Errors[i] = (outputLayer.Target[i] - outputLayer.Activation[i]) * outputLayer.Activation[i] * (1.0 - outputLayer.Activation[i]);

				//La somma totale degli errori non ci serve per la back-propagation ma solo per ritornarla al chiamante ed avere un idea di quanto la rete sta sbagliando
				sumOutputErrors += (outputLayer.Target[i] - outputLayer.Activation[i]) * (outputLayer.Target[i] - outputLayer.Activation[i]) / 2.0;
			}

			//Adesso possiamo passare al calcolo degli errori per l'Hidden-Layer 
			for (int i = 0; i < hiddenLayer.Count; i++) {
				hiddenLayer.Errors[i] = 0;

				for (int j = 0; j < outputLayer.Count; j++)
					hiddenLayer.Errors[i] += hiddenLayer.Weights[i, j] * outputLayer.Errors[j];

				hiddenLayer.Errors[i] *= hiddenLayer.Activation[i] * (1.0 - hiddenLayer.Activation[i]);
			}

			/*** Una volta calcolati tutti gli errori possiamo passare alla fase di aggiornamento dei pesi ***/

			//Aggiorniamo i pesi Hidden-Output in funzione dell'errore calcolato 
			for (int i = 0; i < outputLayer.Count; i++) {
				for (int j = 0; j < hiddenLayer.Count; j++)
					hiddenLayer.Weights[j, i] += learningRate * outputLayer.Errors[i] * hiddenLayer.Activation[j];

				if (biased)
					hiddenLayer.BiasWeights[i] += learningRate * outputLayer.Errors[i] * hiddenLayer.Bias;
			}

			//Aggiorniamo i pesi Input-Hidden in funzione dell'errore calcolato 
			for (int i = 0; i < hiddenLayer.Count; i++) {
				for (int j = 0; j < inputLayer.Count; j++)
					inputLayer.Weights[j, i] += learningRate * hiddenLayer.Errors[i] * inputLayer.Values[j];

				if (biased)
					inputLayer.BiasWeights[i] += learningRate * hiddenLayer.Errors[i] * inputLayer.Bias;
			}

			//Ritorniamo la somma degli errori nell'Output Layer 
			return sumOutputErrors;
		}
	}
}