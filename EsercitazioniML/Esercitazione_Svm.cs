﻿using BioLab.Common;
using BioLab.Math.LinearAlgebra;
using System;
using BioLab.Classification.Supervised;
using System.IO;
using BioLab.DimensionalityReduction;
using System.Collections.Generic;
using System.Globalization;

namespace PRLab.EsercitazioniML
{
    /* Questa classe estende il costrutture del classificatore SVM presente nella libreria
   * Biolab. Durante l'esercitazione sarà necessario implementare i metodi non ancora 
   * implementati o incompleti che per adesso lanciano l'eccezione NotImplemented.
   */
    public class SvmUtilities
    {
        /* Dizionario da utilizzare per comodità per passare agevolmente dall classe
         * in formato stringa al suo corrispondente intero 
         */
        public static Dictionary<string, int> ClassString2Int = new Dictionary<string, int>();

        /* Dizionario da utilizzare per comodità per passare agevolmente dall'indice di
         * un attributo al suo nome in formato stringa. 
         */
        public static Dictionary<int, String> AttrNum2String = new Dictionary<int, String>();

        /* Metodo già implementato per la riduzione delle dimensionalità. Esso prende in 
         * ingresso un FeatureVectorSet e riduce le dimensionalità di ogni FeatureVector
         * al suo interno a [new_dim] dimensioni. Ritorna il nuovo set con le dimensioni 
         * ridotte.
         */
        public static FeatureVectorSet ReduceDimentionality(FeatureVectorSet set, int new_dim)
        {
            PcaTransformBuilder builder = new PcaTransformBuilder(set, new_dim);
            var pcaTranform = builder.Calculate();
            var newfeatVecSet = pcaTranform.Reduce(set);
            newfeatVecSet.Description = "PCA(2 dim)";
            return newfeatVecSet;
        }


        /* Metodo già implementato per la normalizzazioe di una lista di FeatureVectorSet 
         * (train, val e test set). Ritorna la stessa lista di FeatureVectorSet con pattern
         * scalati tra -1 e 1. 
         */
        public static List<FeatureVectorSet> NormalizeData(List<FeatureVectorSet> dataset_splits)
        {
            /* Recuperiamo i valori max e min sul Training set per ogni dimensione
             * per poter scalare i pattern tra  -1 e 1 */
            var dim = dataset_splits[0][0].Features.Count;
            var max = new double[dim];
            var min = new double[dim];
            for (int i = 0; i < dim; i++)
            {
                max[i] = 0.0;
                min[i] = Double.MaxValue;
            }

            foreach (var pattern in dataset_splits[0])
            {
                for (int i = 0; i < dim; i++)
                {
                    if (pattern.Features[i] > max[i])
                        max[i] = pattern.Features[i];
                    if (pattern.Features[i] < min[i])
                        min[i] = pattern.Features[i];
                }

            }

            /* Scaliamo tutti i valori del dataset con i valori Min e Max
             * recuperati dal training set
             */
            var norm_train = new FeatureVectorSet();
            var norm_val = new FeatureVectorSet();
            var norm_test = new FeatureVectorSet();

            var norm_splits = new List<FeatureVectorSet>();

            norm_train.Description = dataset_splits[0].Description;
            norm_val.Description = dataset_splits[1].Description;
            norm_test.Description = dataset_splits[2].Description;

            norm_splits.Add(norm_train);
            norm_splits.Add(norm_val);
            norm_splits.Add(norm_test);

            for (int c = 0; c < dataset_splits.Count; c++)
            {
                foreach (var pattern in dataset_splits[c])
                {
                    var norm_pattern = new double[dim];
                    for (int i = 0; i < dim; i++)
                    {
                        var scaled_val = (pattern.Features[i] - min[i]) /
                            (max[i] - min[i]);
                        norm_pattern[i] = 2 * scaled_val - 1;
                    }
                    norm_splits[c].Add(new FeatureVector(norm_pattern, pattern.Class), true);

                }
            }

            /* ritorniamo la nuova lista di FeatureVectorSet */
            return norm_splits;

        }
        /* Metodo già implemntato per la divisione di un FeatureVectorSet in train val e test set.
         * Ritorna una lista di FeatureVectorSet con due elementi. 
         */
        public static List<FeatureVectorSet> SplitDataset(FeatureVectorSet dataset)
        {
            var splits = new List<FeatureVectorSet>();
            var classes = new List<FeatureVectorSet>();
            var train = new FeatureVectorSet();
            var val = new FeatureVectorSet();
            var test = new FeatureVectorSet();

            train.Description = "Tr. " + dataset.Description;
            val.Description = "Va. " + dataset.Description;
            test.Description = "Te. " + dataset.Description;

            splits.Add(train);
            splits.Add(val);
            splits.Add(test);

            /* Si effettua innanzitutto al suddivisione delle classi */
            for (int i = 0; i < 3; i++)
            {
                classes.Add(new FeatureVectorSet());
            }

            for (int i = 0; i < dataset.Count; i++)
            {
                if (i >= 0 && i < 50)
                    classes[0].Add(dataset[i], true);
                else if (i >= 50 && i < 100)
                    classes[1].Add(dataset[i], true);
                else
                    classes[2].Add(dataset[i], true);
            }

            /* A partire dalla classi selezioniamo un numero di pattern
             * prefissato per Train, Validation e Test set */
            foreach (var clas in classes)
            {
                for (int i = 0; i < clas.Count; i++)
                {
                    if (i >= 0 && i < 20)
                        splits[0].Add(clas[i], true);
                    else if (i >= 20 && i < 35)
                        splits[1].Add(clas[i], true);
                    else
                        splits[2].Add(clas[i], true);
                }
            }

            /* Commentare questa linea di codice per evitare la normalizzazione
             * Non realmente necessaria per il nostro Dataset di esempio */
            //splits = NormalizeData(splits);

            return splits;
        }

        /* Questo metodo prende in imput una list a di stringhe costituente il dataset e 
         * un lista di indici interi che rappresentano gli attributi da selezionare ed 
         * una descrizione da aggiungere al FeatureVectorSet (importante per la GUI).
         */
        public static FeatureVectorSet Convert2FeatureVectorSet(List<String> dataset, List<Int32> indexes,
                                                    String Desc)
        {

            FeatureVectorSet new_dataset = new FeatureVectorSet();
            new_dataset.Description = Desc;

            foreach (String line in dataset)
            {
                Vector app = new Vector(indexes.Count);
                String[] substrings = line.Split(',');
                for (int i = 0; i < indexes.Count; i++)
                    app[i] = double.Parse(substrings[indexes[i]], CultureInfo.InvariantCulture);
                FeatureVector example = new FeatureVector(
                    app,
                    ClassString2Int[substrings[4]] //class
                );
                new_dataset.Add(example, true);
            }

            return new_dataset;
        }

        /* Questo metodo viene invocato direttamente dalla GUI per recuperare i FeatureVectorSet(s)
         * da utilizzare per la classificazione. Non prende in ingresso nulla e ritorna una lista
         * di FeatureVectorSet.
         */
        public static void convertDataset2fvstxt()
        {
            /** PARAMETRI DA UTILIZZARE **/
            string bp = @"C:\Users\Vincenzo\Desktop\Machine Learning\";
            ClassString2Int.Add("Iris-setosa", 0);
            ClassString2Int.Add("Iris-versicolor", 1);
            ClassString2Int.Add("Iris-virginica", 2);
            AttrNum2String.Add(0, "sepal_l");
            AttrNum2String.Add(1, "sepal_w");
            AttrNum2String.Add(2, "petal_l");
            AttrNum2String.Add(3, "petal_w");

            /* Lettura del file csv */
            List<string> dataset = new List<string>(
                File.ReadAllLines(bp + @"iris_dataset\iris.data")
            );

            /* Si rimuove l'ultima riga che è costituita solo da un a capo */
            dataset.RemoveAt(dataset.Count - 1);

            List<FeatureVectorSet> set_list = new List<FeatureVectorSet>();
            List<List<int>> indexesCouples = new List<List<int>>();

            /* Ricorda: 0 = sepal length, 1 = sepal width, 2 petal length, 3 = petal width.
             * In qursto caso per completezza della soluzione tutte le possibili
             * coppie sono inserite. */
            indexesCouples.Add(new List<int> { 1, 2 });
            indexesCouples.Add(new List<int> { 0, 2 });
            indexesCouples.Add(new List<int> { 2, 3 });

            /* I precedenti sono i più discriminanti, per produrre gli altri 
             * decommentare le linee sotto 
            indexesCouples.Add(new List<int> { 0, 1 });
            indexesCouples.Add(new List<int> { 0, 3 });
            indexesCouples.Add(new List<int> { 1, 3 });
            */

            /* Per ogni coppia di indici recuperiamo il FeatureVectorSet e lo dividiamo in train
             * e test set. */
            foreach (var couple in indexesCouples)
            {
                var FeatSet = Convert2FeatureVectorSet(dataset, couple,
                        AttrNum2String[couple[0]] + " vs " + AttrNum2String[couple[1]]);
                var split_list = SplitDataset(FeatSet);

                foreach (var set in split_list)
                {
                    set_list.Add(set);
                }
            }

            /* Si crea e aggiunge alla lista da ritornare train e test set del FeatureVectorSet
             * con tutti e 4 gli attributi. */
            var FeatsSet = Convert2FeatureVectorSet(dataset, new List<int>(
                            new int[] { 0, 1, 2, 3 }),
                            "All feat"
            );

            var splits = SplitDataset(FeatsSet);
            foreach (var set in splits)
            {
                set_list.Add(set);
            }

            /* Si aggiungono train e test set del FeatureVectorSet con le quattro dimensioni 
             * ridotto a due. */
            splits = SplitDataset(ReduceDimentionality(FeatsSet, 2));
            foreach (var set in splits)
            {
                set_list.Add(set);
            }

            /* infine scriviamo i FeaturevectorSet sul file, in formato testuale*/
            foreach (var set in set_list)
            {
                set.SaveToTextFile(set.Description + ".fvstxt");
            }

        }

    }

    public static class MySvmParameterOptimizer
    {
        public static void Execute(string dbBaseFolderPath,string dbName,out double errValidation, out double errTest, out string optimizedParameters)
        {
            /* carichiamo i FeatureVectorSet di Train, Val e Test */
            var train = FeatureVectorSet.LoadFromTextFile(Path.Combine(dbBaseFolderPath, "Tr. " + dbName + ".fvstxt"));
            var val = FeatureVectorSet.LoadFromTextFile(Path.Combine(dbBaseFolderPath, "Va. " + dbName + ".fvstxt"));
            var test = FeatureVectorSet.LoadFromTextFile(Path.Combine(dbBaseFolderPath, "Te. " + dbName + ".fvstxt"));
            
            /* Settiamo i parametri iniziali dell' SVM */
            var svm_builder = new SvmClassifierBuilder();
            svm_builder.Gamma = 1.0 / train.Dim;
            svm_builder.KernelType = KernelType.RadialBasisFunction;
            
            /* Cerchiamo il parametro di costo che minimizza l'err sul validation */
            double minErr = double.MaxValue;
            double bestCost = 0;
            Classifier bestSvmClassifier=null;
            var values = new double[] {0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.25,1.5,1.75,2,3,4,5,10,15,20,25,50,100};
                        
            /* Per ognuno dei valori da testare dobbiamo addestrare il classificatore e
             * recuperare l'errore sul validation set. Durante il ciclo teniamo traccia del valore 
             * che ha ottenendo l'errore più basso. 
             */
            throw new NotImplementedException();

            /* Infine valutiamo l'accuratezza sul test set con
             * il migliore parametro di costo trovato */
            throw new NotImplementedException();

            /* Riempiamo la stringa da visualizzare a video */
            optimizedParameters = "Gamma = " + svm_builder.Gamma
                                  + "\nCost = " + bestCost;
        }
    }
}