﻿
using BioLab.Common;
using System;

namespace PRLab.EsercitazioniML {
	class MyEsercitazioneIntroduttiva {
		private double[][] centers = new double[][]
											 {
													 new double[] {0.25,0.25},
													 new double[] {0.75,0.25},
													 new double[] {0.25,0.75},
													 new double[] {0.75,0.75},
											 };

		private double[] maxDistanceFromCenters = new double[] { 0.1, 0.1, 0.1, 0.1 };

		private int[] patternCounts = new int[] { 10, 15, 20, 25 };

		private int[] randomSeeds = new int[] { 1, 2, 3, 4 };

		public FeatureVectorSet Generate() {
			var featureVectorSet = new FeatureVectorSet();

			for (int i = 0; i < centers.Length; i++) {
				var generator = new MyRandomDistributionGenerator(centers[i], maxDistanceFromCenters[i], randomSeeds[i]);
				featureVectorSet.Add(generator.Generate(patternCounts[i], i), false);
			}
			return featureVectorSet;
		}
	}

	/* Questa classe ha il compito di restituire un FeatureVectorSet contenente una distribuzione di pattern generati casualmente */
	class MyRandomDistributionGenerator : Algorithm {
		private const int maxIterationCountPerPattern = 1000;

		private int randomSeed;
		private Random randomGenerator;

		[AlgorithmInput]
		public double[] Center { get; set; }

		[AlgorithmInput]
		public double MaxDistanceFromCenter { get; set; }

		[AlgorithmInput]
		public int RandomSeed
		{
			get { return randomSeed; }
			set
			{
				randomSeed = value;
				randomGenerator = new Random(randomSeed);
			}
		}

		[AlgorithmParameter]
		public int PatternCount { get; set; }

		[AlgorithmParameter]
		public int ClassId { get; set; }

		[AlgorithmOutput]
		public FeatureVectorSet Patterns { get; private set; }

		public MyRandomDistributionGenerator(double[] center, double maxDistanceFromCenter, int randomSeed) {
			Center = center;
			MaxDistanceFromCenter = maxDistanceFromCenter;
			RandomSeed = randomSeed;
			ClassId = -1;
		}

		public FeatureVectorSet Generate(int patternCount, int classId) {
			PatternCount = patternCount;
			ClassId = classId;
			Run();
			return Patterns;
		}

		public override void Run() {
			Patterns = new FeatureVectorSet();
			var squareMaxDistance = MaxDistanceFromCenter * MaxDistanceFromCenter;
			var twoMaxDistance = 2 * MaxDistanceFromCenter;

			//Per ogni elemento che vado ad inserire
			for (int i = 0; i < PatternCount; i++) {
				//creo un pattern vuoto
				double[] pattern = null;

				//variabile per tenere conto dei tentativi
				var iterationCount = 0;

				//Fino a che non raggiungo il massimo numero di tentativi
				while (iterationCount < maxIterationCountPerPattern) {
					//Creo le coordinate casuali del pattern
					var dx = (randomGenerator.NextDouble() * twoMaxDistance) - MaxDistanceFromCenter;
					var dy = (randomGenerator.NextDouble() * twoMaxDistance) - MaxDistanceFromCenter;

					//Se sono accettabili
					if ((dx * dx + dy * dy) <= squareMaxDistance) {
						//Setto il nuovo pattern
						pattern = new double[2];
						pattern[0] = Center[0] + dx;
						pattern[1] = Center[1] + dy;
						break;
					}

					//Aggiungo una iterazione
					iterationCount++;
				}

				//Se il apttern è rimasto a null, raggiungo il massimo numero di tentativi
				if (pattern == null) {
					throw new Exception("Raggiunto il massimo numero di iterazioni.");
				}

				//creo un vettore di feature
				var fv = new FeatureVector(pattern, false);
				fv.Class = ClassId;

				//Lo aggiungo al set
				Patterns.Add(fv, false);
			}
		}
	}
}