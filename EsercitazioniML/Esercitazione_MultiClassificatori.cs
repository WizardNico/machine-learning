﻿
using BioLab.Classification.Supervised;
using System;
using BioLab.Common;
using System.Text;
using System.IO;

namespace PRLab.EsercitazioniML {
	/// <summary>
	/// Implementation of multi-classifier system with decision-level fusion
	/// and majority-vote rule.
	/// </summary>
	public class MyMcsMajorVote : MultiClassifier {
		/// <summary>
		/// Initializes a new instance of the <see cref="MyMcsMajorVote"/> class.
		/// </summary>
		/// <param name="classifiers">The classifiers to merge.</param>
		public MyMcsMajorVote(Classifier[] classifiers)
			 : base(classifiers) { }

		/// <summary>
		/// Classifies a feature vector with respect to the most voted class. 
		/// </summary>
		/// <param name="vector">The vector to be classified.</param>
		/// <returns>
		/// [0..classesCount-1] or -1 (unknown class)
		/// </returns>
		public override int Classify(FeatureVector vector) {
			//Creo la lista di voti
			int[] votes = new int[ClassCount];

			//Classifico e calcolo le confidenze
			foreach (Classifier classifier in Classifiers)
				votes[classifier.Classify(vector)]++;

			int max = 0;
			int voteMax = 0;

			//Trovo il massimo
			for (int i = 0; i < ClassCount; i++)
				if (votes[i] > voteMax)
					voteMax = votes[max = i];

			return max;
		}

		/// <summary>
		/// Saves the current <see cref="BioLab.Common.Data"/> to a specified <see cref="System.IO.Stream"/>.
		/// </summary>
		/// <param name="stream">The <see cref="System.IO.Stream"/> where the current <see cref="BioLab.Common.Data"/> will be saved.</param>
		public override void SaveToStream(Stream stream) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Creates an exact copy of this instance.
		/// </summary>
		/// <returns>
		/// The <see cref="BioLab.Common.Data"/> this method creates.
		/// </returns>
		protected override Data InternalClone() {
			throw new NotImplementedException();
		}
	}

	/// <summary>
	/// Implementation of a multi-classifier system with confidence-level fusion.
	/// </summary>
	/// <remarks>Assumes no normalization in classifiers confidences.</remarks>
	public class MyMcsConfidence : MultiClassifier {
		/// <summary>
		/// Initializes a new instance of the <see cref="MyMcsConfidence"/> class.
		/// </summary>
		/// <param name="classifiers">The classifiers to merge.</param>
		/// <param name="fusionRule">The fusion rule.</param>
		public MyMcsConfidence(Classifier[] classifiers, ClassifierFusionMethod fusionRule)
			 : base(classifiers) {
			// Verify that every classifier support confidence.
			foreach (Classifier c in Classifiers) {
				if (!c.HasConfidence) {
					throw new ArgumentException("All the classifiers must support confidence.");
				}
			}

			FusionRule = fusionRule;
		}

		/// <summary>
		/// Gets or sets the fusion rule.
		/// </summary>
		/// <value>The fusion rule.</value>
		public ClassifierFusionMethod FusionRule { get; set; }

		/// <summary>
		/// Classifies a feature vector.
		/// </summary>
		/// <param name="vector">The vector to be classified.</param>
		/// <returns>
		/// [0..classesCount-1] or -1 (unknown class)
		/// </returns>
		public override int Classify(FeatureVector vector) {
			//Start classification with the first classifier for confidence vector initialization
			//Then classify with other classifiers
			double[] combinedConf;
			Classifiers[0].Classify(vector, out combinedConf);

			for (int iCl = 1; iCl < Classifiers.Length; iCl++) {
				//Classify with the current classifier and get the confidence for each classes
				double[] confidence;
				Classifiers[iCl].Classify(vector, out confidence);

				//Apply the fusion rule between confidences
				switch (FusionRule) {
					case ClassifierFusionMethod.Sum:
						for (int i = 0; i < ClassCount; i++)
							combinedConf[i] += confidence[i];
						break;

					case ClassifierFusionMethod.Product:
						for (int i = 0; i < ClassCount; i++)
							combinedConf[i] *= confidence[i];
						break;

					case ClassifierFusionMethod.Max:
						for (int i = 0; i < ClassCount; i++)
							if (confidence[i] > combinedConf[i])
								combinedConf[i] = confidence[i];
						break;

					case ClassifierFusionMethod.Min:
						for (int i = 0; i < ClassCount; i++)
							if (confidence[i] < combinedConf[i])
								combinedConf[i] = confidence[i];
						break;
				}
			}

			//Trovo la confidenza massima
			int max = 0;
			double sMax = combinedConf[0];
			for (int i = 1; i < ClassCount; i++) {
				if (combinedConf[i] > sMax) {
					sMax = combinedConf[max = i];
				}
			}
			return max;
		}

		/// <summary>
		/// Saves the current <see cref="BioLab.Common.Data"/> to a specified <see cref="System.IO.Stream"/>.
		/// </summary>
		/// <param name="stream">The <see cref="System.IO.Stream"/> where the current <see cref="BioLab.Common.Data"/> will be saved.</param>
		public override void SaveToStream(Stream stream) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Creates an exact copy of this instance.
		/// </summary>
		/// <returns>
		/// The <see cref="BioLab.Common.Data"/> this method creates.
		/// </returns>
		protected override Data InternalClone() {
			throw new NotImplementedException();
		}
	}

	/// <summary>
	/// Builder for a multi-classifier system with decision-level fusion and majority-vote rule.
	/// </summary>
	public class MyMcsMajorVoteBuilder : ClassifierBuilder {
		/// <summary>
		/// Gets or sets the classifier builders used to create the multi-classifier.
		/// </summary>
		[AlgorithmInput]
		public ClassifierBuilder[] ClassifierBuilders { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="MyMcsMajorVoteBuilder"/> class.
		/// </summary>
		/// <param name="classifierBuilders">The classifier builders to use in the creation of the <see cref="MyMcsMajorVoteBuilder"/>.</param>
		public MyMcsMajorVoteBuilder(ClassifierBuilder[] classifierBuilders) {
			ClassifierBuilders = (ClassifierBuilder[])classifierBuilders.Clone(); // copia i riferimenti dei builder in un nuovo array
		}

		/// <summary>
		/// Executes the Training of the different classifiers and return the multi-classifier.
		/// </summary> 
		public override void Run() {
			var classifiers = new Classifier[ClassifierBuilders.Length];
			for (int i = 0; i < ClassifierBuilders.Length; i++) {
				classifiers[i] = ClassifierBuilders[i].Train(TrainingSet);
			}

			Classifier = new MyMcsMajorVote(classifiers);
		}

		/// <summary>
		/// Returns a string that represents the current <see cref="McsMajorVoteBuilder"/>.
		/// </summary>
		/// <returns></returns>
		public override string ToString() {
			var strBuilder = new StringBuilder();
			strBuilder.Append("MyMcsMajorVote (");
			for (int i = 0; i < ClassifierBuilders.Length; i++) {
				strBuilder.Append((i > 0 ? "," : string.Empty) + ClassifierBuilders[i].ToString());
			}
			strBuilder.Append(")");

			return strBuilder.ToString();
		}
	}

	/// <summary>
	/// Builder for a multi-classifier system with confidence-level fusion.
	/// </summary>
	public class MyMcsConfidenceBuilder : ClassifierBuilder {
		/// <summary>
		/// Gets or sets the classifier builders used to create the multi-classifier.
		/// </summary>
		[AlgorithmInput]
		public ClassifierBuilder[] ClassifierBuilders { get; set; }

		/// <summary>
		/// Gets or sets the fusion rule.
		/// </summary>
		/// <value>The fusion rule.</value>
		[AlgorithmInput]
		public ClassifierFusionMethod FusionRule { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="MyMcsConfidenceBuilder"/> class.
		/// </summary>
		/// <param name="classifierBuilders">The classifier builders to use in the creation of the <see cref="MyMcsConfidenceBuilder"/>.</param>
		/// <param name="fusionRule">The fusion rule.</param>
		public MyMcsConfidenceBuilder(ClassifierBuilder[] classifierBuilders, ClassifierFusionMethod fusionRule) {
			ClassifierBuilders = (ClassifierBuilder[])classifierBuilders.Clone(); // copia i riferimenti dei builder in un nuovo array

			FusionRule = fusionRule;
		}

		/// <summary>
		/// Executes the Training of the different classifiers and return the multi-classifier.
		/// </summary> 
		public override void Run() {
			var classifiers = new Classifier[ClassifierBuilders.Length];
			for (int i = 0; i < ClassifierBuilders.Length; i++) {
				classifiers[i] = ClassifierBuilders[i].Train(TrainingSet);
			}

			Classifier = new MyMcsConfidence(classifiers, FusionRule);
		}

		/// <summary>
		/// Returns a string that represents the current <see cref="McsConfidenceBuilder"/>.
		/// </summary>
		/// <returns></returns>
		public override string ToString() {
			var strBuilder = new StringBuilder();
			strBuilder.AppendFormat("MyMcsConfidence[{0}] (", FusionRule);
			for (int i = 0; i < ClassifierBuilders.Length; i++) {
				strBuilder.Append((i > 0 ? "," : string.Empty) + ClassifierBuilders[i].ToString());
			}
			strBuilder.Append(")");

			return strBuilder.ToString();
		}
	}
}