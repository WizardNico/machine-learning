﻿
using BioLab.Classification.Unsupervised;
using BioLab.Common;
using System;
using BioLab.Math.LinearAlgebra;

namespace PRLab.EsercitazioniML {
	/// <summary>
	/// Represents a k-Means clustering algorithm.
	/// </summary>
	[AlgorithmInfo("MyK-Means")]
	public class MyKMeansClustering : IterativeClusterizer {
		/// <summary>
		/// Returns the sum of square distances of each vector to the corresponding centroid.
		/// </summary>
		[AlgorithmOutput]
		public double SumSquareDistances { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="KMeanClustering"/> class.
		/// </summary>
		/// <remarks>Note that the input parameters must be initialized before running the algorithm.</remarks>
		public MyKMeansClustering() {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="KMeanClustering"/> class and allows to specify the vector set and the number of clusters.
		/// </summary>
		/// <param name="vectorSet">The set of vectors to be clusterized.</param>
		/// <param name="clusterCount">The number of clusters to be created.</param>
		public MyKMeansClustering(FeatureVectorSet vectorSet, int clusterCount)
			 : base(vectorSet) {
			ClusterCount = clusterCount;
		}


		/// <summary>
		/// This method is called before the first iteration, to initialize the data structures and other internal parameters of the algorithm.
		/// </summary>
		protected override void Initialize() {
			centroids = new Vector[ClusterCount];

			SumSquareDistances = double.NaN;

			// inizializza i centri, per semplicità sceglie vettori random in vectorSet
			var rnd = new Random();

			int m = VectorSet.Count / ClusterCount;

			if (m == 0) {
				throw new ArgumentException("Insufficient number of vectors.");
			}

			for (int i = 0; i < ClusterCount; i++) {
				int index = i * m + rnd.Next(m); // in modo da non sceglierne due uguali
				centroids[i] = VectorSet[index].Features.Clone();
			}

			AssignVectorsToCentroids(); // Esegue il primo assegnamento
		}

		/// <summary>
		/// Executes one iteration of the algorithm.
		/// </summary>
		/// <returns>
		/// true if the algorithm converged, false otherwise.
		/// </returns>
		protected override bool Iteration() {
			int dim = VectorSet.Dim;
			int s = ClusterCount;
			int n = VectorSet.Count;


			// Ricalcola i centri e li inizializzo tutti a zero
			for (int i = 0; i < s; i++) {
				centroids[i] = new Vector(dim);
			}

			int[] numPerCluster = new int[s];//Per tenere conto di quanti pattern ho nel cluster
			for (int i = 0; i < n; i++) {
				centroids[VectorSet[i].Class] += VectorSet[i].Features;//Faccio la sommatoria
				numPerCluster[VectorSet[i].Class]++;//Mi segno quanti punti ho nel mio cluster in esame
			}

			//Ora per ogni cluster
			for (int i = 0; i < s; i++) {
				if (numPerCluster[i] > 0) {
					centroids[i] /= numPerCluster[i];//Normalizzo il calcolo e assegno i nuovi centroidi
				}
			}
			// Aggiorna i vettori
			return !AssignVectorsToCentroids();
		}

		/// <summary>
		/// Gets the current centroid of a given cluster.
		/// </summary>
		/// <param name="clusterIndex">Index of the cluster.</param>
		/// <returns>The <see cref="FeatureVector"/> corresponding to the centroid.</returns>
		public Vector GetCentroid(int clusterIndex) {
			return (Vector)centroids[clusterIndex].Clone();
		}

		#region Membri privati
		Vector[] centroids;

		/// <summary>
		/// Riassegna i vettori ai cluster in base alla minima distanza dai centri
		/// </summary>
		/// <returns>true se almeno un vettore è stato assegnato a un diverso cluster</returns>
		private bool AssignVectorsToCentroids() {
			var clustersModified = false;
			var sumSquareDistances = 0.0;

			//Scorro tutti i pattern
			foreach (var v in VectorSet) {
				var dMinQ = double.MaxValue;//Distanza minima, inizialmente settata al massimo
				int iMin = -1;//Indice relativo alla distanza minima

				//Scorro tutti i centroidi
				for (int i = 0; i < centroids.Length; i++) {
					double d = Vector.ComputeSquareEuclideanDistance(v.Features, centroids[i]);//Calcolo la distanza euclidea

					//Se ottengo un risultato migliore
					if (d < dMinQ) {
						dMinQ = d;//Salvo la distanza
						iMin = i;//Salvo l'indice
					}
				}

				//Se l'indice del cluster ottimo è diverso da quello attuale
				if (iMin != v.Class) {
					v.Class = iMin;//Modifico
					clustersModified = true;//Segnalo che è stato attuato un cambiamento
				}
				sumSquareDistances += dMinQ;//Sommo la distanza
			}
			SumSquareDistances = sumSquareDistances;//Setto la distanza precedentemente calcolata

			return clustersModified;
		}
		#endregion


		/// <summary>
		/// Gets or sets the number of clusters to be created by the algorithm.
		/// </summary>
		/// <value>The number of clusters.</value>
		[AlgorithmParameter]
		public int ClusterCount { get; set; }
	}
}