﻿
using BioLab.Classification.DensityEstimation;
using BioLab.Common;
using BioLab.Math.LinearAlgebra;
using System.Diagnostics;
using System;
using BioLab.Classification.Supervised;
using System.IO;
using BioLab.Math;
using System.Collections.Generic;

namespace PRLab.EsercitazioniML {
	/// <summary>
	/// Represents a Normal Distribution
	/// </summary>
	public class MyNormalDistribution : IProbabilityDensity {
		/// <summary>
		/// Mean vector
		/// </summary>
		public Vector Mean { get; set; }

		/// <summary>
		/// Covariance matrix
		/// </summary>
		public Matrix Covariance { get; set; }

		/// <summary>
		/// True if the covariance matrix became singular
		/// </summary>
		/// <value><c>true</c> if the distribution is invalid; otherwise, <c>false</c>.</value>
		public bool Invalid { get; set; }

		// Inverse of covariance matrix
		private Matrix invΣ;

		//Normalization factor
		private double normFactor;

		public MyNormalDistribution(Vector[] classTrainingSet) {
			/*-------- STIMA DEL VETTORE MEDIA --------*/
			Mean = new Vector(classTrainingSet[0].Count);
			Double count = 0;

			//Per ogni elemento del vettore
			for (int i = 0; i < classTrainingSet[0].Count; i++) {
				count = 0;
				//Per ogni vettore
				foreach (Vector train in classTrainingSet) {
					//Sommo tutti gli elementi sulla i-esima colonna
					count += train[i];
				}
				//Faccio la media e aggiungo sul vettore
				Mean[i] = (count / classTrainingSet.Length);
			}

			/*-------- STIMA DELLA MATRICE COVARIANZA --------*/
			Covariance = new Matrix(Mean.Count);
			for (int i = 0; i < Covariance.RowCount; i++) {
				for (int j = 0; j < Covariance.ColumnCount; j++) {
					count = 0;
					for (int k = 0; k < classTrainingSet.Length; k++) {
						count += (classTrainingSet[k][i] - Mean[i]) * (classTrainingSet[k][j] - Mean[j]);
					}
					Covariance[i, j] = count / classTrainingSet.Length;
				}
			}

			PreCalculateData();
		}

		private Matrix transformInColumn(Vector x) {
			var col = new Matrix(x.Count, 1);
			for (int i = 0; i < x.Count; i++) {
				col[i, 0] = x[i];
			}
			return col;
		}

		/// <summary>
		/// Compute the probability density
		/// </summary>
		public double CalculateDensity(Vector x) {
			if (Invalid) {
				return 0;
			}
			return normFactor * Math.Exp((-0.5) * (invΣ * (x - Mean) * (transformInColumn(x) - transformInColumn(Mean)))[0]);
		}

		/// <summary>
		/// To call if mean or covariance are modified
		/// </summary>
		public void PreCalculateData() {
			double determinant = Covariance.ComputeDeterminant();
			if (determinant <= double.Epsilon) {
				Invalid = true;
			} else {
				invΣ = Covariance.ComputeInverse();
				Debug.Assert(!double.IsNaN(invΣ[0, 0]));
				normFactor = 1.0 / (System.Math.Sqrt(System.Math.Pow(Constants.DoublePI, Mean.Count) * determinant));
				Invalid = false;
			}
		}

		public double CalculateDensity(FeatureVector vector) {
			return CalculateDensity(vector.Features);
		}

		public void SaveToStream(Stream stream) {
			throw new NotImplementedException();
		}

		public object Clone() {
			throw new NotImplementedException();
		}
	}

	/// <summary>
	/// Parametric maximum likelihood estimation, assuming normal distributions.
	/// </summary>
	public class MyNormalMLEstimator : DensityEstimator {
		/// <summary>
		/// Initializes a new instance of the MyNormalMLEstimator class.
		/// </summary>
		public MyNormalMLEstimator() {
		}

		/// <summary>
		/// Initializes a new instance of the MyNormalMLEstimator class.
		/// </summary>
		/// <param name="trainingSet">The training set.</param>
		public MyNormalMLEstimator(FeatureVectorSet trainingSet)
			 : base(trainingSet) {
		}

		/// <summary>
		/// Executes the estimation on the training set.
		/// </summary>
		public override void Run() {
			var classCount = TrainingSet.FindMaxClassIndex() + 1;
			if (classCount == 0)
				classCount = 1;

			//Split training vectors per class
			var classTrainingSets = new List<Vector>[classCount];
			for (int i = 0; i < classCount; i++) {
				classTrainingSets[i] = new List<Vector>();
			}
			foreach (var fv in TrainingSet) {
				int index = fv.Class;
				if (index < 0) // unique class
					index = 0;

				classTrainingSets[index].Add(fv.Features);
			}

			ClassConditionalDensities = new MyNormalDistribution[classCount];
			for (int i = 0; i < classCount; i++) {
				ClassConditionalDensities[i] = new MyNormalDistribution(classTrainingSets[i].ToArray());
			}

			var classifier = new MyBayesClassifier(ClassConditionalDensities);
			foreach (var fv in TrainingSet)
				classifier.Classify(fv);
		}

		public override string ToString() {
			return "MyNormal";
		}
	}

	/// <summary>
	/// Represents a classifier based on the Bayes Classification Rule 
	/// </summary>
	public class MyBayesClassifier : Classifier {
		private IProbabilityDensity[] classConditionalDensities;
		private double[] priorProbabilities;

		/// <summary>
		/// Initializes a new instance of the MyBayesClassifier class.
		/// </summary>
		/// <param name="classConditionalDensities">The class conditional density for each class.</param>
		/// <param name="priorProbabilities">The prior probabilitie for each class.</param>
		public MyBayesClassifier(IProbabilityDensity[] classConditionalDensities, double[] priorProbabilities)
			 : base(classConditionalDensities.Length) {
			this.classConditionalDensities = new IProbabilityDensity[classConditionalDensities.Length];
			for (int i = 0; i < classConditionalDensities.Length; i++) {
				this.classConditionalDensities[i] = classConditionalDensities[i];
			}

			this.priorProbabilities = (double[])priorProbabilities.Clone();
		}

		/// <summary>
		/// Initializes a new instance of the MyBayesClassifier class. The class conditional
		/// densities are given by the user. The priors are computed as fractions of examples of training 
		/// set belonging to a given class. 
		/// </summary>
		/// <param name="classConditionalDensities">The class conditional densities.</param>
		public MyBayesClassifier(IProbabilityDensity[] classConditionalDensities)
			 : base(classConditionalDensities.Length) {
			this.classConditionalDensities = classConditionalDensities;
			priorProbabilities = new double[classConditionalDensities.Length];
			for (int i = 0; i < classConditionalDensities.Length; i++) {
				priorProbabilities[i] = 1.0 / classConditionalDensities.Length;
			}
		}

		/// <summary>
		/// Classifies a feature vector.
		/// </summary>
		/// <param name="vector">The vector to be classified.</param>
		/// <returns>
		/// [0..classesCount-1] or -1 (unknown class)
		/// </returns>
		public override int Classify(FeatureVector vector) {
			double[] unused;
			return Classify(vector, out unused);
		}

		/// <summary>
		/// True if the classifier returns confidence information.
		/// </summary>
		public override bool HasConfidence { get { return true; } }

		/// <summary>
		/// Classifies the specified vector.
		/// </summary>
		/// <param name="vector">The vector.</param>
		/// <param name="confidence">The confidence of classification.</param>
		/// <returns></returns>
		public override int Classify(FeatureVector vector, out double[] confidence) {
			confidence = new double[priorProbabilities.Length];
			double maxPostProbability = 0;
			int max = -1;
			double probTot = 0; // Total probabilities (denominator in the Bayes rule)

			//Calcolo la probabilità totale, il denominatore nella regola di Bayes
			for (int i = 0; i < priorProbabilities.Length; i++)
				probTot += ((classConditionalDensities[i].CalculateDensity(vector)) * priorProbabilities[i]);

			//Calcolo il vettore di confidence, e trovo la classe (quella con valore di confidence più alto) a cui appartiene il patten
			for (int i = 0; i < priorProbabilities.Length; i++) {
				//Applico la formula di Bayes, se sommo tutti questi valori ottengo 1, in quanto sono valori probabilistici -> compresi tra 0 e 1
				confidence[i] = ((classConditionalDensities[i].CalculateDensity(vector)) * priorProbabilities[i]) / probTot;
				if (maxPostProbability < confidence[i]) {
					//Setto la probbilità più alta
					maxPostProbability = confidence[i];
					max = i;
				}
			}
			return max;
		}

		public override void SaveToStream(Stream stream) {
			throw new NotImplementedException();
		}

		protected override Data InternalClone() {
			throw new NotImplementedException();
		}
	}

	public class MyBayesClassifierBuilder : BayesClassifierBuilder {
		private MyNormalMLEstimator myNormalMLEstimator;

		public MyBayesClassifierBuilder(DensityEstimator densityEstimator)
			 : base(densityEstimator) {
		}

		public override string ToString() {
			return string.Format("My Bayes Classifier ({0})", DensityEstimator.ToString());
		}
	}

	/// <summary>
	/// Estimates the probability density functions of each class in a training set using
	/// the Parzen Window method.
	/// </summary>
	public class MyParzenEstimator : DensityEstimator {
		bool copy = true;

		/// <summary>
		/// Initializes a new instance of the <see cref="ParzenEstimator"/> class.
		/// </summary>
		public MyParzenEstimator() {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ParzenEstimator"/> class.
		/// </summary>
		/// <param name="bandwidth">The smoothing parameter of the method.</param>
		/// <param name="kernelType">Type of the kernel function.</param>
		public MyParzenEstimator(double bandwidth, ParzenKernelType kernelType)
			 : this(null, bandwidth, kernelType, true) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ParzenEstimator"/> class.
		/// </summary>
		/// <param name="trainingSet">The training set.</param>
		/// <param name="bandwidth">The smoothing parameter of the method.</param>
		/// <param name="kernelType">Type of the kernel function.</param>
		/// <param name="makeCopy">If True make a copy of the dataset (default true).</param>
		public MyParzenEstimator(FeatureVectorSet trainingSet, double bandwidth, ParzenKernelType kernelType, bool makeCopy)
			 : base(trainingSet) {
			Bandwidth = bandwidth;
			KernelType = kernelType;
			copy = makeCopy;
		}

		/// <summary>
		/// Gets or sets the value of the smoothing parameter.
		/// </summary>
		[AlgorithmParameter]
		public double Bandwidth { get; set; }

		/// <summary>
		/// Gets or sets the type of kernel function to be used.
		/// </summary>
		[AlgorithmParameter]
		public ParzenKernelType KernelType { get; set; }

		/// <summary>
		/// Gets or sets the user-defined kernel function. This property is used only when 
		/// <see cref="KernelType"/> is <see cref="ParzenKernelType.UserDefined"/>.
		/// </summary>
		[AlgorithmParameter]
		public ParzenKernel UserKernel { get; set; }

		/// <summary>
		/// Executes the algorithm.
		/// </summary>
		public override void Run() {
			var classCount = TrainingSet.FindMaxClassIndex() + 1;
			if (classCount == 0)
				classCount = 1;

			int dimension = TrainingSet.Dim;

			// Compute the normalization factor 1/h^d
			double normValue = 1.0 / Math.Pow(Bandwidth, dimension);

			// If GaussianKernel compute the constant factor
			if (KernelType == ParzenKernelType.Gaussian) {
				normValue *= 1.0 / (Math.Pow(Constants.DoublePI, (double)dimension / 2));
			}

			// Set the kernel type
			ParzenKernel kernel;
			switch (KernelType) {
				case ParzenKernelType.Hypercube:
					kernel = MyParzenClassEstimator.KernelFuncHyperCube;
					break;
				case ParzenKernelType.Gaussian:
					kernel = MyParzenClassEstimator.KernelFuncNormal;
					break;
				case ParzenKernelType.UserDefined:
					kernel = UserKernel;
					break;
				default:
					throw new ArgumentException("Invalid kernel type");
			}

			//Split training vectors per class
			var classTrainingSets = new List<Vector>[classCount];
			for (int i = 0; i < classCount; i++) {
				classTrainingSets[i] = new List<Vector>();
			}
			foreach (var fv in TrainingSet) {
				int index = fv.Class;
				if (index < 0) // unique class
					index = 0;

				classTrainingSets[index].Add(fv.Features);
			}

			ClassConditionalDensities = new IProbabilityDensity[classCount];
			for (int i = 0; i < classCount; i++) {
				ClassConditionalDensities[i] = new MyParzenClassEstimator(classTrainingSets[i].ToArray(), Bandwidth, kernel, normValue);
			}
		}

		public override string ToString() {
			return "MyParzen";
		}
	}

	public class MyParzenClassEstimator : IProbabilityDensity {
		private Vector[] classTrainingSet;
		private double bandWidth;
		private double normFactor;
		private ParzenKernel kernel;

		/*
		 * In caso il kernel sia quello soft il normFactor viene calcolato nel momento dell'invocazione quindi non mi devo preoccupare.
		 */
		public MyParzenClassEstimator(Vector[] classTrainingSet, double bandWidth, ParzenKernel kernel, double normFactor) {
			this.classTrainingSet = classTrainingSet;
			this.bandWidth = bandWidth;
			this.kernel = kernel;
			this.normFactor = normFactor / classTrainingSet.Length; // divide già il fattore di normalizzazione per il numero di elementi nella classe
		}

		public double CalculateDensity(Vector x) {
			double sum = 0;
			//Applico la formula generale su tutti i pattern (nota che qua usiamo la reflection quindi in kernel ho già la funzione settata)
			foreach (var pattern in classTrainingSet)
				sum += kernel((x - pattern) / bandWidth);

			//Moltiplico di due fattori, quello pre calcolato e quello calcolato da me, se viene maggiore di uno ritorna uno direttamente.
			if (normFactor * sum > 1)
				return 1;
			else
				return normFactor * sum;
		}

		public double CalculateDensity(FeatureVector vector) {
			return CalculateDensity(vector.Features);
		}

		public static double KernelFuncHyperCube(Vector u) {
			for (int i = 0; i < u.Count; i++)
				if (Math.Abs(u[i]) > 0.5)
					return 0.0;
			return 1.0;
		}

		public static double KernelFuncNormal(Vector u) {
			return Math.Exp(-(u.ComputeDotProduct() / 2));
		}

		public object Clone() {
			throw new NotImplementedException();
		}

		public void SaveToStream(Stream stream) {
			throw new NotImplementedException();
		}
	}
}