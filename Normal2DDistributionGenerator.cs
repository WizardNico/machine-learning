﻿using System;
using BioLab.Common;
using Accord.Statistics.Distributions.Multivariate;
using BioLab.Math.LinearAlgebra;

namespace PRLab
{
    class Normal2DDistributionGenerator : Algorithm
    {
        [AlgorithmInput]
        public double[] Mean { get; set; }

        [AlgorithmInput]
        public double[,] Covariance { get; set; }

        [AlgorithmInput]
        public double RotationAngle { get; set; }

        [AlgorithmInput]
        public int ObservationCount { get; set; }

        [AlgorithmOutput]
        public double[][] Observations { get; private set; }

        public Normal2DDistributionGenerator(double[] mean, double[,] covariance, double rotationAngle)
        {
            if (mean.Length != covariance.GetLength(0) ||
                mean.Length != covariance.GetLength(1))
            {
                throw new ArgumentException();
            }

            if (mean.Length != 2)
            {
                throw new ArgumentException();
            }

            Mean = mean;
            Covariance = covariance;
            RotationAngle = rotationAngle;
        }

        public override void Run()
        {
            var distribution = new MultivariateNormalDistribution(Mean, Covariance);

            var observations = distribution.Generate(ObservationCount);

            var cosTheta = Math.Cos(RotationAngle);
            var sinTheta = Math.Sin(RotationAngle);
            var rotationMat = new Matrix(new double[,]
                                        {
                                            {cosTheta,-sinTheta},
                                            {sinTheta,cosTheta}
                                        });

            var meanVector = new Vector(Mean, false);
            Observations = new double[observations.Length][];
            for (int i = 0; i < observations.Length; i++)
            {
                var v = new Vector(observations[i], false);

                v = v - meanVector;

                v = rotationMat.LeftMultiply(v);

                Observations[i] = (v + meanVector).ToArray();
            }
        }

        public double[][] Generate(int observationCount)
        {
            ObservationCount = observationCount;
            Run();

            return Observations;
        }
    }
}
